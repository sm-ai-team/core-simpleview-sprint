package debug

import "fmt"

func LogDebug(a ...any) {
	fmt.Println("\033[31m", a, "\033[0m")
}

func LogDebugf(fmt_string string, a ...any) (n int, err error) {
	s := fmt.Sprintf(fmt_string, a...)
	return fmt.Printf("\033[31m" + s + "\033[0m\n")
}
