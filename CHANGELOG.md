# Changelog

We'll do our best to document all notable changes to this project in this file.

## [0.1.0]
`core` version: v0.1.0
`smcore` version: v0.1.0

Ok big changes abound, not all of them super well tested, but all of them put us on a better long-term trajectory than previous versions.  Your patience is appreciated and thank you all in advance for the feedback and issues you have filed.  Most of these changes should be pretty invisible to end-users, but not entirely.

- Most important, the **configuration file has a new format** that is entirely agent-focused.

	This removes the `input_data` and `parameter_tuning` from the specification and adds support for a new `controllers` field.  Please see the Go [ping pong example](test-files/ping-pong.yaml) and the Python [ping pong example](test-files/pyng-pong.yaml) to see how to optionally use the new controller field.  If no controller is specified, the default controller will behave like the previous controller.  Pretty much everything else in terms of agent specification.

- Improved application entrypoints

	The current entrypoints still work fine, for now:
		- `./core run path/to/config.yaml` starts server, agents, and executes a knowledge graph until it completes
		- `./core server` runs a bare-bones blackboard server.  NOTE: I have removed the web-interface being present by default.  You will need to create and run a config file if you want additional agents.
		
	I have plans to deprecate `./core server` in favor of the new option:
		- `core start server` (exact same as `./core server`)
	I will try to give plenty of warning though before I strip the functionality out.	
		
	Overall these changes start to push all of the executable commands to take the form `./core <verb> <noun/target> [OPTIONS]` which improves long-term usability or something... (I'm not married to this idea, I just think it feels a little more natural to follow this pattern).
	
	With this shift I've added a **new function** to the exe where you can start ONLY your agents (no server) against an already running blackboard.  To point your agents toward the correct blackboard, remember to use the `-addr=myhost.mydomain.com:<port-num>`
		- `core start agents path/to/config.yaml`
	
- New controllers:

	One of the motivations behind these refactors was to support different control and decision logic around agent management.  I can also forsee where multiple controllers are managing agents on the same blackboard working on different but related problems. This was not possible with the previous implementation, but now should be possible with minimal addition engineering.  I've implemented two "out of the box" controllers for core:
	- [NaiveController](pkg/agents/controller_naive.go): Naive controller starts all of the agents at once and then returns.  It does not monitor the blackboard for agent completion or track agent status.  It is designed to be the "simplest possible" controller.
	- [BasicController](pkg/agents/controller_basic.go): The basic controller is very similar to the previous (invisible to end-users) controller that was utilized in, for example, `core run path/to/config.yaml`.  It dispatches agents once all incoming promises are fulfilled, as well as monitors for agent completion and shuts down the blackboard server once all dispatched agents have said goodbye.
	
- New [gonum/graph](https://pkg.go.dev/gonum.org/v1/gonum/graph)-compatible [knowledge graph](pkg/core/knowledge_graph.go) implementation.  This data structure is suitable for managing, monitoring, and analyzing knowledge graphs using graph algorithms available from gonum, and structures from core.  I will write more about this as we start to better utilize it.
  - [Save DOT/PNG/PDF/SVG](https://gitlab.com/hoffman-lab/core/-/blob/main/pkg/core/knowledge_graph.go?ref_type=heads#L159) of your knowledge graphs.  There's not an optional way to enable/disable this just yet without recompiling `core`, but if you want it badly enough, please file an issue to let me know.

- Refactors

	If I had kept better track, I'd be able to list it here, but I can't so I'll describe the stuff that I'm thinking about right now.  All of the refactors with done with an eye towards these issues.
	
	- **Performance**: 
		- Although folks are observing somewhat slow behavior, I think we should be able to do a lot with improved data serialization and networking practices before we really need to focus on speed.  I've added [an example](python/dicom_uploader.yaml) up uploading a file onto the blackboard in a space-efficient way.  Please take a look.  I've also written an [FAQ about saving numpy matrices](https://gitlab.com/hoffman-lab/core/-/issues/123) onto the blackboard as well (with a runnable example for you to copy/paste!).
		- I've basically "stubbed out" the code required ([here](https://gitlab.com/hoffman-lab/core/-/blob/main/cmd/start_server.go?ref_type=heads#L88) and [here](https://gitlab.com/hoffman-lab/core/-/blob/main/pkg/apis/core_api.go?ref_type=heads)) to add additional, lower-level (i.e. more performant) networking APIs.  This positions the core project to easily add and support in-parallel, better throughput communications at some point in the future.  Refactoring the entrypoint code gave me a good opportunity to think about how to handle this.  **I would be looking for help with this**, if anyone is curious to learn more about network and socket programming, data encoding and serialization, etc.  Please shoot me an email or leave a comment on this release.
		- Profiling: I've done some *very* cursory profiling of the server and it looks like some of the server-side bottleneck that folks might be experiencing could be due to a quick-and-dirty implementation choice for thread safety in the blackboard (required, since each server call from an agent is handled in a different [*goroutine*](https://gobyexample.com/goroutines).  This is likely due for a revisit, or offloading the blackboard operations out of memory and into something like [MongoDB](https://gitlab.com/hoffman-lab/core/-/blob/main/pkg/core/mongo_blackboard.go?ref_type=heads) or Redis (coming soon!) which are already extremely optimizing for multithreading, server-style communication, etc.  I've been reluctant to move us too quickly into this paradigm though because it requires thinking about `core` and SimpleMind as a software stack (of multiple applications and services) rather than the single-exe.  It will also probably require us generally leveraging docker-compose more in our day-to-day use.  I might be doing some testing over the holidays and making some choices based on what I find, if anything.
	
	- **Example Knowledge Graphs**
		- As we start to use the new system more and more, I'd like to start incorporating some runnable, scientific examples that illustrate more concepts than poor little "ping pong"!
		- I mentioned to Matt that I think I'd like to embed myself with a few of you to help you get running, performant versions of some of the early knowledge graphs that *work* but maybe run too slowly to be viable.  I think this would be useful for anyone in CVIB that wants to learn a bit more about "computer" things as this will essentially be us working one-on-one together to optimize agents, think about agent architecture, etc.  Please get in touch with me if you'd like to participate in this or have an example you'd like to send me.
		- I'm also happy for folks to just send me complete examples to optimize as well (also useful). Keep in mind the examples should be complete with data if possible.
		
	- **Runners**
		- Runners and cluster-scale computing are where this all actually gets good.  One of the key underlying design goals of all of this was to make it easier to get jobs where they needed to go.  I actually think that some of this will get better and easier once we can more comfortably utilize runners in our day-to-day work.
		- My exact vision for runners is not 100% clear (even to me), but it is motivated by my belief that they should quickly and easily allow you to call your agent in a target environment.  No annoying `condor_submit` files, no crazy AWS configuration, etc.  The runners will be programmed *once* by someone with expertise in that environment, and then everyone else can simply use the runner.
		- As a start I know I'd like to add support for the following environments (required pretty soon):
			- HTCondor 
			- Docker (simple, via remote host functionality and DOCKER_HOST environment variable - will likely require docker command to be installed)
			- Docker (full, via https://pkg.go.dev/github.com/docker/docker/client although I think this also requires the docker daemon to be installed... will need to investigate more)
			
		- Nice to have runners sometime in the future
			- SLURM (https://github.com/LenaO/slurm_go_bindings but also the two golang libraries readily available require the slurm shared library to be installed which goes against the usability philosophy of `core`/SM)
			- Kubernetes (k8s) - Although it's a little to experimental for now, I think (and strongly suspect) that k8s is the long term solution for all our environmental woes. It has built in job queing and management, it orchestrates containers and even now supports [GPU scheduling](https://kubernetes.io/docs/tasks/manage-gpus/scheduling-gpus/).  I'll be interested to see if anyone can come up with a workload that merits a cluster setup.  So that we don't have to initially go through the hassle of setting up a cluster on bare metal (which I've heard is more work than you think/want), cloud may be a good way to go to begin testing this.
		- Anyways, the refactors in this version play around with how runners are dispatched.  They are the only "special" agents left, dispatched directly by the executable (which is also now an agent), rather than the controller.
		
	- **Everything is an Agent**
		- And no agent should be special.  The more "special" agents we have, the more complicated our backend has to be, the less maintainable the code is in the long run, etc.
		- I've gone back and forth about how pendantic to be about this concept and what has emerged to me over the last few weeks is that the more pedantic we are about this, the more doors we have open to use with SM.
		- A lot of the refactoring between these two latest releases has been about reducing the number of special cases (i.e. removing the controller, simplifying the configuration file to just a grouped list of agents, etc.).
		
	- **Long-lived agents**
		- This is the next highest priority (after I finish this documentation).  Right now there's not a mechanism to fulfill a promise multiple times, which is a requirement for any agent that's going to stick around and keep working.
		- I see this as the last obvious requirement before we are at kind of a feature-complete, initial version of `core`.  Once we get to this point, I'd like to shift my focus "up" the stack to the SimpleMind/reasoning layer and start to press the system to see where it breaks, but with real research questions.
			
This has gone on for far too long, but the takeaways are this: 
- Some key changes in the latest version, new controllers, etc. Stuff I'm pretty excited about but probably won't have a massive impact on you!
- `core` should be nearing a "feature-complete" version relatively soon! After that most of the work should just be optimization of existing systems, more support for different environments, and adding new agents.  Pretty wild!

## [0.0.52]

`core` version: v0.0.52 `smcore` version: v0.0.66

**Make sure that you update both the `core` server as well as your `smcore` package.  This update includes a (theoretically backwards-compatible) protocol change, which means if the two packages are out of sync you may experience errors.  Please please please double check this before filing tickets. Peer pressure your friends into double-checking this.**

### Big updates:

- Switch Python API from `requests` to `pycurl`.

	Requests was functional, but a hot piece of garbage where performance is concerned.  PyCurl shows 10-20x performance improvements for smaller messages (<1MB).  Larger messages are bandwidth limited elsewhere in the software stack (which we'll look into at some point soon).
	
- Object Store offloading

	Hard to overstate the importance of this.  Large results (size is configurable by user, however default is 1MiB) will be transparently offloaded into an "object store" to relieve pressure on the blackboard.  Only agents that need the results will receive the result data.  You don't need to do anything to take advantage of this.  This should cut network load on the blackboard down tremendously (literally exponentially).
	
- DICOM uploader example

	I think there was some confusion about what I meant when I suggested base64 encoding as an efficient way to get data onto the blackboard.  Base64 is inherently *inefficient* however it's a quick way to get a JSON-compatible version of any blob of raw byte data (such as a file).  This means that you can essentially just upload the contents of a file directly to the blackboard, without having to read/parse/serialize the contents of the underlying file type (we still have to actually read the data though).  Just get the data into memory, base64 encode it, then save it to the blackboard.  The DICOM uploader example illustrates this concept.  Please see the comment at the top of `python/dicom_uploader.py` for how you would read such data from the blackboard.
	
- Await individual promises with the new "attributes" API

	Attributes show now be accessed via `val = self.attributes.value("attribute-name")` or awaited via `val = await self.attributes.await_value("attribute-name")`.  This allows you begin processing as soon as individual chunks of result data arrive, rather than having to await all promises being resolved.  The old API should still work, so no need to update old code if you don't want to.
	
### Medium changes

- Load testing agent - load_test.py - is now available for some *basic* benchmarking of CPU and networking performance.  This is a highly synthetic benchmark unlikely to reflect real SM-core use, however it's a starting point for comparing code changes and more importantly hardware and software stack limitations.  It measures how many messages of a given size we can "stuff down the pipe" (i.e. write to the blackboard) in a given duration.  I will write more about this once I've matured it a bit, but please feel free to start experimenting with it.

```
./core run test-files/load-test.yaml
grep sent blackboard.json # get the results from the blackboard
```

### Smaller changes

- Promises and promise resolution should now be fully thread safe (they were not in previous versions)
- `core` executable exits with non-zero exit code (1) if an *agent* throws an error. This is useful if you want to programmatically determine if a run was successful or failed.
- Better traceback reporting from the blackboard if python agents experience an error (useful for debugging)

### Final thoughts

The above changes should put us on the roadmap to a faster SM-core setup.  Unfortunately, in some simple load tests, I'm still unable to fully saturate my available network bandwidth.  This hints at inefficiency in the software side, which is not unexpected.  I won't delve into all of the possible reasons for this, however there are loads of possible fixes.  I'd like to see how far this set of changes takes us, along with some improvements in data handling on the agent side (I've heard about some... "explosive" serialization techniques that can be significantly improved upon), before I double/triple/quadruple down the efforts to improve networking.
	
## [Prerelease]

## [0.0.47]

### Stuff you probably care about
- First actual release!
- **Adds --addr command line argument for setting hostname:port of blackboard server**
- Fix python regexp for promise parsing; **Allows agent names with `-`**
- Embed cytoscape JS files for offline use of the web-interface (**live dash actually works now without internet!**)

### Medium stuff
- Sane defaults for python agents (1 sec for pingInterval and pollInterval)
- Set minimum ping/poll interval (33ms) for server sparing
- Add VERSION file and `--version` to rootCmd

### Smaller stuff (or still experimental)
- Add `GetResults` to Blackboard interface
- Add `/result/{starting_at}` HTTP endpoint
- Disable gRPC API (at least temporarily)

The format of this doc is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

[prerelease]: https://gitlab.com/hoffman-lab/core/compare/v0.0.47...HEAD
[0.0.47]: https://gitlab.com/hoffman-lab/core/-/tree/v0.0.47
[0.0.52]: https://gitlab.com/hoffman-lab/core/-/tree/v0.0.52