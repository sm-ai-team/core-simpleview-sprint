package config

var Globals *Config

// Struct for global configuration
func init() {
	Globals = DefaultConfig()
}

type Config struct {
	QuietBlackboard          bool   // disable the blackboard output
	NoSplash                 bool   // disable the splash screen
	ShowPings                bool   // hide/show agent background pings in the blackboard
	MinPingInterval_Ms       int    // minimum allowed ping interval in ms
	OutputFile               string // save blackboard to OutputFile
	BigResultThreshold       int    // Data sizes greater than BigResultThreshold will be sent to file store
	BlackboardGRPCAddress    string //
	BlackboardHTTPAddress    string //
	ObjectStoreAddress       string //
	MongoBBSpec              string //
	OutputWidth              int    // limit blackboard output to OutputWidth (columns); does not impact other CLI output;
	ObjectSizeThreshold      int    //
	UseDefaultController     bool
	ShutdownOnControllerHalt bool
}

func DefaultConfig() *Config {
	return &Config{
		QuietBlackboard:          false,
		NoSplash:                 false,
		ShowPings:                false,
		MinPingInterval_Ms:       1000,
		OutputFile:               "blackboard.json",
		BigResultThreshold:       4096,
		BlackboardHTTPAddress:    "localhost:8080",
		BlackboardGRPCAddress:    "localhost:8081",
		ObjectStoreAddress:       "localhost:8082",
		MongoBBSpec:              "",
		OutputWidth:              0,
		ObjectSizeThreshold:      1_048_576, // 1 Mebibyte
		UseDefaultController:     true,
		ShutdownOnControllerHalt: true,
	}
}
