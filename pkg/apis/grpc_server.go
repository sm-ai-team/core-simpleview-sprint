package apis

import (
	"context"
	"net"

	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core" // I know dot imports are bad...
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type BlackboardGRPCService struct {
	UnimplementedBlackboardHandlerServer

	bb Blackboard // Data backend
}

func NewBlackboardGRPCService(bb Blackboard) *BlackboardGRPCService {

	return &BlackboardGRPCService{
		bb: bb,
	}
}

// Start is a blocking call to begin listening on
func (b *BlackboardGRPCService) Start(listen_addr string) error {

	msg := &Message{
		Source:   "grpc-api",
		Priority: 5,
		Contents: &Message_Log{
			Log: &Log{
				Severity: Log_Info,
				Message:  "grpc api starting on " + listen_addr,
			},
		},
		TimeSent: timestamppb.Now(),
	}

	b.bb.WriteMessage(msg)

	var opts = []grpc.ServerOption{
		grpc.Creds(insecure.NewCredentials()),
	}
	// TODO: Maybe support TLS?  No obvious use research case for now.

	lis, err := net.Listen("tcp", listen_addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer(opts...)
	RegisterBlackboardHandlerServer(grpcServer, b)
	return grpcServer.Serve(lis)
}

// Transaction (polling) API
func (b *BlackboardGRPCService) GetMessages(ctx context.Context, req *GetMessagesRequest) (*MessageStack, error) {

	msgs, err := b.bb.GetMessages(int(req.StartingAt), req.FilterPings)
	if err != nil {
		return nil, status.Errorf(codes.Unavailable, "get messages from blackboard failed: "+err.Error())
	}

	msg_stack := &MessageStack{
		Messages: msgs,
	}

	return msg_stack, nil
}

func (b *BlackboardGRPCService) SendMessage(ctx context.Context, msg *Message) (*Ack, error) {

	err := b.bb.WriteMessage(msg)
	if err != nil {
		return nil, status.Errorf(codes.Unavailable, "write message to blackboard failed: "+err.Error())
	}

	return &Ack{}, nil
}

// TODO: Streaming API
func (b *BlackboardGRPCService) OpenMessageStream(BlackboardHandler_OpenMessageStreamServer) error {
	return status.Errorf(codes.Unimplemented, "method OpenMessageStream not implemented")
}
