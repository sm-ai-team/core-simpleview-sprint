# apis

This package contains the implementation code for exposing a Blackboard interface. 

## HTTP

This is currently the only maintained API for the blackboard.

## gRPC

As of this update, the gRPC API is (at least temporarily) not being maintained so that we can focus effort onto maintaining the REST API and other features and functionality.

Ultimately gRPC is just not quite as reliable as HTTP (a high bar to clear), but if gRPC is a priority for you (i.e. network performance issues like latency, packet sizes, etc.), please let us know in the issues and it should not be too difficult to bring it up to date.