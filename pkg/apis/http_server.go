package apis

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	//	"fmt"

	"github.com/golang/protobuf/jsonpb"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type BlackboardHTTPService struct {
	R    *mux.Router
	bb   Blackboard
	serv *http.Server
}

func NewBlackboardHTTPService(bb Blackboard) *BlackboardHTTPService {

	http_bb := &BlackboardHTTPService{
		bb: bb,
	}

	// Configure the RESTish API
	r := mux.NewRouter()
	r.HandleFunc("/blackboard", http_bb.BlackboardHandler)                                       // /blackboard?start=<idx>&stop=<idx,optional>
	r.HandleFunc("/blackboard/{start_idx:[0-9]+}", http_bb.BlackboardHandler)                    // /blackboard?start=<idx>&stop=<idx,optional>
	r.HandleFunc("/message/{id:[0-9]+}", http_bb.MessageHandler).Methods(http.MethodGet)         // /message?id=<id>
	r.HandleFunc("/message", http_bb.NewMessageHandler).Methods(http.MethodPost, http.MethodPut) // /message?id=<id>
	r.HandleFunc("/result", http_bb.ResultHandler).Methods(http.MethodGet)                       // /result?id=<id>
	r.HandleFunc("/result/{start_idx:[0-9]+}", http_bb.ResultHandler).Methods(http.MethodGet)    // /result?id=<id>
	r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) }).Methods(http.MethodGet)

	http_bb.R = r

	return http_bb
}

// Start is a blocking call to begin listening list_addr
func (b *BlackboardHTTPService) Start(listen_addr string) error {

	msg := &Message{
		Source:   "http-api",
		Priority: 5,
		Contents: &Message_Log{
			Log: &Log{
				Severity: Log_Info,
				Message:  "http api starting on " + listen_addr,
			},
		},
		TimeSent: timestamppb.Now(),
	}

	b.bb.WriteMessage(msg)

	b.serv = &http.Server{
		Addr:    listen_addr,
		Handler: b.R,
	}

	//return http.ListenAndServe(listen_addr, b.R)
	return b.serv.ListenAndServe()
}

func (b *BlackboardHTTPService) Shutdown() error {
	log.Info("blackboard http service shutting down")
	return b.serv.Shutdown(context.Background())
}

func (b *BlackboardHTTPService) BlackboardHandler(w http.ResponseWriter, r *http.Request) {
	//Println("Got Blackboard request from:", r.RemoteAddr)

	params := mux.Vars(r)

	start_idx, ok := params["start_idx"]
	if !ok {
		start_idx = "0"
	}

	start, err := strconv.Atoi(start_idx)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	msgs, err := b.bb.GetMessages(start, true)
	if err != nil {
		http.Error(w, "Blackboard error: "+err.Error(), http.StatusInternalServerError)
	}

	// Protobuffers - nothing else like it but damn is it janky.  It would be a lot
	// less janky if they could get it working with the default json encoder in go.
	//
	// We encode with protojson since the default encoder does not work correctly
	// protocol buffer messages.  Annoyingly, we have to now configure the header
	// data separately.
	//
	// We have to encode each msg indepedently and then transmit the array once it's built

	tmp := &MessageStack{
		Messages: msgs,
	}

	data, err := protojson.Marshal(tmp)
	if err != nil {
		log.Error("Failed to encode to json: ", err)
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(data)
	if err != nil {
		log.Error("Failed to transmit json")
	}

	//err = json.NewEncoder(w).Encode(msgs)
	//if err != nil {
	//	// Should errors be written to the blackboard?
	//	log.Error("Failed to encode to json: ", err)
	//}

}

func (b *BlackboardHTTPService) NewMessageHandler(w http.ResponseWriter, r *http.Request) {

	msg := &Message{}

	err := jsonpb.Unmarshal(r.Body, msg)
	if err != nil {
		log.Error("Failed to decode to json: ", err)
		return
	}

	err = b.bb.WriteMessage(msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else {
		w.WriteHeader(http.StatusCreated)
	}

}

func (b *BlackboardHTTPService) MessageHandler(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, ok := params["id"]
	if !ok {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	id_int, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, "Bad request: "+err.Error(), http.StatusBadRequest)
		return
	}

	msg, err := b.bb.GetMessage(id_int)
	if err != nil {
		http.Error(w, "Something went wrong: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(msg)
	if err != nil {
		log.Error("Failed to encode to json: ", err)
		return
	}

}

func (b *BlackboardHTTPService) ResultHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	start_idx, ok := params["start_idx"]
	if !ok {
		start_idx = "0"
	}

	start, err := strconv.Atoi(start_idx)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	msgs, err := b.bb.GetResults(start)
	if err != nil {
		http.Error(w, "Something went wrong when retrieving result messages: "+err.Error(), http.StatusInternalServerError)
		return
	}

	tmp := &MessageStack{
		Messages: msgs,
	}

	data, err := protojson.Marshal(tmp)
	if err != nil {
		log.Error("Failed to encode to json: ", err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(data)
	if err != nil {
		log.Error("Failed to transmit json")
		return
	}
}
