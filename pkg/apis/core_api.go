package apis

type CoreAPI interface {
	Start(listen_addr string) error
	Shutdown() error
}
