package core

import (
	"fmt"
	"testing"

	"github.com/matryer/is"
)

func TestKnowledgeGraph(t *testing.T) {

	is := is.New(t)

	var agents = map[string]*AgentSpec{
		"ping": {
			Name: "ping",
		},
		"pong": {
			Name: "pong",
			Attributes: map[string]string{
				"message": "Promise( ping as pingdata from ping )",
			},
		},
		"pong2": {
			Name: "pong2",
			Attributes: map[string]string{
				"message": "Promise( ping as pingdata from ping )",
			},
		},
		"downstream": {
			Name: "downstream",
			Attributes: map[string]string{
				"message": "Promise( random as downstream-data from pong2 )",
				"contact": "Promise( validation as downstream-data from ping )",
			},
		},
	}

	kg := NewKnowledgeGraph()

	kg.FromAgentBundle(agents)

	// Test the CanStart method
	ping_node := kg.Node(kg.nameLookup["ping"])
	pong_node := kg.Node(kg.nameLookup["pong"])

	is.True(kg.CanStart(ping_node))  // ping can start
	is.True(!kg.CanStart(pong_node)) // pong cannot start

	e := kg.Edge(ping_node.ID(), pong_node.ID()).(*PromiseEdge)
	e.State = Promise_Final

	is.True(kg.CanStart(pong_node)) // pong can start (after promise fulfillment)

	// Test GetStartableSpecs()
	specs := kg.GetStartableSpecs()
	fmt.Println(specs)
	is.Equal(len(specs), 2) // two startable agents

	// Make ping "started"; should reduce the number
	// of startable agents to one (only pong)
	kg.MarkStarted("ping")
	specs = kg.GetStartableSpecs()
	is.Equal(len(specs), 1) // one startable agents (pong)

	// Save methods (should probably generally be skipped)
	//err := kg.ToPDF("test-dot.pdf")
	//is.NoErr(err)
	//
	//err = kg.ToSVG("test-dot.svg")
	//is.NoErr(err)
	//
	//err = kg.ToPNG("test-dot.png")
	//is.NoErr(err) // must save to dot

}
