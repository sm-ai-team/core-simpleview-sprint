package core

import (
	"time"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
)

type AgentNode struct {
	id         int64
	dotID      string
	spec       *AgentSpec
	started    bool
	incoming   graph.Nodes
	outgoing   graph.Nodes
	lastStatus *StatusUpdate
	lastPing   time.Time
}

// Fulfill the Node interface
func (a *AgentNode) ID() int64 {
	return a.id
}

func (a *AgentNode) Started() bool {
	return a.started
}

func (a *AgentNode) Attributes() []encoding.Attribute {
	return []encoding.Attribute{
		{Key: "label", Value: a.spec.Name},
		{Key: "fontname", Value: "Helvetica"},
	}
}
