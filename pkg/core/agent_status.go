package core

import (
	"fmt"
	"sync"
	"time"
)

// AgentStatus is a cache for the latest state information reported to the controller.
// It is not intended for use outside of the controller.
type AgentStatus struct {
	State            StatusUpdate_AgentState
	LastPing         time.Time
	Errors           []error
	LastStatusUpdate *StatusUpdate
}

// AgentStatusSet adds behavior to a group of agent statuses.  Methods also guarantee thread
// safety since the structure will be used by the controller in multiple goroutines, and
// eventually be accessed externally to report the current state of the system (such as in
// a web dashboard, or similar)
//
// Quiet failures (i.e. if we attempt to set state on an agent "name" that's not being tracked)
// are intentional at this stage, however may not always be preferred behavior.
type AgentStatusSet struct {
	mu     sync.Mutex
	status map[string]*AgentStatus
}

func NewAgentStatusSet() *AgentStatusSet {
	return &AgentStatusSet{
		mu:     sync.Mutex{},
		status: make(map[string]*AgentStatus),
	}
}

// Add a new agent to the StatusSet
func (a *AgentStatusSet) NewAgentStatus(name string) {
	a.mu.Lock()
	defer a.mu.Unlock()
	a.status[name] = &AgentStatus{
		Errors: make([]error, 0, 0),
	}
}

// GetStatus return the current AgentStatus for agent "name".  Returns nil if
// agent does not exist in the agent monitor tracker.
func (a *AgentStatusSet) GetStatus(name string) *AgentStatus {
	a.mu.Lock()
	defer a.mu.Unlock()
	a_status, ok := a.status[name]
	if !ok {
		return nil
	}

	return a_status
}

func (a *AgentStatusSet) GetStatuses() map[string]*AgentStatus {
	return a.status
}

// UpdateState sets the agent state, if found
func (a *AgentStatusSet) UpdateState(name string, state StatusUpdate_AgentState) {
	a.mu.Lock()
	defer a.mu.Unlock()

	if state == StatusUpdate_Error {
		a.status[name].Errors = append(a.status[name].Errors, fmt.Errorf("%s experienced an error", name))
	}

	a_tmp, ok := a.status[name]
	if !ok {
		return
	}
	a_tmp.State = state
}

// UpdateState sets the agent LastStatusUpdate, if found
func (a *AgentStatusSet) UpdateStatus(name string, sm *StatusUpdate) {
	a.mu.Lock()
	defer a.mu.Unlock()

	if sm.State == StatusUpdate_Error {
		if a.status[name] != nil { // kluge for issue #106
			a.status[name].Errors = append(a.status[name].Errors, fmt.Errorf("%s experienced an error: %s", name, sm.Message))
		}
	}

	a_tmp, ok := a.status[name]
	if !ok {
		return
	}

	a_tmp.LastStatusUpdate = sm
	a_tmp.State = sm.State

}

// UpdateState sets the agent LastPing, if found
func (a *AgentStatusSet) UpdatePing(name string, ping_time time.Time) {
	a.mu.Lock()
	defer a.mu.Unlock()

	a_tmp, ok := a.status[name]
	if !ok {
		return
	}
	a_tmp.LastPing = ping_time
}

// AllCompleted checks the state of all agents from the AgentCompleted state
// Returns true if all agents have completed; false otherwise.
func (a *AgentStatusSet) AllCompleted() bool {
	a.mu.Lock()
	defer a.mu.Unlock()

	for _, status := range a.status {
		//fmt.Printf("%s: %+v\n", name, a.status["pong_agent"])
		if status.State != StatusUpdate_Completed {
			return false
		}
	}
	return true
}

func (a *AgentStatusSet) GetErrors() []error {

	errs := make([]error, 0)

	for _, agent_status := range a.status {
		errs = append(errs, agent_status.Errors...)
	}

	return errs
}

func (a *AgentStatusSet) DispatchGroupCompleted(disp_mon *DispatchMonitor) bool {
	a.mu.Lock()
	defer a.mu.Unlock()

	curr_grp := disp_mon.GetCurrentAgentGroup()

	for agent_name, status := range a.status {
		if agent_name == "web_agent" {
			continue
		}
		if _, ok := curr_grp[agent_name]; ok {
			if status.State != StatusUpdate_Completed {
				return false
			}
		}
	}

	return true
}
