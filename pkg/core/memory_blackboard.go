package core

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/hoffman-lab/core/pkg/config"
	"google.golang.org/protobuf/types/known/timestamppb"

	"github.com/briandowns/spinner"
)

// Concrete implementation of Blackboard interface to keep everything in memory
var _ Blackboard = &MemoryBB{}

// John thought: Passing the object store and storing
// the interface like this is kind of ugly.  It does allow
// us to avoid having globals, but something about it just
// doesn't really work for me.  Feels kinda MATLAB/Python to do
// it this way.
type MemoryBB struct {
	objectStore ObjectStore
	mu          sync.Mutex
	Messages    []*Message `json:"Messages"`
	Results     []*Message `json:"Results"`
}

func NewMemoryBB(obj ObjectStore) *MemoryBB {
	m := &MemoryBB{
		objectStore: obj,
		Messages:    make([]*Message, 0, 500),
	}
	return m
}

func (b *MemoryBB) ResultSizeLimit() int {
	return 4096 // Pretty aggressive for the MemoryBB
}

func (b *MemoryBB) Len() int {
	return len(b.Messages)
}

func (b *MemoryBB) WriteMessage(msg *Message) error {
	b.mu.Lock()
	defer b.mu.Unlock()

	// Set message ID, Index, and time received
	//
	// I'm including "index" here since I can imagine needing to
	// change the id to a randomly generated something or other, but
	// still have an auto-incrementing value.  May not be necessary
	msg.Id = int32(len(b.Messages))
	msg.Index = int32(len(b.Messages))
	msg.TimeRecv = timestamppb.Now()

	// Type-specific block for saving messages
	switch msg.Contents.(type) {
	case *Message_Result:

		// Check for large result. We do not need to check Oneof here
		// since incoming messages will not (should not) ever have Link set.
		result := msg.GetResult()
		prom := result.GetPromise()
		data := prom.GetData()

		if len(data) > config.Globals.ObjectSizeThreshold {

			// save to object store
			link, err := b.objectStore.Put(data)
			if err != nil {
				return err
			}

			// change out the message contents
			prom.Contents = &Promise_Link{
				Link: link,
			}

			result.Promise = prom

			msg.Contents = &Message_Result{
				Result: result,
			}
		}

		b.Results = append(b.Results, msg)
	}

	// Only print non-ping messages; controls BB stdout
	switch msg.Contents.(type) {
	case *Message_Ping:
		if config.Globals.ShowPings {
			fmt.Println(msg.TerminalString())
		}
	default:
		if !config.Globals.QuietBlackboard {
			fmt.Println(msg.TerminalString())
		}
	}

	b.Messages = append(b.Messages, msg)

	return nil
}

// This sort of approach mandates that the message ID must be the index in the slice
// While this makes sense for this data structure, it may not always be so.
// Returns messages starting from idx
//
// As the BB and # of pings grows, this operation is going to get
// expensive.  We may not want to actually store pings on the BB.  Let's
// not make any changes for now, but maybe profile as users start implementing
// agents.  We could also just have a minimum ping interval that we recommend.
func filterPings(msgs []*Message) []*Message {
	filt_msgs := make([]*Message, 0, len(msgs))
	for _, msg := range msgs {

		_, is_ping := msg.Contents.(*Message_Ping)

		if !is_ping {
			filt_msgs = append(filt_msgs, msg)
		}
	}
	return filt_msgs
}

func (b *MemoryBB) GetMessages(starting_at int, filter_pings bool) ([]*Message, error) {
	b.mu.Lock()
	defer b.mu.Unlock()

	var messages []*Message
	if starting_at >= len(b.Messages) {
		messages = []*Message{}
	} else if starting_at < 0 {
		messages = b.Messages
	} else {
		messages = b.Messages[starting_at:]
	}

	if filter_pings {
		messages = filterPings(messages)
	}

	return messages, nil
}

func (b *MemoryBB) GetMessage(id int) (*Message, error) {
	b.mu.Lock()
	defer b.mu.Unlock()

	if id < 0 || id >= len(b.Messages) {
		return nil, fmt.Errorf("invalid ID: %d (allowed 0 to %d)", id, len(b.Messages)-1)
	}

	return b.Messages[id], nil
}

func (b *MemoryBB) GetResults(starting_at int) ([]*Message, error) {

	b.mu.Lock()
	defer b.mu.Unlock()

	var messages []*Message
	if starting_at >= len(b.Messages) {
		return messages, nil
	} else if starting_at < 0 {
		starting_at = 0
	}

	for _, msg := range b.Results {
		if msg.Index < int32(starting_at) {
			continue
		}
		messages = append(messages, msg)
	}

	return messages, nil
}

func (b *MemoryBB) Save(output_file string) error {

	output_filepath := config.Globals.OutputFile

	log.Info("saving blackboard to " + output_filepath)

	f, err := os.Create(output_filepath)
	if err != nil {
		// Allow user to specify another path if the first one fails?
		log.Error("unable to open file: ", output_filepath+":", err)
	}

	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")
	err = enc.Encode(b)
	if err != nil {
		log.Error("unable to write to: ", output_filepath+":", err)
	}

	return nil
}

func (b *MemoryBB) Shutdown() {

	fmt.Println("================================================================================")

	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.Start()

	err := b.Save(config.Globals.OutputFile)
	if err != nil {
		log.Error("saving blackboard failed:", err)
	}

	s.Stop()

	log.Info("blackboard shutting down")
}
