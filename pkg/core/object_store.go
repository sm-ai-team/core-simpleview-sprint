package core

// Upload data to object, receive link that can be used to retrieved
// data later.
type ObjectStore interface {
	Put(object []byte) (string, error)
}
