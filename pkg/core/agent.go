package core

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
)

type Agent interface {
	Start()
	Stop()
	Run() error
	Done() <-chan struct{}
}

type GoAgent struct {
	*AgentSpec
	OnNewMessage func(m *Message)

	// Cached, state stuff
	ctx        context.Context
	CancelFunc func()
	wg         *sync.WaitGroup
	//promises         PromiseSet
	BB               Blackboard
	parsedAttributes map[string]interface{}
	fulfilled        chan struct{}

	Attributes *Attributes
}

func NewGoAgent(BB Blackboard, spec *AgentSpec) *GoAgent {

	a := &GoAgent{
		AgentSpec:  spec,
		BB:         BB,
		wg:         &sync.WaitGroup{},
		fulfilled:  make(chan struct{}),
		Attributes: NewAttributes(spec.Attributes),
	}
	a.OnNewMessage = a.DefaultOnNewMessage

	// TODO generate attributes structure from spec
	// make sure that attributes is proper

	//a.promises, err = a.AgentSpec.GetPromises()
	//if err != nil {
	//	log.Fatal(err)
	//}

	return a
}

func (a *GoAgent) Run() error {
	return fmt.Errorf("using goagent default run method")
}

func (a *GoAgent) Start() {

	err := a.SayHello()
	if err != nil {
		return
	}

	//Start concurrent processes
	ctx, CancelFunc := context.WithCancel(context.Background())

	a.ctx, a.CancelFunc = ctx, CancelFunc

	//a.Log(Log_Info, "Starting agent: "+a.Name)

	// Ping loop
	miss_count := 0

	if a.PingIntervalMs < 5000 {
		a.PingIntervalMs = 5000
	}

	a.wg.Add(1)
	go func() {
		defer a.wg.Done()
		for {
			select {
			case <-time.After(time.Duration(a.PingIntervalMs) * time.Millisecond):
				err := a.SendPing()
				if err != nil {
					miss_count++

					// this may be off-by-one...
					if miss_count >= int(a.AllowedPingRetries) {
						return
					}

					continue
				}
				miss_count = 0 // reset miss counter on success

			case <-a.ctx.Done():
				return
			}
		}
	}()

	// Message retrieval
	a.wg.Add(1)
	go func() {
		defer a.wg.Done()

		last_idx := 0
		poll_retries := 0

		// TO DO: Add an intitialization code block that can determine whether a user
		// wants to retrieve all messages or just results

		for {
			select {
			case <-time.After(time.Duration(a.PollIntervalMs) * time.Millisecond):
				msgs, err := a.BB.GetMessages(last_idx+1, true)
				if err != nil {
					poll_retries++
					if poll_retries > int(a.AllowedPollRetries) {
						return
					}
				}

				// _, err = a.BB.GetResultMessages(last_idx + 1)
				// if err != nil {
				// 	poll_retries++
				// 	if poll_retries > int(a.AllowedPollRetries) {
				// 		return
				// 	}
				// }

				for _, m := range msgs {
					last_idx = int(m.Index)
					a.OnNewMessage(m)
				}

			case <-a.ctx.Done():
				return
			}
		}
	}()

}

func (a *GoAgent) Stop() {
	// Take down processes
	a.CancelFunc()
	//time.Sleep(1 * time.Second)
	a.wg.Wait()
	a.SayGoodbye()
}

func (a *GoAgent) Done() <-chan struct{} {
	return a.ctx.Done()
}

//func (a *GoAgent) AwaitData() *PromiseSet {
//
//	if a.Attributes.HasPromises() {
//		a.PostStatusUpdate(StatusUpdate_AwaitingPromise, fmt.Sprintf("awaiting %d promises", a.Attributes.NumPromises()))
//	} else {
//		a.PostStatusUpdate(StatusUpdate_Working, "")
//		return a.Attributes.promises
//	}
//
//	select {
//	case <-a.fulfilled:
//		a.PostStatusUpdate(StatusUpdate_Working, "")
//		return a.Attributes.promises
//	case <-a.ctx.Done():
//		return nil
//	}
//
//}

func (a *GoAgent) notifyFullfillment() {
	select {
	case a.fulfilled <- struct{}{}:
	default:
	}
}

func (a *GoAgent) SayHello() error {
	return a.SendMessage(&Message_Hello{Hello: &Hello{}})
}

func (a *GoAgent) SayGoodbye() error {
	return a.SendMessage(&Message_Goodbye{Goodbye: &Goodbye{}})
}

func (a *GoAgent) SendPing() error {
	return a.SendMessage(&Message_Ping{Ping: &Ping{}})
}

func (a *GoAgent) Log(severity Log_Severity, message string) error {
	return a.SendMessage(&Message_Log{Log: &Log{Severity: severity, Message: message}})
}

func (a *GoAgent) PostStatusUpdate(state StatusUpdate_AgentState, message string) error {
	return a.SendMessage(
		&Message_StatusUpdate{
			StatusUpdate: &StatusUpdate{
				State:   state,
				Message: message,
			},
		})
}

func (a *GoAgent) PostResult(result_type Result_ResultType, promise *Promise) error {

	promise.Source = a.Name
	promise.State = Promise_Final

	//Source: p.Name,
	return a.SendMessage(
		&Message_Result{
			Result: &Result{
				Type:    result_type,
				Promise: promise,
			},
		})
}

func (a *GoAgent) PostPartialResult(result_type Result_ResultType, promise *Promise) error {

	promise.Source = promise.Name
	promise.State = Promise_Fulfilled

	//Source: p.Name,
	return a.SendMessage(
		&Message_Result{
			Result: &Result{
				Type:    result_type,
				Promise: promise,
			},
		})
}

func (a *GoAgent) PostNoResult(result_type Result_ResultType, promise *Promise) error {

	promise.Source = promise.Name

	//Source: p.Name,
	return a.SendMessage(
		&Message_Result{
			Result: &Result{
				Type:    result_type,
				Promise: promise,
			},
		})
}

func (a *GoAgent) DefaultOnNewMessage(m *Message) {

	switch m.Contents.(type) {
	case *Message_Result:
		r := m.GetResult()

		a.Attributes.ProcessResult(r)

		// I don't want users interacting with promises directly
		//attr_name, ok := a.Attributes.promises.Contains(r.Promise)
		//
		//if ok {
		//	a.promises[attr_name] = r.Promise
		//}
	case *Message_Halt:
		a.CancelFunc()
	case *Message_HaltAndCatchFire:
	}

	if a.Attributes.HasPromises() && a.Attributes.AllFulfilled() {
		a.notifyFullfillment()
	}

}

// I think I would generally discourage non-runner, non-controller agents
// from posting create agent messages.  but who am I to judge? you do you.
// just let me know if you find or do something cool!
func (a *GoAgent) PostCreateAgent(spec *AgentSpec) error {
	return a.SendMessage(&Message_CreateAgent{CreateAgent: spec})
}

// Wrap the message contents in the necessary envelope and fire
func (a *GoAgent) SendMessage(contents isMessage_Contents) error {

	msg := &Message{
		Source:   a.Name,
		Priority: 5,
		Contents: contents,
		TimeSent: timestamppb.Now(),
	}

	return a.BB.WriteMessage(msg)
}

func (a *GoAgent) SetAttributes(attrs map[string]interface{}) {
	a.parsedAttributes = attrs
}

func (a *GoAgent) ParseAttributes() (map[string]interface{}, error) {
	// Transform the map[string]string into a map[string]interface{}

	attrs_json, err := json.Marshal(a.Attributes)
	if err != nil {
		return nil, err
	}

	a.parsedAttributes = make(map[string]interface{})
	err = json.Unmarshal(attrs_json, &a.parsedAttributes)
	if err != nil {
		return nil, err
	}

	return a.parsedAttributes, nil
}
