package core

import (
	"os"
	"testing"
	"time"

	"github.com/matryer/is"
)

func TestAgent(t *testing.T) {

	is := is.New(t)

	BB := NewMemoryBB(nil)

	spec := DefaultAgentSpec("test-agent")
	agent := NewGoAgent(BB, spec)

	a := agent

	// Need to actually verify blackboard state; skipping that for now
	err := a.SayHello()
	is.NoErr(err)

	err = a.PostStatusUpdate(StatusUpdate_OK, "Writing a message!")
	is.NoErr(err)

	err = a.PostStatusUpdate(StatusUpdate_AwaitingPromise, "Doing some more stuff!")
	is.NoErr(err)

	time.Sleep(1 * time.Second)

	err = a.SayGoodbye()
	is.NoErr(err)

}

func cleanUpTempFile(f *os.File) {
	f.Close()
	os.Remove(f.Name())
}

// Is this test still valuable? *Agents* aren't supposed to be
// serialized, only their spec is, which is covered by protobuffers.
// Leaving here for the time being, but if you still can't justify this
// in a few weeks, delete it. 2023/07/04 JMH
//
//func TestAgentSerialization(t *testing.T) {
//
//is := is.New(t)
//
//sample_attributes := map[string]interface{}{
//	"hello":          "world",
//	"another_value":  1234,
//	"somethingelse":  []int{1, 2, 3, 4},
//	"somethingelse2": "[1,2,3,4]",
//}
//
//// I know assigning function like this is sorta bad form, but I
//// want to lazily capture our "is" instance
//checkKeyAgentConfigFields := func(a1, a2 *GoAgent) bool {
//
//	is.Equal(a1.Name, a2.Name)
//	is.Equal(a1.PollIntervalMs, a2.PollIntervalMs)
//	is.Equal(a1.PingIntervalMs, a2.PingIntervalMs)
//	is.Equal(a1.AllowedPingRetries, a2.AllowedPingRetries)
//	is.Equal(a1.AllowedPollRetries, a2.AllowedPollRetries)
//
//	return true
//}
//
//BB := NewMemoryBB()
//
//spec := DefaultAgentSpec("test-agent")
//a := NewGoAgent(BB, &spec)
//
//a.SetAttributes(sample_attributes)
//
//// Test yaml
//f_yaml, err := os.Create("test-agent.yaml")
//is.NoErr(err)
//defer cleanUpTempFile(f_yaml)
//
//data, err := yaml.Marshal(a)
//is.NoErr(err)
//t.Logf("YAML Agent Encoding: \n%s", string(data))
//
//err = yaml.NewEncoder(f_yaml).Encode(a)
//is.NoErr(err)
//
//f_yaml.Seek(0, 0) // Reset the file pointer
//
//a_dec := &GoAgent{}
//err = yaml.NewDecoder(f_yaml).Decode(a_dec)
//is.NoErr(err)
//
//checkKeyAgentConfigFields(a, a_dec)
//
//// Test json
//f_json, err := os.Create("test-agent.json")
//is.NoErr(err)
//defer cleanUpTempFile(f_json)
//
//json.NewEncoder(f_json).Encode(a)
//
//data, err = json.MarshalIndent(a, "", "  ")
//is.NoErr(err)
//t.Logf("JSON Agent Encoding: \n %s", string(data))
//
//a_dec_2 := &Agent{}
//f_json.Seek(0, 0) // Reset the file pointer
//err = json.NewDecoder(f_json).Decode(a_dec_2)
//is.NoErr(err)
//
//checkKeyAgentConfigFields(a, a_dec_2)

//}
