package core

import (
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

// Playing around with the idea of a Blackboard spec here. Right now, only the Mongo implementation
// of the Blackboard needs a spec to function properly.
type MongoBBSpec struct {
	Addr           string `yaml:"addr"`
	DatabaseName   string `yaml:"database"`
	CollectionName string `yaml:"collection"`
	Username       string `yaml:"username"`
	Password       string `yaml:"password"`
}

func NewMongoBBSpec(path string) *MongoBBSpec {
	b, err := os.ReadFile(path)
	if err != nil {
		log.Fatal("failed to read mongo blackboard spec: ", err)
	}

	spec := &MongoBBSpec{}

	if err = yaml.Unmarshal(b, spec); err != nil {
		log.Fatal("failed to unmarshal mongo blackboard spec: ", err)
	}

	return spec
}

// TODO: Implement some sort of Default spec in case a user doesn't want to specify one themselves.
// It would be nice if we could automatically launch a Docker container with Mongo running locally,
// but that'll take a little more engineering... Right now, we are going to force a user to provide
// a valid spec if they want to use MongoBB.
func DefaultMongoBBSpec() *MongoBBSpec {
	return &MongoBBSpec{
		Addr:           "",
		DatabaseName:   "",
		CollectionName: "",
		Username:       "",
		Password:       "",
	}
}

// Deal with potentially empty fields in spec
func (s *MongoBBSpec) ValidateSpec() {
	if s.Addr == "" {
		log.Fatal("missing MongoDB address in spec")
	}

	if s.DatabaseName == "" {
		if s.CollectionName != "" {
			log.Fatalf("MongoDB collection name '%s' given in spec without specifying a database", s.CollectionName)
		} else {
			s.DatabaseName = "core db"
			s.CollectionName = fmt.Sprintf("blackboard %s", time.Now().String())
		}
	} else {
		if s.CollectionName == "" {
			s.CollectionName = fmt.Sprintf("blackboard %s", time.Now().String())
		}
	}

	if s.Username == "" || s.Password == "" {
		log.Fatal("missing username or password for MongoDB instance")
	}
}
