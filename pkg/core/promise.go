package core

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"
)

// Thoughts:
// Users should ONLY interact with the Attributes structure
//
// Users should never interact directly with the *Promise structures
// No "promise set" (without a reeeeally good reason)

type SafePromise struct {
	*Promise
	mu sync.Mutex
}

func NewSafePromise(p *Promise) *SafePromise {
	return &SafePromise{
		Promise: p,
		mu:      sync.Mutex{},
	}
}

// Anything that modifies a promise must be thread safe
func (s *SafePromise) Update(p *Promise) {
	s.mu.Lock()
	defer s.mu.Unlock()

	//s.State = p.State
	//
	//s.Data = p.Data

	s.Promise = p
}

func fetchData(link string) ([]byte, error) {
	resp, err := http.Get(link)
	if err != nil {
		return nil, errors.New("http promise data error: " + err.Error())
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New("http response read error: " + err.Error())
	}

	return data, nil
}

func extractData(p *Promise) ([]byte, error) {

	if p.Contents == nil {
		return make([]byte, 0), nil
	}

	switch p.Contents.(type) {
	case *Promise_Data:
		return p.GetData(), nil
	case *Promise_Link:
		return fetchData(p.GetLink())
	default:
		//return nil, errors.New("promise data is empty")
		return nil, fmt.Errorf("promise data is empty: %T %v", p.Contents, p.Contents)
	}

}

// TODO: Consider renaming?
// TODO: Bug: always holds for at least 1 second before
//
//	executing; poor form.
func (s *SafePromise) Await() chan interface{} {

	ch := make(chan interface{})

	go func() {
		for {
			select {
			case <-time.After(1 * time.Second):

				s.mu.Lock()

				switch s.State {
				case Promise_Fulfilled:
					// HOW TO HANDLE THIS ERROR?
					data, err := extractData(s.Promise)
					if err != nil {
						panic("this is an unhandled error condition." + err.Error())
					}
					ch <- data
					s.mu.Unlock()
					return
				case Promise_Final:
					// HOW TO HANDLE THIS ERROR?
					data, err := extractData(s.Promise)
					if err != nil {
						panic("this is an unhandled error condition." + err.Error())
					}
					ch <- data

					s.mu.Unlock()
					return
				case Promise_Empty:
					s.mu.Unlock()
					continue
				case Promise_Pending:
					s.mu.Unlock()
					continue
				}
			}
		}
	}()

	return ch
}

func EmptyPromise() *Promise {
	return &Promise{
		State: Promise_Pending,
	}
}

// Resolve will be used to sanity check that specified "source" actually exists at config load time
// We do not want to allow users to quietly wait to discover typos in their configs
func (p *Promise) Resolve(agents map[string]*Agent) (success bool, source *Agent) {

	success = false

	a, exists := agents[p.Source]
	if !exists {
		success = false
		source = nil
		return
	}

	success = true
	source = a

	return
}

// Satisfies returns true if promise fulfills the potential match.
// "*" should match on any. TODO: fields other than source.
func (p *Promise) Satisfies(potential_match *Promise) bool {

	data_id_match := (p.Name == potential_match.Name) || (potential_match.Name == "*")
	prom_type_match := (p.Type == potential_match.Type) || (potential_match.Type == "*")
	source_match := (p.Source == potential_match.Source) || (potential_match.Source == "*")

	return data_id_match && prom_type_match && source_match
}

// //////////////////////////////////////
// This parser will likely need to be "hardened" over time.  Here, I'm just going for a very basic initial
// implementation to get something working.
// //////////////////////////////////////
//
// How to represent data depenedencies between agents (i.e. edges in the knowledge graph)?
//
// Promise(CarinaBoundingBox Result from AgentName)
//
// Promise(WHAT as RESULT_TYPE from PROVIDING_AGENT)
//
// attribute_name: Promise(DataIdentifier, result type (result, partialresult), providing_agent)
//
// Do we need to require Data/Result identifiers.  That does sort of
// add implicit "knowledge of" other agents (or at least the data that
// will be provided)
//
// We use an approach similar to net/url.URL.Parse
// Valid raw_promises should be of the form "Promise(WHAT as RESULT_TYPE from PROVIDING_AGENT)"
// We also support without the "Promise(...)" wrapper:  WHAT as RESULT_TYPE from PROVIDING_AGENT
func ParsePromise(raw_promise string) (*Promise, error) {

	promise := stripPromiseWrapper(raw_promise)

	// TODO: Revisit this regex and quadruple check that it's doing what we want.
	//       Wildcarding is a little limited right now, but mostly appears to be functioning.
	//       That being said, I can foresee a lot of edge cases potentially.
	pattern := regexp.MustCompile(`(?P<data_id>[\w-*]+)\s+as\s+(?P<data_type>[\w-*]+)\s+from\s+(?P<providing_agent>\w+|\*)`)
	result := pattern.FindStringSubmatch(promise)

	// TODO: Return error if promise fails to parse, but starts with the keyword "Promise"

	if result == nil {
		return nil, fmt.Errorf("invalid promise string (did not match): %s", promise)
	}

	p := EmptyPromise()
	p.Name = strings.TrimSpace(result[1])
	p.Type = strings.TrimSpace(result[2])
	p.Source = strings.TrimSpace(result[3])

	if p.Source == "" {
		return nil, fmt.Errorf("invalid promise string (empty source): %s", promise)
	}

	return p, nil
}

func stripPromiseWrapper(raw_string string) string {

	promise_string, _ := strings.CutPrefix(raw_string, "Promise(")
	promise_string, _ = strings.CutSuffix(promise_string, ")")

	return promise_string
}
