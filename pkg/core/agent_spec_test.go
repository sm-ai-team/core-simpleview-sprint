package core

import (
	"testing"

	"github.com/matryer/is"
)

func TestAgentNameValidation(t *testing.T) {

	is := is.New(t)

	test_name := "my-test-agent-1234"
	bad_name := "129my-teWEIJsdsf-0ent-1234"
	another_bad_name := "1234_my-teWEIJsdsf-0ent-1234"

	ok, err := ValidateAgentName(test_name)
	is.True(ok)
	is.NoErr(err)

	ok, err = ValidateAgentName(bad_name)
	is.Equal(ok, false)
	is.True(err != nil)

	ok, err = ValidateAgentName(another_bad_name)
	is.Equal(ok, false)
	is.True(err != nil)

}

//func TestDecodeAgentSpec(t *testing.T) {
//
//	is := is.New(t)
//
//	test_file := "../../test-files/ping-pong.yaml"
//
//	
//
//	err := curr_config.Load(test_file)
//	is.NoErr(err)
//	
//	
//}
