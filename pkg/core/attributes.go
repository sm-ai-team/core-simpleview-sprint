package core

import (
	"errors"
	"strconv"
)

var FulfillmentError = errors.New("promise fulfillment error")

// We guarantee thread safety via two mechisms
// 1. Attributes's promises map and attributes map are ONLY written during initialization (single-threaded)
// 2.
type Attributes struct {
	promises   map[string]*SafePromise
	attributes map[string]string
}

// Return ready-to-use attributes structure for given spec Because
// structure must be initialized, and we do not provide any mechanism
// to update, attributes should be consider runtime immutable.
func NewAttributes(raw_attr_map map[string]string) *Attributes {

	attrs := &Attributes{
		promises:   make(map[string]*SafePromise),
		attributes: make(map[string]string),
	}

	// Divide incoming map into attributes and promises
	for attr_name, attr_val := range raw_attr_map {

		// Check for promise
		p, err := ParsePromise(attr_val)

		if err == nil {
			attrs.promises[attr_name] = NewSafePromise(p)
			continue
		}

		// If not a promise, save as attribute
		attrs.attributes[attr_name] = attr_val
	}

	return attrs
}

// This is a hacky function that should not be in the API in the long run.
// It is currently used by the Controller to do graph dependency resolution.
// We should really keep it to just the controller (or similar)
func (a *Attributes) GetPromises() map[string]*SafePromise {
	return a.promises
}

func (a *Attributes) NumPromises() int {
	return len(a.promises)
}

func (a *Attributes) HasPromises() bool {
	return (len(a.promises) > 0)
}

func (a *Attributes) AllFulfilled() bool {
	for _, v := range a.promises {
		if v.State == Promise_Pending {
			return false
		}
	}

	return true
}

func (a *Attributes) ProcessResult(r *Result) {
	incoming_promise := r.Promise
	for _, v := range a.promises {
		if incoming_promise.Satisfies(v.Promise) {
			v.Update(incoming_promise)
		}
	}
}

func (a *Attributes) MustGetInt(key string) int64 {
	val, err := a.value(false, key)
	if err != nil {
		panic(err)
	}
	return val.(int64)
}

func (a *Attributes) MustGetFloat(key string) float64 {
	val, err := a.value(false, key)
	if err != nil {
		panic(err)
	}
	return val.(float64)
}

func (a *Attributes) MustGetString(key string) string {
	val, err := a.value(false, key)
	if err != nil {
		panic(err)
	}
	return val.(string)
}

func (a *Attributes) MustGetBytes(key string) []byte {
	val, err := a.value(false, key)
	if err != nil {
		panic(err)
	}
	return val.([]byte)
}

func (a *Attributes) Value(key string) (interface{}, error) {
	return a.value(false, key)
}

func (a *Attributes) AwaitValue(key string) (interface{}, error) {
	return a.value(true, key)
}

// Internal accessor method to avoid repeating myself.
// All convenience accessors Value and AwaitValue should expose this
// functionality to users with the required parameters set as needed.
//
// The choice to return interface is key here as it force the user to
// recognize that type is important.
// We attempt to parse the values in the following order Promise->int->float64->string
// We return the underlying string if we can't figure out anything else.
func (a *Attributes) value(await bool, key string) (interface{}, error) {

	// First check if promise
	if safe_prom, ok := a.promises[key]; ok {
		if await {
			//This will block until result is available
			// TODO: What data type does Await return??
			return <-safe_prom.Await(), nil
		} else {
			// TODO: This won't work in its current form
			if safe_prom.State == Promise_Pending {
				return nil, FulfillmentError
			} else {
				// John's note: This is kind of awkward to use the Await
				// accessor, however it's a safe and protected way to
				// get data from a SafePromise.  The 'if' statement here
				// should allow the behavior to be consistent with how
				// 'value' should be used.
				return <-safe_prom.Await(), nil
			}
		}
	}

	// If not a promise attempt to return first
	// as number, then finally just return the string
	// if we can't figure it out.
	attr, ok := a.attributes[key] // string
	if !ok {
		return nil, errors.New("attribute not found: " + key)
	}

	// Attempt to parse as 64 bit integer
	if v, err := strconv.ParseInt(attr, 10, 64); err == nil {
		return v, nil
	}

	// Attempt to parse as 64 bit float
	if v, err := strconv.ParseFloat(attr, 64); err == nil {
		return v, nil
	}

	// Return as string if didn't decode as anything else
	return attr, nil
}
