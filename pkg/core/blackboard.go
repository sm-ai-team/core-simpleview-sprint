package core

// Blackboard should really embed the object store API

// Errors are added here (possibly prematurely) to support other data
// storage systems that may be able to encounter faults (e.g., DB
// connection drop)
type Blackboard interface {
	WriteMessage(*Message) error
	GetMessage(int) (*Message, error)
	GetMessages(int, bool) ([]*Message, error)
	GetResults(int) ([]*Message, error)
	ResultSizeLimit() int
	Len() int
	Shutdown()
}
