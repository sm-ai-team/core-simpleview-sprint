package core

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/hoffman-lab/core/pkg/config"
	"golang.org/x/crypto/ssh/terminal"
)

//type Message struct {
//	ID       int         `json:"id"`
//	Index    int         `json:"index"`
//	Type     MessageType `json:"type"`
//	Source   string      `json:"source"`
//	Data     []byte      `json:"data,omitempty"`
//	Priority int         `json:"priority"` // Should this be an enumerated type??
//	TimeSent time.Time   `json:"timesent,omitempty"`
//	TimeRecv time.Time   `json:"timerecv,omitempty"`
//}

//func (m *Message) String() string {
//	return fmt.Sprintf("%06d - %11s - %12s - %s - %s", m.Index, m.Type, m.Source, string(m.Data), m.Latency())
//}

func (m *Message) Latency() time.Duration {
	s := m.TimeSent.AsTime()
	r := m.TimeRecv.AsTime()
	return r.Sub(s)
}

// The functions below are intended for pretty formatting our strings for terminal output and are
// not expected to generally function outside of the narrow context they're defined for.
func PrettyTruncateString(s string, length_limit int) string {
	if len(s) > length_limit {
		return s[0:length_limit-3] + "..."
	}
	return s
}

type Alignment int

const (
	AlignLeft Alignment = iota
	AlignCenter
	AlignRight
)

func PadToWidth(s string, width int, alignment Alignment) string {
	var fmt_string string

	switch alignment {
	case AlignLeft:
		fmt_string = "%-" + strconv.Itoa(width) + "s"

	case AlignRight:
		fmt_string = "%" + strconv.Itoa(width) + "s"
	case AlignCenter:
		// TODO: not yet implemented
		fmt_string = "%s"
	}

	return fmt.Sprintf(fmt_string, s)
}

var header_printed = false

func (m *Message) TerminalString() string {

	output_width := config.Globals.OutputWidth

	// If 0, attempt to autodetect
	if output_width == 0 {

		// This may be expensive to compute here... not sure
		fd := int(os.Stdin.Fd())
		output_width, _, _ = terminal.GetSize(fd)

	} else if output_width < 0 {
		output_width = 1_000_000 // 1MB (gonna look so bad)
	}

	index_width := 10
	source_width := 20
	type_width := 16
	latency_width := 10

	msg_type, _ := strings.CutPrefix(fmt.Sprintf("%T", m.Contents), "*core.Message_")

	// In the long run, this block should be moved elsewhere, but for now it's doing its job.
	if !header_printed {

		fmt.Println("================================================================================")
		index_string := PadToWidth("Index", index_width, AlignLeft)
		source_string := PadToWidth("Source", source_width, AlignLeft) //PrettyTruncateString(m.Source, source_width)
		type_string := PadToWidth("Type", type_width, AlignLeft)       //PrettyTruncateString(msg_type, type_width)
		latency_string := PadToWidth("Latency", latency_width, AlignLeft)
		content_string := "Msg Contents"

		final_strings := []string{index_string, source_string, type_string, latency_string, content_string}

		str := strings.Join(final_strings, " ")

		if len(str) > output_width && output_width > 3 {
			fmt.Println("!!!!!!!!!!!!!!!!", output_width)
			str = str[0:output_width-3] + "..."
		}

		fmt.Println(str)
		fmt.Println("================================================================================")

		header_printed = true
	}

	index_string := fmt.Sprintf("%08d", m.Index)
	source_string := PrettyTruncateString(m.Source, source_width)
	type_string := PrettyTruncateString(msg_type, type_width)
	latency_string := PrettyTruncateString(m.Latency().String(), latency_width)

	index_string = PadToWidth(index_string, index_width, AlignLeft)
	source_string = PadToWidth(source_string, source_width, AlignLeft)
	type_string = PadToWidth(type_string, type_width, AlignLeft)
	latency_string = PadToWidth(latency_string, latency_width, AlignLeft)

	final_strings := []string{index_string, ColorWrap(m.Source, source_string), type_string, latency_string, fmt.Sprintf("%v", m.Contents)}

	str := strings.Join(final_strings, " ")

	if len(str) > output_width && output_width > 3 {
		return str[0:output_width-3] + "..."
	}

	return str
}
