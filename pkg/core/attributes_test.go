package core

import (
	"testing"
	"time"

	"github.com/matryer/is"
)

var test_data = map[string]string{
	"str-key":   "val1",
	"int-key":   "24",
	"float-key": "1.01",
	"prom-key":  "Promise(data as datatype from dataprovider)",
}

// Validate accessors and that we can read stuff through the intended user interface
func TestAttributes(t *testing.T) {

	is := is.New(t)

	attrs := NewAttributes(test_data)

	// No panics (I don't really know if we should even expose this
	// "Must*" style interface. Seems like there are cases where stuff
	// is pretty hard-coded and you don't need to muck around with error
	// handling for every attribute you want to read.  E.g., you know it's an Int,
	// why should I doubt you?  I should just let you access how ya want.
	// That being said, we could and maybe should go through the trouble of error
	// handling.
	i := attrs.MustGetInt("int-key")
	is.Equal(i, int64(24))

	f := attrs.MustGetFloat("float-key")
	is.Equal(f, float64(1.01))

	s := attrs.MustGetString("str-key")
	is.Equal(s, "val1")

	// check that we get an error if we try to read a value that
	// doesn't exist
	_, err := attrs.Value("doesnt-exist")
	is.True(err != nil)

}

func TestAwaitValue(t *testing.T) {

	is := is.New(t)

	a := NewAttributes(test_data)

	go func() {

		time.Sleep(1000 * time.Millisecond)

		// Pretend a result came in
		a.ProcessResult(
			&Result{
				Promise: &Promise{
					Name:   "data",
					Type:   "datatype",
					Source: "dataprovider",
					State:  Promise_Fulfilled,
					Contents: &Promise_Data{
						Data: []byte("helloworld"),
					},
				},
			},
		)

	}()

	data, err := a.AwaitValue("prom-key")
	is.NoErr(err)
	is.Equal(data.([]byte), []byte("helloworld"))

}
