package core

import (
	"testing"
	"time"

	"github.com/matryer/is"
)

func TestAgentStatus(t *testing.T) {
	is := is.New(t)

	ass := NewAgentStatusSet()

	name := "test-agent"
	ass.NewAgentStatus(name)

	tmp := ass.GetStatus(name)
	is.True(tmp != nil)
	is.Equal(tmp.State, StatusUpdate_Unknown)

	ass.UpdateState(name, StatusUpdate_OK)
	is.Equal(tmp.State, StatusUpdate_OK)

	now := time.Now()
	ass.UpdatePing(name, now)
	is.Equal(tmp.LastPing, now)

	test_msg := "agent is working"
	sm := &StatusUpdate{
		State:   StatusUpdate_Working,
		Message: test_msg,
	}

	ass.UpdateStatus(name, sm)
	is.Equal(tmp.LastStatusUpdate.State, sm.State)
	is.Equal(tmp.State, sm.State)
	is.Equal(tmp.LastStatusUpdate.Message, sm.Message)

	tmp = ass.GetStatus("doesnt-exist")
	is.True(tmp == nil) // Non-existent agent status in monitor should return nil
}
