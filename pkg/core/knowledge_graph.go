// This data structure represents both the underlying structure
// and current state of an "agent bundle"
//
// We build off of gonum's interfaces so that we can leverage
// any pre-existing GoNum graph tools.
//
// In this implementation, there is a 1-1 correspondence between
// agents and nodes, as well as a 1-1 correspondence between

package core

import (
	"fmt"
	"os"
	"time"

	"os/exec"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/simple"
)

// Knowedge Graph is a *directed graph*
//
// Knowledge graph provides information about the relationships between agents.
// It uses a gonum WeightedDirectedGraph under the hood. The API is intended
// to be higher than the graph level, but we can also add that functionality
// if requested or if it turns out to be valuable/useful.
type KnowledgeGraph struct {
	*simple.WeightedDirectedGraph
	nameLookup map[string]int64
}

func NewKnowledgeGraph() *KnowledgeGraph {
	return &KnowledgeGraph{
		WeightedDirectedGraph: simple.NewWeightedDirectedGraph(1.0, 0.0),
		nameLookup:            make(map[string]int64, 0),
	}
}

func (kg *KnowledgeGraph) MarkStarted(source string) {
	agt := kg.Node(kg.nameLookup[source]).(*AgentNode)
	agt.started = true
}

func (kg *KnowledgeGraph) UpdateState(source string, status *StatusUpdate) {
	node_id, ok := kg.nameLookup[source]
	if !ok {
		return
	}

	node := kg.Node(node_id).(*AgentNode)
	node.lastStatus = status
}

func (kg *KnowledgeGraph) UpdatePing(source string, recv time.Time) {
	node_id, ok := kg.nameLookup[source]
	if !ok {
		return
	}

	node := kg.Node(node_id).(*AgentNode)
	node.lastPing = recv
}

// UpdateEdges iterates over all edges updating all promises fulfilled
// by the incoming promise
func (kg *KnowledgeGraph) UpdateEdges(promise *Promise) {
	edges := kg.Edges()

	for edges.Next() {
		it := edges.Edge()
		pe := it.(*PromiseEdge)

		if promise.Satisfies(pe.Promise) {
			pe.Promise = promise
		}
	}
}

// AllCompleted iterates over all nodes in the knowledge graph
// checking for completed status. Returns true if all agents
// have reported status completed.
func (kg *KnowledgeGraph) AllCompleted() bool {
	nodes := kg.Nodes()

	for nodes.Next() {
		it := nodes.Node()
		agt := it.(*AgentNode)
		if agt.lastStatus.State != StatusUpdate_Completed {
			return false
		}
	}

	return true
}

// Parse slice of agent specs into a knowledge graph
func (kg *KnowledgeGraph) FromAgentBundle(agents map[string]*AgentSpec) {

	// Create nodes for each agent
	idx := 0
	for name, spec := range agents {
		kg.nameLookup[name] = int64(idx)
		kg.AddNode(&AgentNode{
			id:    int64(idx),
			spec:  spec,
			dotID: spec.Name,
			lastStatus: &StatusUpdate{
				State: StatusUpdate_Unknown,
			},
		})
		idx++
	}

	// Create edges if we find promises
	for _, spec := range agents {

		for _, attr_val := range spec.Attributes {
			p, err := ParsePromise(attr_val)
			if err != nil {
				continue
			}

			// Identify who (if anyone) will provide the promised data
			//
			// PROBLEM/TODO: We have no way of determining "*"
			//        providers, we cannot draw those links.  It would
			//        be nice if users could maybe "decorate" an agent
			//        with a hint about what promises it will fullfill
			//        "<dataname> as <datatype>".  This would not be a
			//        hard requirement, but would at the very least
			//        allow for better visualizations.
			_, ok := agents[p.Source]
			if !ok {
				continue
			}

			from, to := kg.Node(kg.nameLookup[p.Source]), kg.Node(kg.nameLookup[spec.Name])
			pe := kg.NewPromiseEdge(from, to, float64(1.0), p)
			kg.SetWeightedEdge(pe)

		}
	}
}

func (kg *KnowledgeGraph) NewPromiseEdge(from, to graph.Node, weight float64, p *Promise) *PromiseEdge {

	edge := kg.NewWeightedEdge(from, to, 1.0)

	return &PromiseEdge{
		Promise:      p,
		WeightedEdge: edge.(simple.WeightedEdge),
	}

}

func (kg *KnowledgeGraph) ToDot(filename string) error {
	data, err := dot.Marshal(kg, "knowledge-graph", "", "  ")
	if err != nil {
		return fmt.Errorf("failed to marshal graph to dot: %s", err)
	}

	f, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to open dot output: %s", err)
	}

	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return fmt.Errorf("write failed: %s", err)
	}

	return nil
}

func (kg *KnowledgeGraph) ToPNG(filename string) error {

	// Write DOT intermediate file
	err := kg.ToDot(filename + ".dot")
	if err != nil {
		return nil
	}

	// shell out for graphviz
	cmd := exec.Command("dot", "-Tpng", filename+".dot", "-o", filename)
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to save png: %s", err)
	}

	return nil
}

func (kg *KnowledgeGraph) ToSVG(filename string) error {

	// Write DOT intermediate file
	err := kg.ToDot(filename + ".dot")
	if err != nil {
		return nil
	}

	// shell out for graphviz
	cmd := exec.Command("dot", "-Tsvg", filename+".dot", "-o", filename)
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to save svg: %s", err)
	}

	return nil
}

func (kg *KnowledgeGraph) ToPDF(filename string) error {

	// Write DOT intermediate file
	err := kg.ToDot(filename + ".dot")
	if err != nil {
		return nil
	}

	// shell out for graphviz
	cmd := exec.Command("dot", "-Tpdf", filename+".dot", "-o", filename, "-Gdpi=256")
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to save pdf: %s", err)
	}

	return nil
}

// CanStart returns true if all incoming promises to agt have been
// fulfilled.
func (kg *KnowledgeGraph) CanStart(agt graph.Node) bool {

	// Get all nodes that can reach agt (note: knowledge graph is not
	// a dependency graph; but the same graph, with all edges flipped)
	to := kg.To(agt.ID())
	for to.Next() {
		it := to.Node()

		// Get Edge (PromiseEdge); determine if all are fulfilled
		e := kg.Edge(it.ID(), agt.ID())

		pe := e.(*PromiseEdge)
		if pe.State == Promise_Pending {
			return false
		}
	}
	return true
}

func (kg *KnowledgeGraph) GetStartableSpecs() []*AgentSpec {

	startable := make([]*AgentSpec, 0, kg.Nodes().Len())

	// Iterate over all nodes to determine if its
	// startable. This could be made more efficient, but
	// I don't think our graphs are going to be so large
	nodes := kg.Nodes()
	for nodes.Next() {
		it := nodes.Node()

		if kg.CanStart(it) && !it.(*AgentNode).Started() {
			agt := it.(*AgentNode)
			spec := agt.spec
			startable = append(startable, spec)
		}
	}

	return startable
}
