package core

import (
	"context"
	"fmt"
	"slices"
	"sync/atomic"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/hoffman-lab/core/pkg/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// Structure for unmarshaling BSON
type MessageStringJSON struct {
	JSONMessage []byte `bson:"json_message"`
}

// Concrete implementation of Blackboard interface backed by MongoDB database
var _ Blackboard = &MongoBB{}

type MongoBB struct {
	Spec       *MongoBBSpec
	Database   *mongo.Database
	Collection *mongo.Collection
	client     *mongo.Client
	counter    *atomic.Uint64
	length     int32 // Not sure if this is a necessary field
}

// TODO: I think we may need to return an error from the NewMongoBB since
// it can fail under fairly reasonable circumstances (i.e., connection issues,
// incorrect credentials, etc.
func NewMongoBB(spec *MongoBBSpec) *MongoBB {
	// Generate client options
	credential := options.Credential{
		Username: spec.Username,
		Password: spec.Password,
	}

	clientOpts := options.Client().ApplyURI(spec.Addr).SetAuth(credential)

	// Connect to the project
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		log.Fatalf("failed to fetch client from Mongo URI: %v", err)
	}

	// Check whether the collection exists and create it if it does not
	db := client.Database(spec.DatabaseName)
	coll_names, err := db.ListCollectionNames(ctx, bson.D{})
	if err != nil {
		log.Fatalf("failed to retrieve collections for database %s", spec.DatabaseName)
	}

	ok := slices.Contains(coll_names, spec.CollectionName)
	if ok {
		log.Fatalf("collection '%s' already exists: please enter a new collection name in spec", spec.CollectionName)
	}

	if err := db.CreateCollection(ctx, spec.CollectionName); err != nil {
		log.Fatalf("failed to create collection '%s' for database '%s': %v", spec.CollectionName, spec.DatabaseName, err)
	}

	coll := db.Collection(spec.CollectionName)

	m := &MongoBB{
		Spec:       spec,
		Database:   db,
		Collection: coll,
		client:     client,
		counter:    &atomic.Uint64{},
		length:     int32(0),
	}

	return m
}

func (m *MongoBB) ResultSizeLimit() int {
	return 1_000_000 // Should be around 16 MB for one BSON-encoded message
}

func (m *MongoBB) Len() int {
	count, err := m.Collection.CountDocuments(context.TODO(), bson.D{})
	if err != nil {
		log.Error("failed to calculate the length of the MongoBB instance")
		return 0
	}

	return int(count)
}

func (m *MongoBB) WriteMessage(msg *Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// Not sure if this creates race conditions... Do we need a mutex? -JG
	m.length = int32(m.counter.Add(1))
	msg.Id = m.length
	msg.Index = m.length
	msg.TimeRecv = timestamppb.Now()

	msg_json, err := protojson.Marshal(msg)
	if err != nil {
		return fmt.Errorf("failed to encode proto Message to json: %v", err)
	}

	// Check the type of the Message for querying purposes
	// This switch statement isn't ideal, but it works for now...

	// Only print non-ping messages; controls BB stdout
	switch msg.Contents.(type) {
	case *Message_Ping:
		if config.Globals.ShowPings {
			fmt.Println(msg.TerminalString())
		}
	default:
		fmt.Println(msg.TerminalString())
	}

	// Manually set the content type; controls message parsing
	var content_type string
	switch msg.Contents.(type) {
	case *Message_CreateAgent:
		content_type = "createAgent"
	case *Message_Hello:
		content_type = "hello"
	case *Message_Goodbye:
		content_type = "goodbye"
	case *Message_Log:
		content_type = "log"
	case *Message_Ping:
		content_type = "ping"
	case *Message_Result:
		content_type = "result"
	case *Message_StatusUpdate:
		content_type = "statusUpdate"
	case *Message_Halt:
		content_type = "halt"
	case *Message_HaltAndCatchFire:
		content_type = "haltAndCatchFire"
	}

	// Create BSON document for Message
	msg_bson := bson.D{
		{Key: "id", Value: msg.Id},
		{Key: "index", Value: msg.Index},
		{Key: "source", Value: msg.Source},
		{Key: "priority", Value: msg.Priority},
		{Key: "contents", Value: content_type},
		{Key: "json_message", Value: string(msg_json)},
	}

	if len(msg_bson) > m.ResultSizeLimit() {
		// TODO: have to use MinIO for things we can't store in BSON. Thoughts @John? -JG
		log.Info("message exceeds BSON size limit")
	}

	_, err = m.Collection.InsertOne(ctx, msg_bson)
	if err != nil {
		return fmt.Errorf("failed to write message to MongoDB blackboard: %v", err)
	}

	return nil
}

func (m *MongoBB) GetMessage(n int) (*Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	filter := bson.D{{Key: "index", Value: n}}

	sr := m.Collection.FindOne(ctx, filter)
	if sr.Err() != nil {
		return nil, sr.Err()
	}

	msg_json := &MessageStringJSON{}
	if err := sr.Decode(msg_json); err != nil {
		return nil, fmt.Errorf("could not decode JSON field of Mongo message: %v", err)
	}

	msg := &Message{}
	if err := protojson.Unmarshal(msg_json.JSONMessage, msg); err != nil {
		return nil, fmt.Errorf("failed to unmarshal Mongo message into Message: %v", err)
	}

	return msg, nil
}

func (m *MongoBB) GetMessages(starting_at int, filter_pings bool) ([]*Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	var messages []*Message
	if starting_at < 0 {
		starting_at = 0
	} else if starting_at >= m.Len() {
		return messages, nil
	}

	pipeline := mongo.Pipeline{
		bson.D{{Key: "$skip", Value: starting_at}},
		bson.D{{Key: "$sort", Value: bson.D{{Key: "index", Value: 1}}}},
	}
	if filter_pings {
		pipeline = append(pipeline, bson.D{{Key: "$match", Value: bson.D{{Key: "$nor", Value: []bson.D{{{Key: "contents", Value: "ping"}}}}}}})
	}

	cursor, err := m.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}

	var results []bson.D
	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}

	for _, result := range results {
		msg_byte, err := bson.Marshal(result)
		if err != nil {
			return nil, fmt.Errorf("could not marshal BSON from message '%v': %v", result, err)
		}

		msg_json := &MessageStringJSON{}
		if err = bson.Unmarshal(msg_byte, msg_json); err != nil {
			return nil, fmt.Errorf("could not unmarshal BSON into message: %v", err)
		}

		msg := &Message{}
		if err = protojson.Unmarshal(msg_json.JSONMessage, msg); err != nil {
			return nil, fmt.Errorf("could not unmarshal JSON into message: %v", err)
		}

		messages = append(messages, msg)
	}

	return messages, nil
}

func (m *MongoBB) GetResults(starting_at int) ([]*Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	var messages []*Message
	if starting_at < 0 {
		starting_at = 0
	} else if starting_at >= m.Len() {
		return messages, nil
	}

	pipeline := mongo.Pipeline{
		bson.D{{Key: "$match", Value: bson.D{{Key: "contents", Value: "result"}}}},
	}

	cursor, err := m.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}

	var results []bson.D
	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}

	for _, result := range results {
		msg_byte, err := bson.Marshal(result)
		if err != nil {
			return nil, fmt.Errorf("could not marshal BSON from message '%v': %v", result, err)
		}

		msg_json := &MessageStringJSON{}
		if err = bson.Unmarshal(msg_byte, msg_json); err != nil {
			return nil, fmt.Errorf("could not unmarshal BSON into message: %v", err)
		}

		msg := &Message{}
		if err = protojson.Unmarshal(msg_json.JSONMessage, msg); err != nil {
			return nil, fmt.Errorf("could not unmarshal JSON into message: %v", err)
		}

		messages = append(messages, msg)
	}
	return messages, nil
}

func (m *MongoBB) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	fmt.Println("================================================================================")

	// Disconnect the client
	if err := m.client.Disconnect(ctx); err != nil {
		log.Error("failed to disconnect Mongo blackboard client: ", err)
	}

	log.Info("blackboard shutting down")
}
