package core

type DispatchMonitor struct {
	dispGroup *DispatchGroup
	currIndex int
}

type DispatchGroup struct {
	agentGroups []map[string]*AgentSpec
	completed   chan struct{}
}

func NewDispatchMonitor(group_map []map[string]*AgentSpec) *DispatchMonitor {
	disp_grp := &DispatchGroup{
		agentGroups: group_map,
		completed:   make(chan struct{}),
	}

	return &DispatchMonitor{
		dispGroup: disp_grp,
		currIndex: -1,
	}
}

func (d *DispatchMonitor) GetAllAgentGroups() []map[string]*AgentSpec {
	return d.dispGroup.agentGroups
}

func (d *DispatchMonitor) GetCurrentAgentGroup() map[string]*AgentSpec {
	return d.dispGroup.agentGroups[d.currIndex]
}

func (d *DispatchMonitor) SignalCompleted() {
	d.dispGroup.completed <- struct{}{}
}

func (d *DispatchMonitor) Done() chan struct{} {
	return d.dispGroup.completed
}

func (d *DispatchMonitor) Next() map[string]*AgentSpec {
	d.currIndex++
	return d.dispGroup.agentGroups[d.currIndex]
}
