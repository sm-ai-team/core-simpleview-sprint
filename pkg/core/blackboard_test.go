package core

import (
	"context"
	"os"
	"testing"

	"github.com/matryer/is"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	MONGOBB_ADDR     = "mongodb+srv://sm-core-go.ojatfav.mongodb.net"
	MONGOBB_DATABASE = "test_database"
)

func TestMemoryBlackboard(t *testing.T) {

	is := is.New(t)

	msg_0 := &Message{
		Id: 0,
		Contents: &Message_Result{
			Result: &Result{
				Promise: &Promise{
					Name: "test0",
				},
			},
		},
	}

	msg_1 := &Message{
		Id: 1,
		Contents: &Message_Result{
			Result: &Result{
				Promise: &Promise{
					Name: "test1",
				},
			},
		},
	}

	msg_2 := &Message{
		Id: 2,
	}

	m := NewMemoryBB(nil)

	// Test for WriteMessage
	err := m.WriteMessage(msg_0)
	is.NoErr(err)

	err = m.WriteMessage(msg_1)
	is.NoErr(err)

	err = m.WriteMessage(msg_2)
	is.NoErr(err)

	// Test for GetMessages
	msg_list, _ := m.GetMessages(0, false)
	is.Equal(len(msg_list), 3)

	msg_list, _ = m.GetMessages(1, false)
	is.Equal(len(msg_list), 2)

	msg_list, _ = m.GetMessages(2, false)
	is.Equal(len(msg_list), 1)

	msg_list, _ = m.GetMessages(3, false)
	is.Equal(len(msg_list), 0)

	msg_list, err = m.GetMessages(25, false)
	is.NoErr(err)
	is.Equal(len(msg_list), 0)

	// Test for GetResults
	msg_list, err = m.GetResults(1)
	is.NoErr(err)
	is.Equal(len(msg_list), 1)

	msg_list, err = m.GetResults(0)
	is.NoErr(err)
	is.Equal(len(msg_list), 2)

	msg_list, err = m.GetResults(-1)
	is.NoErr(err)
	is.Equal(len(msg_list), 2)

	msg_list, err = m.GetResults(100)
	is.NoErr(err)
	is.Equal(len(msg_list), 0)
}

func TestMongoBlackboard(t *testing.T) {

	if os.Getenv("CI_ENV") != "INTEGRATION" {
		t.Skip("skipping integration test (CI_ENV!=INTEGRATION)")
	}

	mongo_user := os.Getenv("MONGOUSER")
	mongo_pass := os.Getenv("MONGOPASS")

	is := is.New(t)

	spec := &MongoBBSpec{
		Addr:         MONGOBB_ADDR,
		DatabaseName: MONGOBB_DATABASE,
		Username:     mongo_user,
		Password:     mongo_pass,
	}
	spec.ValidateSpec()

	m := NewMongoBB(spec)

	msg_0 := &Message{
		Source:   "testing 1",
		Priority: int32(1),
		TimeSent: timestamppb.Now(),
		Contents: &Message_Ping{
			Ping: &Ping{},
		},
	}

	msg_1 := &Message{
		Source:   "testing 2",
		Priority: int32(1),
		TimeSent: timestamppb.Now(),
		Contents: &Message_Result{
			Result: &Result{
				Type: Result_Result,
				Promise: &Promise{
					Name:   "testing Name",
					Type:   "testing Type",
					Source: "testing Source",
					State:  Promise_Final,
					Contents: &Promise_Data{
						Data: []byte("testing 123"),
					},
				},
			},
		},
	}

	// Test for WriteMessage
	err := m.WriteMessage(msg_0)
	is.NoErr(err)

	err = m.WriteMessage(msg_1)
	is.NoErr(err)

	// Test for GetMessages
	messages, err := m.GetMessages(0, false)
	is.NoErr(err)
	is.Equal(len(messages), 2)

	messages, err = m.GetMessages(0, true)
	is.NoErr(err)
	is.Equal(len(messages), 1)

	messages, err = m.GetMessages(1, false)
	is.NoErr(err)
	is.Equal(len(messages), 1)

	messages, err = m.GetMessages(4, false)
	is.NoErr(err)
	is.Equal(len(messages), 0)

	// Test for GetResults
	results, err := m.GetResults(1)
	is.NoErr(err)
	is.Equal(len(results), 1)

	results, err = m.GetResults(100)
	is.NoErr(err)
	is.Equal(len(results), 0)

	// Delete the collection at the end
	err = m.Collection.Drop(context.TODO())
	is.NoErr(err)
}
