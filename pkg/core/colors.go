package core

var Reset = "\033[0m"
var Red = "\033[31m"
var Green = "\033[32m"
var Yellow = "\033[33m"
var Blue = "\033[34m"
var Purple = "\033[35m"
var Cyan = "\033[36m"
var Gray = "\033[37m"
var White = "\033[97m"

var colors = []string{
	//"\033[31m", // Red
	"\033[32m", // Green
	"\033[33m", // Yellow
	"\033[34m", // Blue
	"\033[35m", // Purple
	"\033[36m", // Cyan
	//"\033[37m", // Gray
	//"\033[97m", // White
}

// TODO: use RunnerTypes to lower visual importance of runner messages
var color_table = map[string]string{
	"controller": Gray,
	"go":         Gray,
	"local":      Gray,
}
var color_idx = 0

func GetNextColor() string {
	c := colors[color_idx]
	color_idx = (color_idx + 1) % len(colors)
	return c
}

func ColorWrap(name string, str string) string {
	c, ok := color_table[name]
	if !ok {
		c = GetNextColor()
		color_table[name] = c
	}
	return c + str + Reset
}
