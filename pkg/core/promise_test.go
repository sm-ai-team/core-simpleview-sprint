package core

import (
	"fmt"
	"testing"

	"github.com/matryer/is"
)

func TestPromiseParsing(t *testing.T) {

	is := is.New(t)

	// Verify that we strip a "promise" function-style wrapper correctly
	data_id := "DataTest"
	data_type := "Result"
	data_provider := "TestAgent"

	promise_content := fmt.Sprintf("%s as %s from %s", data_id, data_type, data_provider)
	wrapped_content := "Promise(" + promise_content + ")"

	is.Equal(stripPromiseWrapper(wrapped_content), promise_content)

	// Verify that we can parse a full promise
	_, err := ParsePromise(wrapped_content)
	is.NoErr(err)

	// Now put it through it's paces (don't have to handle everything to begin with, but all of these cases should eventually be correctly handled)
	// Best case: Pass
	promise := "Data1 as PartialResult from Agent1"
	_, err = ParsePromise(promise)
	is.NoErr(err)

	// extra leading whitespace (PASS)
	promise = "    Data1 as PartialResult from Agent1"
	_, err = ParsePromise(promise)
	is.NoErr(err)

	// Random whitespace between delimiters (PASS)
	promise = "    Data1    as  PartialResult           from  Agent1   "
	_, err = ParsePromise(promise)
	is.NoErr(err)

	// Missing "as" delimiters (FAIL)
	promise = "Data1  PartialResult           from  Agent1   "
	_, err = ParsePromise(promise)
	is.True(err != nil)

	// Missing "from" delimiters (FAIL)
	promise = "Data1 as PartialResult  Agent1   "
	_, err = ParsePromise(promise)
	is.True(err != nil)

	// Missing source agent (FAIL)
	promise = "Data1 as PartialResult from "
	_, err = ParsePromise(promise)
	is.True(err != nil)

	// Match asterix
	promise = "Data1 as PartialResult from *"
	_, err = ParsePromise(promise)
	is.NoErr(err)

	// Verify that the extracted values are what we expect them to be
	p, err := ParsePromise(promise_content)
	is.NoErr(err)

	is.Equal(p.Name, data_id)
	is.Equal(p.Type, data_type)
	is.Equal(p.Source, data_provider)

}

func TestPromise(t *testing.T) {

	is := is.New(t)

	p := &Promise{
		Name:   "hello",
		Type:   "greeting",
		Source: "agent-smith",
	}

	p_match := &Promise{
		Name:   "hello",
		Type:   "greeting",
		Source: "agent-smith",
	}

	tf := p.Satisfies(p_match)
	is.True(tf) // identical match

	p_match.Source = "*"

	tf = p.Satisfies(p_match)
	is.True(tf) // glob match

}
