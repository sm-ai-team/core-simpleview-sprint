package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"net/url"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func httpClient() *http.Client {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 20,
		},
		Timeout: 10 * time.Second,
	}
	return client
}

type HTTPBlackboard struct {
	Client *http.Client
	Addr   string
}

func NewHTTPBlackboard(addr string) (*HTTPBlackboard, error) {
	return &HTTPBlackboard{
		Client: httpClient(),
		Addr:   addr,
	}, nil
}

// This is uglier than how GoAgent does it
func (h *HTTPBlackboard) SayHello(spec *AgentSpec) error {

	// Add important information to the message
	msg := &Message{
		Source:   spec.Name,
		TimeSent: timestamppb.Now(),
		Contents: &Message_Hello{
			Hello: &Hello{
				Spec: spec,
			},
		},
	}

	return h.WriteMessage(msg)
}

// Ugh, these signatures are so ugly and ad hoc (source passed manually??)
// Should there be a 1-1 associate with agents and blackboard adapter instances.
// This is really just a *GO* HTTP blackboard client.  In the struct-based API, that I would
// prefer people use
func (h *HTTPBlackboard) CreateAgent(source string, spec *AgentSpec) {

}

func (h *HTTPBlackboard) PostAgentBundle(spec *AgentSpec, agnts map[string]*AgentSpec) error {

	//data, err := protojson.Marshal(agnts)
	data, err := json.Marshal(agnts) // This may not behave correctly since protojson is not working
	if err != nil {
		return err
	}

	msg := &Message{
		Source:   spec.Name,
		TimeSent: timestamppb.Now(),
		Priority: 5,
		Contents: &Message_Result{
			Result: &Result{
				Type: Result_Result,
				Promise: &Promise{
					Name:   spec.Name + "-agent-request",
					Type:   "agent-bundle",
					State:  Promise_Final,
					Source: spec.Name,
					Contents: &Promise_Data{
						Data: data,
					},
				},
			},
		},
	}

	return h.WriteMessage(msg)
}

// Transport layer handling (http) of the message
func (h *HTTPBlackboard) WriteMessage(msg *Message) error {

	url := url.URL{
		Scheme: "http",
		Host:   h.Addr,
		Path:   "/message",
	}

	// Inject important information:

	// Create the packet of data
	data, err := protojson.Marshal(msg)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, url.String(), bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	resp, err := h.Client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	_, err = io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func (h *HTTPBlackboard) GetMessage(idx int) (*Message, error) {
	url := url.URL{
		Scheme: "http",
		Host:   h.Addr,
		Path:   fmt.Sprintf("/message/%d", idx),
	}

	req, _ := http.NewRequest(http.MethodGet, url.String(), nil)
	resp, err := h.Client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	msg := &Message{}
	err = protojson.Unmarshal(data, msg)
	if err != nil {
		return nil, err
	}

	return msg, nil
}

func (h *HTTPBlackboard) GetMessages(start_idx int, filter_pings bool) ([]*Message, error) {

	url := url.URL{
		Scheme: "http",
		Host:   h.Addr,
		Path:   fmt.Sprintf("/blackboard/%d", start_idx),
	}

	req, _ := http.NewRequest(http.MethodGet, url.String(), nil)
	resp, err := h.Client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	msg_stack := &MessageStack{}
	err = protojson.Unmarshal(data, msg_stack)
	if err != nil {
		return nil, err
	}

	return msg_stack.GetMessages(), nil
}

func (h *HTTPBlackboard) GetResults(int) ([]*Message, error) {
	return nil, nil
}

func (h *HTTPBlackboard) ResultSizeLimit() int {
	return 0
}

func (h *HTTPBlackboard) Len() int {
	return 0
}

func (h *HTTPBlackboard) Shutdown() {}
