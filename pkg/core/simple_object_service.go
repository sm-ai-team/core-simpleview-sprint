package core

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/hoffman-lab/core-objectstore/pkg/obj"
	"gitlab.com/hoffman-lab/core/pkg/config"
)

type SimpleObjectService struct {
	*obj.ObjectStoreServer
}

// Return a ready, but not started object store service
func NewSimpleObjectService() *SimpleObjectService {
	return &SimpleObjectService{
		ObjectStoreServer: obj.NewServer(obj.NewMemoryStore()),
	}
}

// Return a running object store service
func StartSimpleObjectService() (*SimpleObjectService, error) {

	log.Info("Starting object store on ", config.Globals.ObjectStoreAddress)
	sos := NewSimpleObjectService()

	go func() {
		log.Fatal(sos.Start(config.Globals.ObjectStoreAddress))
	}()

	return sos, nil
}

// Fulfill the required `core` interface of "put data, get url back"
func (sos *SimpleObjectService) Put(data []byte) (string, error) {

	id, err := sos.ObjectStore.Put(data)
	if err != nil {
		return "", err
	}

	return sos.ObjectStoreServer.GetURL(id), nil
}
