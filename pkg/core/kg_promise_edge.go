// Wrapper to ensure that we fulfill the gonum interfaces.
package core

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/simple"
)

var _ graph.Edge = &PromiseEdge{}

// Promise edges are directed
type PromiseEdge struct {
	*Promise
	simple.WeightedEdge
}

func NewPromiseEdge(we simple.WeightedEdge, promise *Promise) *PromiseEdge {
	return &PromiseEdge{
		Promise:      promise,
		WeightedEdge: we,
	}
}

func (pe *PromiseEdge) From() graph.Node {
	return pe.WeightedEdge.From()
}

func (pe *PromiseEdge) To() graph.Node {
	return pe.WeightedEdge.To()
}

func (pe *PromiseEdge) Attributes() []encoding.Attribute {
	return []encoding.Attribute{
		{Key: "label", Value: pe.String()},
		{Key: "fontsize", Value: "8pt"},
		//{Key: "fontcolor", Value: "#AAAAAA"},
		{Key: "fontcolor", Value: "#BBBBBB"},
		{Key: "fontname", Value: "Helvetica"},
		{Key: "labelloc", Value: "r"},
	}
}

func (pe *PromiseEdge) String() string {
	return pe.Name + "\n" + pe.Type
}

// From the docs:
// When a reversal is valid an edge of the same type as
// the receiver with nodes of the receiver swapped should
// be returned, otherwise the receiver should be returned
// unaltered.
//
// Reversed edges don't make much sense in a directed graph...
func (pe *PromiseEdge) ReversedEdge() graph.Edge {
	return pe
}
