package core

import (
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"
)

// AgentSpec is a structure defined by our proto file.  Because of
// this, we don't get full, unbridled control of how certain
// properties (namely Attributes) are handled.  You may seef some
// serialization or structure management that's not 100% clear why
// it's needed, however it more than likely relates to this fact.

// Verifies that the provided agent name complies with RFC1035
func ValidateAgentName(name string) (bool, error) {
	expr := regexp.MustCompile(`^[a-z]([-a-z0-9]*[a-z0-9])?$`)
	matched := expr.MatchString(name)
	if !matched {
		return false, fmt.Errorf("agent type names must comply with RFC1035")
	}

	return true, nil
}

func DefaultAgentSpec(name string) *AgentSpec {
	return &AgentSpec{
		Name:               name,
		PollIntervalMs:     500,
		PingIntervalMs:     1000,
		AllowedPingRetries: 3,
		AllowedPollRetries: 3,
		Attributes:         nil,
		//Attributes:         make(map[string]interface{}),
	}
}

// SetDefaults overrides unset spec values with the defaults defined
// in DefaultAgentSpec()
func (spec *AgentSpec) SetDefaults(name string) {
	def := DefaultAgentSpec(name)

	override_occurred := false

	spec.Name = name

	if spec.PollIntervalMs == 0 {
		spec.PollIntervalMs = def.PollIntervalMs
		override_occurred = true
	}

	if spec.PingIntervalMs == 0 {
		spec.PingIntervalMs = def.PingIntervalMs
		override_occurred = true
	}

	if spec.AllowedPollRetries == 0 {
		spec.AllowedPollRetries = def.AllowedPollRetries
		override_occurred = true
	}

	if spec.AllowedPingRetries == 0 {
		spec.AllowedPingRetries = def.AllowedPingRetries
		override_occurred = true
	}

	if spec.Attributes == nil {
		spec.Attributes = def.Attributes
		override_occurred = true
	}

	if override_occurred {
		log.Warn("A zero/nil value was overridden for agent ", spec.Name)
	}
}

// I decided to implement GetPromises into AgentSpec -JG
//func (sp *AgentSpec) GetPromises() (*PromiseSet, error) {
//
//	attrs_json, err := json.Marshal(sp.Attributes)
//	if err != nil {
//		return nil, err
//	}
//
//	attributes := make(map[string]interface{})
//	err = json.Unmarshal(attrs_json, &attributes)
//	if err != nil {
//		return nil, err
//	}
//
//	promises := make(map[string]*Promise)
//
//	for attr_name, attr_val := range attributes {
//
//		val_string, ok := attr_val.(string)
//		if !ok {
//			continue
//		}
//
//		promise, err := ParsePromise(val_string)
//		if err != nil {
//			// log.Error(err)
//			continue
//		}
//
//		promises[attr_name] = promise
//	}
//
//	promise_set := &PromiseSet{
//		promises: promises,
//	}
//
//	return promise_set, nil
//}
