package agents

import (
	"fmt"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/hoffman-lab/core/pkg/config"
	. "gitlab.com/hoffman-lab/core/pkg/core"
	"google.golang.org/protobuf/encoding/protojson"
)

type LocalRunner struct {
	*Runner
}

// Getting closer, but this still is more boilerplate than I would like
func NewLocalRunner(bb Blackboard, spec *AgentSpec) error {
	r, err := StartRunner(bb, spec)
	if err != nil {
		return err
	}

	l := &LocalRunner{
		Runner: r,
	}
	l.RunAgent = l.RunLocalAgent

	return nil
}

func (r *LocalRunner) RunLocalAgent(spec *AgentSpec) error {

	//data, err := json.Marshal(spec)

	data, err := protojson.Marshal(spec)
	if err != nil {
		return err
	}

	bb_addr := config.Globals.BlackboardHTTPAddress
	cmd_split := strings.Split(spec.Entrypoint, " ")

	json_spec := string(data)

	cmd_split = append(cmd_split, "--blackboard", bb_addr, "--spec", json_spec)

	r.Log(Log_Info, "dispatching agent with command: "+strings.Join(cmd_split, " "))

	cmd := exec.Command(cmd_split[0], cmd_split[1:]...)
	data, err = cmd.CombinedOutput()
	if err != nil {
		r.Log(Log_Error, fmt.Sprintf("agent %s exited with error: %s", spec.Name, err))
		log.Info(err, string(data))
	}
	return nil
}
