package agents

import (
	"os"
	"testing"

	"github.com/matryer/is"
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

func TestAgentConfig(t *testing.T) {

	is := is.New(t)

	test_file := "agent-config-test.yaml"

	defer func() { os.Remove(test_file) }()

	a := &AgentsConfig{
		Controllers: map[string]*AgentSpec{
			"naive": &AgentSpec{
				Name:       "naive",
				Entrypoint: "StartNaiveController",
				Runner:     "go",
			},
		},

		Agents: map[string]*AgentSpec{
			"sample": &AgentSpec{
				Name:       "sample",
				Entrypoint: "python sample_agent.py",
				Runner:     "local",
			},
		},

		Runners: map[string]*AgentSpec{
			"go":    &AgentSpec{Name: "go-runner"},
			"local": &AgentSpec{Name: "local-runner"},
		},
	}

	err := a.Save(test_file)
	is.NoErr(err)

	err = a.Load(test_file)
	is.NoErr(err) // Can load from a test file

}
