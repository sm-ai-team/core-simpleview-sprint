package agents

import (
	"time"

	. "gitlab.com/hoffman-lab/core/pkg/core"
)

// Ping agent definition
type PingAgent struct {
	*GoAgent
}

func StartPingAgent(bb Blackboard, spec *AgentSpec) (Agent, error) {
	p := &PingAgent{
		GoAgent: NewGoAgent(bb, spec),
	}

	return p, nil
}

func (p *PingAgent) Run() error {

	// Handle dwell attribute
	//dwell, err := p.Attributes.Value("dwell")
	//if err != nil {
	//	p.PostStatusUpdate(StatusUpdate_Error, "pingagent requires 'dwell' attribute")
	//	return fmt.Errorf("pingagent requires 'dwell' attribute")
	//}

	time.Sleep(time.Duration(p.Attributes.MustGetInt("dwell")) * time.Second)

	// Post result
	r := &Promise{
		Name: "SpecialPing",
		Type: "Result",
	}

	return p.PostResult(Result_Result, r)
}

// Pong agent definition
type PongAgent struct {
	*GoAgent
}

func StartPongAgent(bb Blackboard, spec *AgentSpec) (Agent, error) {
	// Intantiate the agent
	p := &PongAgent{
		GoAgent: NewGoAgent(bb, spec),
	}

	return p, nil
}

func (p *PongAgent) Run() error {
	// Await promises
	_, err := p.Attributes.AwaitValue("ping")
	if err != nil {
		return err
	}

	// Post result
	r := &Promise{
		Name: "SpecialPong",
		Type: "Result",
	}

	return p.PostResult(Result_Result, r)
}
