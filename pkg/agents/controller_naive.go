// NaiveController starts all agents then shuts down
//
// This is intended to represent a "simplest possible" controller
// example.  If additional functionality is desired, please see
// DefaultController (TODO) or develop your own using ours as
// blueprints!
//
// This shows that controllers do not need to be long lived.
// Controller does not necessarily have to manage the blackboard
//
// Controller does not have to shutdown blackboard

package agents

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type NaiveController struct {
	*GoAgent
}

// Constructor (used to manage the agent lifecyle)
func StartNaiveController(bb Blackboard, spec *AgentSpec) (Agent, error) {

	//fmt.Println(spec)

	c := &NaiveController{
		GoAgent: NewGoAgent(bb, spec),
	}

	return c, nil
}

// Where the fun stuff happens
func (n *NaiveController) Run() error {

	// Wait for an agent bundle to appear on the blackboard
	raw_bundle, err := n.Attributes.AwaitValue("agents")
	if err != nil {
		return fmt.Errorf("failed to resolve agent bundle: %s", err)
	}

	// Agent bundles should always be passed around as json encoded map[string]*AgentSpec.
	var agent_bundle map[string]*AgentSpec

	fmt.Println(string(raw_bundle.([]byte)))

	err = json.Unmarshal(raw_bundle.([]byte), &agent_bundle)
	if err != nil {
		log.Fatal(err)
		return fmt.Errorf("failed to unmarshal agent bundle: %s", err)
	}

	// Start agents all together (naively!)
	for _, spec := range agent_bundle {
		err := n.PostCreateAgent(spec)
		if err != nil {
			return fmt.Errorf("failed to post create agent message: %s", err)
		}
	}

	return nil
}
