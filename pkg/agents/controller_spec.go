package agents

import (
	"errors"
	"fmt"
	"os"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core"
	"gopkg.in/yaml.v3"
)

// ControllerSpec shall use *default* serialization naming schemes
type ControllerSpec struct {
	// TODO: User specification of input data (empty allowed, but still must be specified)
	InputData map[string]interface{} `json:"input_data"`
	// List of agents required for current run (empty NOT allowed)
	Agents map[string]*AgentSpec `json:"agent_specs"`
	// List of runners that agents will run on (empty NOT allowed)
	Runners map[string]*AgentSpec `json:"runner_specs"`
	// TODO: Automatic parameter tuning information (empty allowed)
	ParameterTuning map[string]interface{} `json:"apt"`
}

func NewControllerSpec() *ControllerSpec {
	return &ControllerSpec{
		InputData:       make(map[string]interface{}),
		Agents:          make(map[string]*AgentSpec),
		Runners:         make(map[string]*AgentSpec),
		ParameterTuning: make(map[string]interface{}),
	}
}

func (c *ControllerSpec) Save(filepath string) error {
	f, err := os.Create(filepath)
	if err != nil {
		return err
	}

	err = yaml.NewEncoder(f).Encode(c)
	if err != nil {
		return err
	}

	return nil
}

func (c *ControllerSpec) Load(filepath string) error {

	f, err := os.Open(filepath)
	if err != nil {
		return err
	}

	// Decode the bulk of the config
	// NOTE: this throws a type error because of agent attributes, but they
	// are handled below.
	err = yaml.NewDecoder(f).Decode(c)
	if err != nil {
		return err
	}

	return nil
}

// John is going to fix the Promise structure because it only allows one data type of same
// thing result type thing to be added to promises at a time in attributes.
func (c *ControllerSpec) ResolveGraph() ([]map[string]*AgentSpec, map[string][]string, error) {

	dependency_map := make(map[string]mapset.Set[string])
	no_edges := mapset.NewSet[string]()
	output_sort := make([]map[string]*AgentSpec, 0, len(c.Agents))

	for agent_name, agent_spec := range c.Agents {
		// We are skipping over the web interface agent if it exists
		if agent_name == "web_agent" {
			output_sort = append(output_sort, map[string]*AgentSpec{agent_name: agent_spec})
			continue
		}

		dependency_map[agent_name] = mapset.NewSet[string]()

		//promises, err := agent_spec.GetPromises()
		//if err != nil {
		//	return nil, nil, err
		//}

		tmp_attrs := NewAttributes(agent_spec.Attributes)

		promises := tmp_attrs.GetPromises()

		for _, p := range promises {

			if _, ok := c.Agents[p.Source]; !ok {
				return nil, nil, fmt.Errorf("promise source %s not found in any AgentSpec", p.Source)
			}

			dependency_map[agent_name].Add(p.Source)
		}

		if dependency_map[agent_name].Cardinality() == 0 {
			no_edges.Add(agent_name)
		}
	}

	output_dependency := ConvertMapSetToSlice(dependency_map)
	for no_edges.Cardinality() > 0 {

		spec_bunch := make(map[string]*AgentSpec)

		for _, front_agent := range no_edges.ToSlice() {

			spec_bunch[front_agent] = c.Agents[front_agent]
			delete(dependency_map, front_agent)

			for agent_name := range dependency_map {
				dependency_map[agent_name].Remove(front_agent)
				if dependency_map[agent_name].Cardinality() == 0 && !no_edges.Contains(agent_name) {
					no_edges.Add(agent_name)
				}
			}
			no_edges.Remove(front_agent)

		}

		output_sort = append(output_sort, spec_bunch)

	}

	if len(dependency_map) != 0 {
		return nil, nil, errors.New("circular dependency detected between Agent promises")
	}

	return output_sort, output_dependency, nil
}

func ConvertMapSetToSlice(input map[string]mapset.Set[string]) map[string][]string {
	converted_map := make(map[string][]string)

	for name, deps := range input {
		converted_map[name] = deps.ToSlice()
	}

	return converted_map
}

func RunnerExists(runner_name string) bool {
	return true
}

// Validate runs checks to ensure that a controller spec can properly execute.
// Returns nil if spec is valid and returns an error if not.
//
// TODO: Additional validation to add
// - No agent/runner name collisions
func (c ControllerSpec) Validate() error {

	err_msg := ""

	is_valid := true
	is_error := false
	is_warning := false

	// Check 0: Set any unset defaults
	for name := range c.Agents {
		c.Agents[name].SetDefaults(name)
	}

	// CHECK 1: Verify at least one agent specified
	if len(c.Agents) < 1 {
		is_valid = false
		is_error = true
		err_msg += "Error: Must declare at least one agent\n"
	}

	// CHECK 2: Verify at least one runner specified
	if len(c.Runners) < 1 {
		is_valid = false
		is_error = true
		err_msg += "Error: Must declare at least one runner\n"
	}

	// CHECK 3: Verify runner assignments
	agent_table := make(map[string]int)

	for _, spec := range c.Agents {
		_, exists := c.Runners[spec.Runner]
		if !exists {
			is_valid = false
			is_error = true
			err_msg += fmt.Sprintf("Error: runner \"%s\" for agent \"%s\" does not exist\n", spec.Runner, spec.Name)
		}

	}

	// Check 4: Verify types
	//for _, spec := range c.Agents {
	//	switch spec.Type {
	//	case GoAgent, ShellAgent, DockerAgent:
	//	default:
	//		is_valid = false
	//		is_error = true
	//		err_msg += fmt.Sprintf("Error: type \"%s\" for agent \"%s\" is invalid\n", spec.Type, spec.Name)
	//	}
	//}

	// Iterate over all runners and tally agents
	//for runner_name, runner := range c.Runners {
	//	for _, agent_name := range runner.Agents {
	//		_, exists := agent_table[agent_name]
	//		if !exists {
	//			is_warning = true
	//			err_msg += fmt.Sprintf("Warning: Runner \"%s\" references non-existent agent \"%s\"\n", runner_name, agent_name)
	//			continue
	//		}
	//		agent_table[agent_name]++
	//	}
	//}

	for agent_name, runner_count := range agent_table {
		if runner_count != 1 {
			is_valid = false
			is_error = true
			err_msg += fmt.Sprintf("Error: Agent \"%s\" assigned to incorrect number of runners (%d != 1)\n", agent_name, runner_count)
		}
	}

	// Verify that

	// TODO CHECK 4: Parse and verify all detected promises resolve

	// Finally, return
	if is_error {
		log.Error(err_msg)
	} else if is_warning {
		log.Warn(err_msg)
	}

	if !is_valid {
		return errors.New("invalid configuration file")
	}

	return nil
}

func (c ControllerSpec) String() string {
	s := &strings.Builder{}
	yaml.NewEncoder(s).Encode(c)
	return s.String()
}

// Parse runners translates the config file runner specifications into
// concrete runners for final deployment.
//func (c *ControllerSpec) ParseRunners() (map[string]Runner, error) {
//
//	final_runners := make(map[string]Runner)
//
//	for name, runner := range c.Runners {
//
//		var r Runner
//		curr_runner := runner.(map[string]interface{})
//
//		switch curr_runner["type"] {
//		case RunnerTypeLocal:
//			r = NewLocalRunner(name, curr_runner)
//		case RunnerTypeCondor:
//		case RunnerTypeKubernetes:
//		default:
//			log.Fatalf("Failed to parse runner: %v", runner)
//		}
//
//		final_runners[name] = r
//
//	}
//
//	return final_runners, nil
//}
//
//func (c *ControllerSpec) ParseAgents() (map[string]*Agent, error) {
//	return c.Agents, nil
//}
//
//// Validation must ensure that:
//// - At least 1 agent is specified
//// - At least 1 runner is defined
//// - Each agent is assigned to a runner
//// - All promises are valid (i.e. providing agent must exist)
//// This function signature is ugly.
//func (c *ControllerSpec) Validate() (is_valid bool, promise_table []*Promise, agents map[string]*Agent, runners map[string]Runner, err error) {
//
//	agents, err = c.ParseAgents()
//	if err != nil {
//		return false, nil, nil, nil, err
//	}
//
//	runners, err = c.ParseRunners()
//	if err != nil {
//		return false, nil, nil, nil, err
//	}
//
//	// Easy checks
//	if len(agents) < 1 {
//		return false, nil, nil, nil, errors.New("No agents declared in config. Must declare at least one agent.")
//	}
//
//	if len(runners) < 1 {
//		return false, nil, nil, nil, errors.New("No runners declared in config. Must declare at least one runner.")
//	}
//
//	// Verify each agent assigned to exactly one runner
//	checklist := make(map[string]int)
//
//	for k, _ := range agents {
//		checklist[k] = 0
//	}
//
//	for _, v := range runners {
//		runner_agents := v.AssignedAgents()
//
//		for _, a := range runner_agents {
//			checklist[a]++
//		}
//	}
//
//	is_valid = true
//	for agent, count := range checklist {
//		if count != 1 {
//			log.Errorf("agent %s assigned to incorrect # of runners (%d != 1)\n", agent, count)
//			is_valid = false
//		}
//	}
//
//	// Find all promises and check that they resolve to valid sources
//	// (This code could probably be cleaner)
//	promise_table = make([]*Promise, 0)
//	for _, v := range agents {
//
//		for _, att := range v.Attributes {
//			switch att.(type) {
//			case string:
//				promise, err := Parse(att.(string))
//				if err != nil {
//					log.Error(err)
//					is_valid = false
//				} else {
//
//					success, _ := promise.Resolve(agents)
//					if !success {
//						log.Errorf("Promise did not resolve: %v", promise)
//						is_valid = false
//						continue
//					}
//
//					promise_table = append(promise_table, promise)
//				}
//			default:
//				continue
//			}
//		}
//	}
//
//	// TODO: Run dependency checker (postpone this temporarily)
//	//       Will eventually integrate Josh's knowledge graph parsing
//
//	return is_valid, promise_table, agents, runners, nil
//}
//
//func contains(set []string, val string) bool {
//	for _, v := range set {
//		if val == v {
//			return true
//		}
//	}
//	return false
//}
//
//func (c *ControllerSpec) String() string {
//
//	data, err := yaml.Marshal(c)
//	if err != nil {
//		log.Fatal("Serialize to yaml failed: ", err)
//	}
//
//	return string(data)
//}
