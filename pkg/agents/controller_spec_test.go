package agents

import (
	"os"
	"testing"

	"github.com/matryer/is"
)

func TestControllerConfigMinimal(t *testing.T) {
	is := is.New(t)

	out_filepath := "test.yaml"

	c := NewControllerSpec()
	t.Logf("New config: %+v", c)

	err := c.Save(out_filepath)
	is.NoErr(err)

	err = c.Load(out_filepath)
	t.Logf("Loaded config: %+v", c)
	is.NoErr(err)

	os.Remove(out_filepath)
}

func TestControllerConfigResolve(t *testing.T) {
	is := is.New(t)

	test_filepath := "../../test-files/ping-pong-asterisk.yaml"

	c := NewControllerSpec()
	// t.Logf("New config: %+v", c)

	err := c.Load(test_filepath)
	// t.Logf("Loaded config: %+v", c)
	is.NoErr(err)

	s, _, err := c.ResolveGraph()
	t.Log("Resolved Agent dependencies: ")
	for i, disp_grp := range s {
		t.Logf("Bunch %d: %v", i, disp_grp)
	}
	is.NoErr(err)
}

func TestPingPongControllerConfig(t *testing.T) {

	is := is.New(t)

	input_path := "../../test-files/ping-pong.yaml"

	c := NewControllerSpec()
	err := c.Load(input_path)
	is.NoErr(err)

	t.Logf("Loaded ping-pong config:\n%v", c)

	err = c.Validate()
	is.NoErr(err)
}
