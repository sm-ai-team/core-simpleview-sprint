// This version of web interface purely monitors and visualizes/reports
// the contents of the blackboard.  It has no promised data and lives forever.

package agents

import (
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core"
	"google.golang.org/protobuf/encoding/protojson"
)

//var WebFS embed.FS

type WebInterfaceLive struct {
	bb        Blackboard
	addr      string
	AgentType string
	*GoAgent
}

var tpl *template.Template

func StartWebInterfaceLive(bb Blackboard, spec *AgentSpec) (Agent, error) {

	ga := NewGoAgent(bb, spec)

	addr, err := ga.Attributes.Value("addr")
	if err != nil {
		addr = "localhost:9099"
	}

	a := &WebInterfaceLive{
		bb:        bb,
		addr:      addr.(string),
		AgentType: "web-interface-live",
		GoAgent:   ga,
	}

	a.GoAgent.OnNewMessage = a.OnNewMessage

	tpl, err = template.ParseFS(WebFS, "web/template/index_live.html")
	if err != nil {
		log.Fatal("could not access required template: ", err)
	}

	return a, nil
}

func (a *WebInterfaceLive) Run() error {
	// Launch SimpleMind web interface server
	r := mux.NewRouter()
	r.HandleFunc("/", a.IndexHandler)
	r.HandleFunc("/blackboard/{start_idx:[0-9]+}", a.BlackboardHandler) // /blackboard?start=<idx>&stop=<idx,optional>
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./web/static"))))

	a.PostStatusUpdate(StatusUpdate_OK, "web interface is online!")

	return http.ListenAndServe(a.addr, r)
}

func (a *WebInterfaceLive) IndexHandler(w http.ResponseWriter, r *http.Request) {
	tpl.Execute(w, nil)
}

// Proxies a request through to the original blackboard.
// At the time of writing, this is the exact same handler as the
// BlackboardHandler in BlackboardHTTPService.
func (a *WebInterfaceLive) BlackboardHandler(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)

	start_idx, ok := params["start_idx"]
	if !ok {
		start_idx = "0"
	}

	//a.Log(Log_Info, "blackboard request starting at: "+start_idx)

	start, err := strconv.Atoi(start_idx)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	msgs, err := a.bb.GetMessages(start, true)
	if err != nil {
		http.Error(w, "Blackboard error: "+err.Error(), http.StatusInternalServerError)
	}

	tmp := &MessageStack{
		Messages: msgs,
	}

	data, err := protojson.Marshal(tmp)
	if err != nil {
		log.Error("Failed to encode to json: ", err)
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(data)
	if err != nil {
		log.Error("Failed to transmit json")
	}

}
