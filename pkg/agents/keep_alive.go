package agents

import (
	"time"

	. "gitlab.com/hoffman-lab/core/pkg/core"
)

// KeepAlive agent will prevent the blackboard from shutting down after
// other agents complete.  It was developed for testing the python client.
type KeepAliveAgent struct {
	*GoAgent
}

func StartKeepAliveAgent(bb Blackboard, spec *AgentSpec) (Agent, error) {
	ka := &KeepAliveAgent{
		GoAgent: NewGoAgent(bb, spec),
	}
	return ka, nil
}

func (ka *KeepAliveAgent) Run() error {
	for {
		select {
		case <-time.After(10 * time.Second):
		case <-ka.Done():
			return nil
		}
	}
}
