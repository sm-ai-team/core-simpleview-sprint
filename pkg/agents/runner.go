package agents

import (
	"fmt"

	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type Runner struct {
	*GoAgent
	RunAgent func(a *AgentSpec) error
	stop     chan struct{}
}

func StartRunner(bb Blackboard, spec *AgentSpec) (*Runner, error) {
	r := &Runner{
		GoAgent: NewGoAgent(bb, spec),
		stop:    make(chan struct{}, 1),
	}

	r.Start()

	r.RunAgent = func(a *AgentSpec) error {
		r.Log(Log_Info, fmt.Sprintf("%s runner got job, but runner is not yet implemented", r.GoAgent.AgentSpec.Name))
		return nil
	}

	r.GoAgent.OnNewMessage = r.OnNewMessage

	return r, nil
}

func (r *Runner) OnNewMessage(m *Message) {

	switch m.Contents.(type) {
	case *Message_CreateAgent:
		spec := m.GetCreateAgent()

		if spec.Runner != r.Name {
			return
		}

		// Call the RunJob function
		go func() {
			err := r.RunAgent(spec)
			if err != nil {
				r.Log(Log_Error, fmt.Sprintf("agent %s exited with error: %s", spec.Name, err))
			}
		}()
	case *Message_Halt:
		r.CancelFunc()
	case *Message_HaltAndCatchFire:
	}

}

// Run function for the runner agent
func (r *Runner) Run() error {
	r.PostStatusUpdate(StatusUpdate_Working, fmt.Sprintf("Runner %s listening for jobs", r.Name))
	<-r.stop
	return nil
}
