// AgentConfig is primary "user" interface for core.
//
// AgentConfig is simple a grouped list of agent specs used by the
// `core` executable to dispatch agents and runners.
//
// The key blocks are the following:
//
// controllers (optional) - controllers manage other agents via
// CreateAgent messages.  They can optionally monitor for agent
// completion (Hello/Goodbye messages), status updates, promise
// resolution, etc. however this is not a requirement.  Please see
// pkg/agents/controller_naive.go and pkg/agents/controller_basic.go
// for examples of how controllers can manage agent behavior
// differently.
//
// agents (required) - these are your processing agents.  these are
// what do the work.  These are the same as in ControllerConfig.
//
// runners (required) - these are the agents that know how to map
// your agents into the runtime environment.
//
// AgentConfig replaces the previously used "ControllerConfig".
//
// Please see the following for up-to-date examples of AgentConfigs
//   - test-files/pyng-pong.yaml
//   - test-files/ping-pong.yaml

package agents

import (
	"fmt"
	"os"

	. "gitlab.com/hoffman-lab/core/pkg/core"
	"gopkg.in/yaml.v3"
)

// This is an abstraction for serializing agent configuration files/documents
type AgentsConfig struct {
	Controllers map[string]*AgentSpec // Started by exe via a create agent request (i.e. uses the bb interface)
	Agents      map[string]*AgentSpec // Started via create agent requests from the controller
	Runners     map[string]*AgentSpec // Started manually by exe
}

func (a *AgentsConfig) Load(filepath string) error {
	f, err := os.Open(filepath)
	if err != nil {
		return err
	}

	err = yaml.NewDecoder(f).Decode(a)
	if err != nil {
		return err
	}

	// Make sure we're injecting the correct name into the agent (the
	// heading that they're specified under in the yaml
	//
	// TODO: Use reflection here to add the ability for new "headings"
	// to specify agents under in the future.

	for k := range a.Controllers {
		a.Controllers[k].Name = k
	}

	for k := range a.Agents {
		a.Agents[k].Name = k
	}

	for k := range a.Runners {
		a.Runners[k].Name = k
	}

	return nil
}

func (a *AgentsConfig) Save(filepath string) error {
	fmt.Println(a)
	f, err := os.Create(filepath)
	if err != nil {
		return err
	}

	return yaml.NewEncoder(f).Encode(*a)
}
