// This function should always return a working, minimal spec that
// satisfies most use cases for most users.

package agents

import (
	"gitlab.com/hoffman-lab/core/pkg/core"
)

func GetDefaultControllerSpec() *core.AgentSpec {

	//return &core.AgentSpec{
	//	Name:       "naive-controller",
	//	Runner:     "go",
	//	Entrypoint: "StartNaiveController",
	//	Attributes: map[string]string{
	//		"agents": "Promise( * as agent-bundle from * )", // this is a little awkward.  it will be the most flexible solution going forward, but still might be hard for new users to understand.
	//	},
	//}

	return &core.AgentSpec{
		Name:       "basic-controller",
		Runner:     "go",
		Entrypoint: "StartBasicController",
		Attributes: map[string]string{
			"agents": "Promise( * as agent-bundle from * )", // this is a little awkward.  it will be the most flexible solution going forward, but still might be hard for new users to understand.
		},
	}

}
