// TODO: At some point, this entire file should be generated based on the agents
// found in the "agents" package of core

// There's probably a cleaner way to do all of this, but interfaces
// don't quite solve it.  (it's actually a case where true inheritability
// is the *most* suitable, but we don't have that available to us in go)

package agents

import (
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type AgentEntrypoint func(bb Blackboard, spec *AgentSpec) (Agent, error)

type EntrypointTable map[string]AgentEntrypoint

// this table should be auto-generated
var AgentEntrypoints EntrypointTable = map[string]AgentEntrypoint{
	"StartPingAgent":        StartPingAgent,
	"StartPongAgent":        StartPongAgent,
	"StartKeepAliveAgent":   StartKeepAliveAgent,
	"StartWebInterface":     StartWebInterface,
	"StartWebInterfaceLive": StartWebInterfaceLive,
	"StartNaiveController":  StartNaiveController,
	"StartBasicController":  StartBasicController,
}
