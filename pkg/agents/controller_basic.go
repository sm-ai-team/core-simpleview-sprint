// The basic controller is the controller we expect most people to
// utilize for most applications.
//
// The basic controller manages agents via a knowledge graph,
// which is updated and checked for new dispatch opportunities
// whenever a new result is posted to the blackboard.
//
// Agents will only be launched (i.e. controller will write a
// CreateAgent message to the BB) when all of their declared promises
// have been fulfilled.  This introduces latency while the agents run
// through their startup processes (CreateAgent -> runner -> agent
// startup) so it may not be suitable for all use cases, but should be
// a reliable default choice.

package agents

import (
	"encoding/json"
	"fmt"

	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type BasicController struct {
	*GoAgent
	*KnowledgeGraph
	completed chan struct{}
}

func StartBasicController(bb Blackboard, spec *AgentSpec) (Agent, error) {

	c := &BasicController{
		GoAgent:        NewGoAgent(bb, spec),
		KnowledgeGraph: NewKnowledgeGraph(),
		completed:      make(chan struct{}),
	}

	c.GoAgent.OnNewMessage = c.OnNewMessage

	return c, nil
}

func (c *BasicController) DispatchAgents(specs []*AgentSpec) error {
	for idx := range specs {
		err := c.PostCreateAgent(specs[idx])
		if err != nil {
			return err
		}
		c.MarkStarted(specs[idx].Name)
	}

	return nil
}

func (c *BasicController) Completed() chan struct{} {
	return c.completed
}

// The controller's OnNewMessage tracks graph state and checks for
// certain conditions.  Controller will shutdown, and trigger
// a blackboard shutdown when
//
// If users are implementing their own message handler, it's annoying
// that they would have to manually check for promise fulfillment
// in order for value and await_value to work normally.  We should fix
// that.
func (c *BasicController) OnNewMessage(m *Message) {

	switch m.Contents.(type) {
	case *Message_Ping:
		c.UpdatePing(m.Source, m.TimeSent.AsTime())
	case *Message_Hello:
		c.UpdateState(m.Source, &StatusUpdate{State: StatusUpdate_OK})
	case *Message_Goodbye:
		c.UpdateState(m.Source, &StatusUpdate{State: StatusUpdate_Completed})

		// Check if all agents have finished
		if c.AllCompleted() {
			c.Log(Log_Info, "all agents have completed. basic controller shutting down.")
			c.Log(Log_Info, "DEBUG: automatic shutdown currently not working.")
			c.Log(Log_Info, "       Ctrl-C ONCE to shutdown and save the blackboard.")
			close(c.completed)
		}

	case *Message_StatusUpdate:
		c.UpdateState(m.Source, m.GetStatusUpdate())
	case *Message_Result:

		// Update the knowledge graph
		promise := m.GetResult().GetPromise()
		c.KnowledgeGraph.UpdateEdges(promise)

		// Dispatch any newly-ready agents
		specs := c.GetStartableSpecs()
		err := c.DispatchAgents(specs)
		if err != nil {
			err_string := "dispatch failed: " + err.Error()
			c.Log(Log_Error, err_string)
			c.PostStatusUpdate(StatusUpdate_Error, err_string)
		}

		// Update any attributes as we would normally
		r := m.GetResult()
		c.Attributes.ProcessResult(r)
	}

}

func (c *BasicController) Run() error {

	// Wait for an agent bundle to appear on the blackboard
	raw_bundle, err := c.Attributes.AwaitValue("agents")
	if err != nil {
		return fmt.Errorf("failed to resolve agent bundle: %s", err)
	}

	// Agent bundles should always be passed around as json encoded map[string]*AgentSpec.
	var agent_bundle map[string]*AgentSpec

	err = json.Unmarshal(raw_bundle.([]byte), &agent_bundle)
	if err != nil {
		return fmt.Errorf("failed to unmarshal agent bundle: %s", err)
	}

	// Load the knowledge graph with with agent bundle
	c.KnowledgeGraph.FromAgentBundle(agent_bundle)

	// Check if we're supposed to save visual graph
	save, err := c.Attributes.Value("save-to-svg")
	if err == nil {
		c.KnowledgeGraph.ToSVG(save.(string))
	}

	// Get the initial list of startable agents:
	specs := c.GetStartableSpecs()
	err = c.DispatchAgents(specs)
	if err != nil {
		return err
	}

	// Wait for the channel to close (triggered in the OnNewMessage
	// callback)
	<-c.completed
	return nil
}
