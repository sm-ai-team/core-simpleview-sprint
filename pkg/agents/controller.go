package agents

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/hoffman-lab/core/pkg/config"
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

// The controller is responsible for
// 1. Parsing of the knowledge base into agents
// 2. Monitor the the blackboard for new agents to dispatch (based on location in knowledge graph)
// 3. Pass agents to job distributor to dispatch
type Controller struct {
	*GoAgent                        // Controller *is* a type of agent, therefore, we embed Agent type to get base agent functionality
	Spec            *ControllerSpec `json:"specs"`
	AgentMonitor    *AgentStatusSet
	RunnerMonitor   *AgentStatusSet
	DispatchMonitor *DispatchMonitor
	completed       chan struct{}
}

func (c *Controller) OnNewMessage(m *Message) {
	switch m.Contents.(type) {
	case *Message_Ping:
		c.AgentMonitor.UpdatePing(m.Source, m.TimeSent.AsTime())
	case *Message_Hello:
		c.AgentMonitor.UpdateState(m.Source, StatusUpdate_OK)
	case *Message_Goodbye:
		c.AgentMonitor.UpdateState(m.Source, StatusUpdate_Completed)
	case *Message_StatusUpdate:
		s := m.GetStatusUpdate()
		c.AgentMonitor.UpdateStatus(m.Source, s)
	}
}

// Starts the SimpleMind controller agent.  This is the primary
// operating agent responsible for parsing the configuration/knowledge
// and running a SimpleMind job.  "spec" must be a *validated*
// ControllerSpec (c_spec.Validate()); behavior on a un-validated spec is
// not defined or designed for.
func StartController(bb Blackboard, spec *ControllerSpec) (*Controller, error) {

	tmp_spec := DefaultAgentSpec("controller")

	c := &Controller{
		GoAgent:       NewGoAgent(bb, tmp_spec),
		Spec:          spec,
		AgentMonitor:  NewAgentStatusSet(),
		RunnerMonitor: NewAgentStatusSet(),
		completed:     make(chan struct{}),
	}

	// Handle knowledge graph messaging
	agent_groups, agent_deps, err := c.Spec.ResolveGraph()
	if err != nil {
		log.Fatal("could not resolve dependencies in spec: ", err)
	}

	c.DispatchMonitor = NewDispatchMonitor(agent_groups)

	// Initialize agent monitor and start controller
	c.InitializeAgentMonitor()

	c.Start()

	c.SendKnowledgeGraph(agent_groups, agent_deps)

	//func (c *Controller) OnNewMessage(m *Message)
	c.GoAgent.OnNewMessage = c.OnNewMessage

	// Monitor for dispatch group and agent completion
	go func() {
		for {
			time.Sleep(1 * time.Second)
			if c.AgentMonitor.DispatchGroupCompleted(c.DispatchMonitor) {
				// fire the dispatch signal (should only be read by main.go using <-controller.DispatchMonitor.Done())
				c.DispatchMonitor.SignalCompleted()
			}

			if c.AgentMonitor.AllCompleted() {
				break
			}
		}
		// fire the completion signal (should only be read by main.go using <-controller.Done())
		c.completed <- struct{}{}

	}()

	return c, nil
}

func (c *Controller) InitializeAgentMonitor() {
	for _, agent_group := range c.DispatchMonitor.GetAllAgentGroups() {
		for name := range agent_group {
			c.AgentMonitor.NewAgentStatus(name)
		}
	}
}

func SerializeDataToByte(input any) ([]byte, error) {
	input_byte, err := json.Marshal(input)
	if err != nil {
		return nil, errors.New("failed to encode knowledge graph into bytes")
	}

	return input_byte, nil

	// buf := new(bytes.Buffer)
	// if err := gob.NewEncoder(buf).Encode(input); err != nil {
	// }
	// return buf.Bytes(), nil
}

func (c *Controller) SendKnowledgeGraph(agent_groups []map[string]*AgentSpec, agent_deps map[string][]string) error {
	// Serialize maps into []byte

	deps_serialized, err := SerializeDataToByte(agent_deps)
	if err != nil {
		return err
	}

	r_dep := &Promise{
		Name: "KnowledgeGraph",
		Type: "Result",
		Contents: &Promise_Data{
			Data: deps_serialized,
		},
	}

	if err := c.PostResult(Result_Result, r_dep); err != nil {
		return errors.New("failed to send knowledge graph to blackboard: " + err.Error())
	}

	groups_serialized, err := SerializeDataToByte(agent_groups)
	if err != nil {
		return err
	}

	r_specs := &Promise{
		Name: "AgentSpecs",
		Type: "Result",
		Contents: &Promise_Data{
			Data: groups_serialized,
		},
	}

	if err := c.PostResult(Result_Result, r_specs); err != nil {
		return errors.New("failed to send agent specs to blackboard: " + err.Error())
	}

	return nil
}

func (c *Controller) DispatchAgent(a *AgentSpec) error {

	// Override with limits as needed
	if int(a.PingIntervalMs) < config.Globals.MinPingInterval_Ms {
		a.PingIntervalMs = int32(config.Globals.MinPingInterval_Ms)
	}

	c.AgentMonitor.NewAgentStatus(a.Name)
	return c.SendMessage(&Message_CreateAgent{CreateAgent: a})
}

func (c *Controller) DispatchAgentGroup(agent_group map[string]*AgentSpec) error {
	for _, spec := range agent_group {
		err := c.DispatchAgent(spec)
		if err != nil {
			return err
		}
	}
	return nil
}

// Runners must match on names.  We dispatch runners based on type.
func (c *Controller) DispatchRunner(spec *AgentSpec) error {

	// Handle special cases (Name==type and only singleton runner is allowed)
	switch name := spec.Name; name {
	case "go":
		c.RunnerMonitor.NewAgentStatus(name)
		return NewGoRunner(c.BB, spec)
	case "local":
		c.RunnerMonitor.NewAgentStatus(name)
		return NewLocalRunner(c.BB, spec)
	}

	// TODO: Handle other case
	return fmt.Errorf("only runners 'go' and 'local' are currently supported")
}

func (c *Controller) Errors() []error {
	agent_errs := c.AgentMonitor.GetErrors()
	runner_errs := c.RunnerMonitor.GetErrors()
	return append(agent_errs, runner_errs...)
}

func (c *Controller) Done() chan struct{} {
	return c.completed
}

// GracefulShutdown sends the "Halt" signal to blackboard/agents
// and will monitor the BB for Goodbye messages to confirm clean shutdown.
func (c *Controller) GracefulShutdown() error {

	// Monitor until all agents have said goodbye
	return c.SendMessage(&Message_Halt{Halt: &Halt{}})
}

// ImmediateShutdown sends the HCF signal, returns and exits the program
// immediately.
func (c *Controller) ImmediateShutdown() error {
	return c.SendMessage(&Message_HaltAndCatchFire{HaltAndCatchFire: &HaltAndCatchFire{}})
	//return c.SendMessage(HaltAndCatchFire, []byte("Controller + Blackboard shutting down immediately"))
}
