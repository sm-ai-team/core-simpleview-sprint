// The agent was generated using SM's "generate" tool.  Go Agents
// *must* be compiled into the SM program to function.  To utilize
// this agent, copy/move this file into pkgs/agents/ and recompile.
//
// If you think your agent would be broadly useful, please consider
// submitting it back to the SimpleMind team via merge request!
//
// TODO: include link to complete go agent example
//
// TODO: include MR link.

package agents

import (
	"bytes"
	"embed"
	"encoding/json"
	"errors"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/hoffman-lab/core/pkg/core"
	"google.golang.org/protobuf/encoding/protojson"
)

var WebFS embed.FS

type WebInterface struct {
	AgentType string
	*GoAgent
}

func StartWebInterface(bb Blackboard, spec *AgentSpec) (Agent, error) {
	a := &WebInterface{
		AgentType: "web-interface",
		GoAgent:   NewGoAgent(bb, spec),
	}
	return a, nil
}

func (a *WebInterface) Run() error {
	// Await Promises
	// promises := a.AwaitData()
	// if promises == nil {
	// 	a.Log(Log_Info, "web agent context canceled; shutting down")
	// 	return nil
	// }

	// Handle knowledge graph information from Blackboard
	knowledge_graph := make(map[string][]string)

	g, err := a.Attributes.AwaitValue("graph")
	if err != nil {
		return err
	}

	buf := bytes.NewBuffer(g.([]byte))
	if err := json.NewDecoder(buf).Decode(&knowledge_graph); err != nil {
		return errors.New("failed to decode knowledge graph: " + err.Error())
	}
	buf.Reset() // Reset for safety

	// Handle agent spec information from Blackboard
	var agent_specs []map[string]interface{}

	s, err := a.Attributes.AwaitValue("specs")
	if err != nil {
		return err
	}

	buf = bytes.NewBuffer(s.([]byte))
	if err := json.NewDecoder(buf).Decode(&agent_specs); err != nil {
		return errors.New("failed to decode agent specs: " + err.Error())
	}

	// Launch SimpleMind web interface server
	r := mux.NewRouter()

	r.HandleFunc("/", a.IndexHandler(knowledge_graph, agent_specs))
	r.HandleFunc("/graph", a.GraphHandler)
	r.HandleFunc("/bb/{start_idx:[0-9]+}", a.BBHandler)

	addr, err := a.Attributes.Value("addr")
	if err != nil {
		addr = "localhost:9080"
	}

	a.PostStatusUpdate(StatusUpdate_Working, "web interface listening at "+addr.(string))

	return http.ListenAndServe(addr.(string), r)
}

func (a *WebInterface) IndexHandler(knowledge_graph map[string][]string, agent_specs []map[string]interface{}) http.HandlerFunc {
	// Parse the index.html template
	tmpl, err := template.ParseFS(WebFS, "web/template/index.html")
	if err != nil {
		log.Error("failed to parse template: ", err)
		return nil
	}

	// Gather data that needs to be sent to the webpage on launch
	data := make(map[string]interface{})
	data["time_sent"] = time.Now().UTC().Format("2006-01-02T15:04:05.000Z")

	graph_byte, err := json.Marshal(knowledge_graph)
	if err != nil {
		log.Error("failed to marshal knowledge graph to json: ", err)
	}
	data["graph"] = string(graph_byte)

	specs_byte, err := json.Marshal(agent_specs)
	if err != nil {
		log.Error("failed to marshal agent specs to json: ", err)
	}
	data["specs"] = string(specs_byte)

	// Return the handler function, which executes the template with the given data.
	// This pattern is a quirky one--it allows us to pass in variables while maintaining
	// the conventional http.HandleFunc syntax (which is why we wrap the HandleFunc itself
	// in the IndexHandler). What's important here is that the variables we are passing
	// into the handler are **not** obtainable from the WebInterface itself, unlike the
	// BBHandler, which is why we wrap it.
	return func(w http.ResponseWriter, r *http.Request) {
		if err := tmpl.Execute(w, data); err != nil {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
	}
}

func (a *WebInterface) GraphHandler(w http.ResponseWriter, r *http.Request) {
	msgs, err := a.BB.GetMessages(0, true)
	if err != nil {
		http.Error(w, "Blackboard error: "+err.Error(), http.StatusInternalServerError)
	}

	// TODO: Eventually, we intend to allow the JS side of the web agent to view
	// and style agents from StatusUpdate messages according to the following styling:
	// // "Hello" but no StatusUpdate --> white border
	// // "Waiting" --> timer animation
	// // "Working" --> green border (or spinning cog animation?)
	// // "Error" --> red border
	// // "Goodbye" (or no "Hello" yet) --> reduce opacity (or gray border)
	// If this is an important feature to you, please create an issue and we will prioritize it.
	tmp := &MessageStack{}
	for _, m := range msgs {
		if _, is_hello := m.Contents.(*Message_Hello); is_hello {
			tmp.Messages = append(tmp.Messages, m)
		} else if _, is_goodbye := m.Contents.(*Message_Goodbye); is_goodbye {
			tmp.Messages = append(tmp.Messages, m)
		}
	}

	data, err := protojson.Marshal(tmp)
	if err != nil {
		log.Error("failed to encode to json: ", err)
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(data)
	if err != nil {
		log.Error("failed to transmit json: ", err)
	}
}

func (a *WebInterface) BBHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	start_idx, ok := params["start_idx"]
	if !ok {
		start_idx = "0"
	}

	start, err := strconv.Atoi(start_idx)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
	}

	msgs, err := a.BB.GetMessages(start, true)
	if err != nil {
		http.Error(w, "Blackboard error: "+err.Error(), http.StatusInternalServerError)
	}

	tmp := &MessageStack{
		Messages: msgs,
	}

	data, err := protojson.Marshal(tmp)
	if err != nil {
		log.Error("failed to encode to json: ", err)
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(data)
	if err != nil {
		log.Error("failed to transmit json: ", err)
	}
}
