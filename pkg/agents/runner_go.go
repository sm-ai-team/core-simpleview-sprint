package agents

import (
	"fmt"

	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type GoRunner struct {
	*Runner
}

func NewGoRunner(bb Blackboard, spec *AgentSpec) error {
	r, err := StartRunner(bb, spec)
	if err != nil {
		return err
	}

	gr := &GoRunner{
		Runner: r,
	}

	gr.RunAgent = gr.RunGoAgent

	return nil
}

func (gr *GoRunner) RunGoAgent(spec *AgentSpec) error {

	start_func, ok := AgentEntrypoints[spec.Entrypoint]
	if !ok {
		return fmt.Errorf("unrecognized start function " + string(spec.Entrypoint))
	}

	// WHERE ARE ATTRIBUTES GETTING PARSED FOR GO AGENTS???

	agent, err := start_func(gr.BB, spec)
	if err != nil {
		return fmt.Errorf("agent %s failed to start : %s", spec.Name, err)
	}

	agent.Start()

	err = agent.Run()
	if err != nil {
		gr.Log(Log_Error, fmt.Sprintf("agent %s exited with error: %s", spec.Name, err))
	}

	agent.Stop() // call the context's cancel function

	// Block until both Run and Start have returned
	<-agent.Done()

	return nil
}
