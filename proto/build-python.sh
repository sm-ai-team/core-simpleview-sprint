#!/bin/bash

mkdir -p generated/python
python -m grpc_tools.protoc --python_out=./generated/python --pyi_out=./generated/python/ --grpc_python_out=./generated/python/ --proto_path=./ types.proto