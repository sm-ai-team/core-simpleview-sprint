#!/bin/bash

mkdir -p generated/go
protoc --go_out=paths=source_relative:./generated/go/ --go-grpc_out=paths=source_relative:./generated/go/ types.proto
#protoc --go_out=./generated/go/ types.proto