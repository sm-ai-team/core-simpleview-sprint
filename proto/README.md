# Proto

`types.proto` is the source of truth for the wireline protocol (and other serialization) used in the core.  It also specifes the ([currently unsupported](https://gitlab.com/hoffman-lab/core/-/issues/79)) gRPC service and API for the blackboard.

## Building (recommended method)

You will need `make` and `docker` installed to build

```
make all
```

If the files build successfully, a directory should be created for each language:

```
generated/python/<files>
generated/go/<files>
```

in which you can find the updated, language-specific protocols.

You will need to copy them into the correct project locations for the project to utilize them. (Although if you find that you're needing to do this, you should probably have a quick chat with John first).

## Building (not recommended)

If for some reason you need to build/rebuild proto files a lot, the docker build workflow can get a little tedious.  You can install `protoc` and the required plugins `proto-gen-go`, `protoc-gen-go-grpc`, and python's `grpcio-tools` and run the commands locally to build the files.  After being built, the files will need to be copied to their final locations.

To discourage people from using this approach, I am not going to provide the commands to do this, but if you have questions you are free to message John or file an issue.