# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of sm-core-go, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from pkg.src.smcore import Agent, default_args, Log, AgentState
import traceback

import pandas as pd

import os
import time
import base64

class LoadTest(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)

    def load_test(self, message_size, duration):

        # Base64 is ugly and clumsy, but also kind of required here (for now)
        msg = os.urandom(message_size)
        msg = base64.b64encode(msg)
        msg = msg.decode("ascii")
        
        start = time.time()
        n_messages = 0
        while (time.time() - start) < duration:
            self.post_result("msg-" + str(start), "load-test", msg)
            n_messages = n_messages + 1

        # remove the final count (we want to report # messages
        # completed *before* duration has elapsed)
        n_messages = n_messages - 1
        n_bytes = n_messages * len(msg) # record the actual number of bytes sent (different b/c of base64 encoding)
        
        return int(n_messages), int(n_bytes)
        
    async def run(self):
        try:

            await asyncio.sleep(3)
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from LoadTest")

            data = {"message_size": [32 , 64 , 512 , 4096 , 1_000_000 , 4_000_000 , 10_000_000 , 100_000_000] ,
                    "duration":     [3  , 3  ,  3  ,   3  ,        3  ,        3  ,         3  ,          10] ,
                    "n_messages":   [ 0 ,  0 ,   0 ,    0 ,         0 ,         0 ,          0 ,           0] ,
                    "n_bytes":      [ 0 ,  0 ,   0 ,    0 ,         0 ,         0 ,          0 ,           0]}
                
            df = pd.DataFrame(data)
            
            def do_load_test(row):
                
                msg_size = row["message_size"]
                duration = row["duration"]
                
                self.log(Log.INFO, "starting load test: {} byte messages, {} seconds".format(msg_size, duration))

                result_name = "load-test-{}-{}".format(msg_size, duration)
                result_type = "load-test-result"

                # Run the test
                n_msg, n_bytes = self.load_test(row["message_size"], row["duration"])
                
                d = {"n_messages": n_msg, "throughput": float(n_bytes)*float(8)/float(1_000_000)/float(duration)}
                
                self.post_result(result_name, result_type, d)
                self.log(Log.INFO, "{} byte messages: sent {} messages in {} seconds ({} Mbps)".format(msg_size, n_msg, duration, d["throughput"]))
                #print(row["message_size"], row["duration"], self.load_test(row["message_size"], row["duration"]))
                
            
            df.apply(lambda row: do_load_test(row), axis = 1)

            self.log(Log.INFO, "{}".format(df["n_messages"]))
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("load_test.py")
    t = LoadTest(spec, bb)
    t.launch()

