# Changelog

This changelog is specific to the Python package.

## [0.0.57]
- **Adds support for wildcard promises in python agents**

## [0.0.56]
- **Sane defaults for pingInterval_ms and pollInterval_ms**
- Minimum allowed values of pingInterval\_ms and pollInterval\_ms (33ms, for server sparing)
- Fixed bug that did not correctly parse agent names with dashes ("-") in them (e.g. my-test-agent)
- Adds \_get\_result\_messages() method to BlackboardHTTPClient (not yet ready for general use)

[0.0.57]: https://pypi.org/project/smcore/0.0.57/
[0.0.56]: https://pypi.org/project/smcore/0.0.56/
[0.0.55]: https://pypi.org/project/smcore/0.0.55/
[0.0.54]: https://pypi.org/project/smcore/0.0.54/