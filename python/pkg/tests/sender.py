# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link
import os
import sys
sys.path.append(os.path.abspath('..'))

import asyncio
from src.smcore import Agent, default_args, Log, AgentState
import traceback

class Sender(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:            
            # Your code HERE
            self.log(Log.INFO,"hello from Sender")
            self.post_result("fulfilled", "hug", "big squeeze")

        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("sender.py")
    t = Sender(spec, bb)
    t.launch()

