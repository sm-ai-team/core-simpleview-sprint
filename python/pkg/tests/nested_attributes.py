# run using the following commands:
# ./core server 
# python nested_attributes.py --blackboard localhost:8080 --spec '{"name":"na-agent","attributes":{"data":"value","tlpromise":"Promise(data1 as type1 from agent1)", "moredata":{"deeper":"dictionary", "nestedpromise":"Promise(data2 as type2 from type2)"}}}'

import os
import sys
sys.path.append(os.path.abspath('..'))

import asyncio

from src.smcore import default_args
from src.smcore import PromiseState, AgentState
from src.smcore import Agent, AgentSpec

class NestedAttributesTester(Agent):
    def __init__(self, spec, bb):
        super().__init__(spec,bb)

    async def run(self):
        try:
            
            # See if we can read a regular attribute valueb
            v = self.attributes.value("data")
            if v != "value":
                print("ERROR: expected 'value' but got ", v)
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":
    spec, bb = default_args("nested_attributes.py")
    test_agent = NestedAttributesTester(spec, bb)
    test_agent.launch()