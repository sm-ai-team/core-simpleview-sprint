import os
import sys
sys.path.append(os.path.abspath('..'))

import asyncio
#from smcore import Agent, default_args, Log, AgentState
from src.smcore import Agent, default_args, Log, AgentState
import traceback

# For testing, this agent should be connected to an empty blackboard
# running in server mode: `./core server`

# To run: python run_tests.py --spec spec.json

# TODO: This is really an awful test harness that needs some love at some point.

# define Python user-defined exceptions
class TestFailureException(Exception):
    def __init__(self, message):
         super().__init__(message)
    
class Receiver(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            PASSED = True
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from Receiver")

            ### Promise Strings (issue 61)
            ## Reading from a fulfilled promise should be fine
            try:
                await asyncio.sleep(2)
                data = self.attribute("fulfilled")
                self.log(Log.INFO, "PASS: read data successfully from promise")
                print("PASS: read data successfully from promise")
            except Exception as e:
                # TODO: Actually validate that this is the expected exception
                self.log(Log.WARNING, "FAIL: exception thrown trying to read fulfilled promise")
                #print("FAIL: exception thrown trying to read fulfilled promise")
                print(traceback.format_exc())
                raise TestFailureException("failed trying to read fullfilled promise (should have succeeded)")
            
            ## Reading an attribute before a promise has been been fulfilled
            ## should throw an exception.
            try:
                data = self.attribute("unfulfilled")
                self.log(Log.WARNING, "FAIL: read data successfully from unfulfilled promise")
                print("FAIL: read data successfully from unfulfilled promise")
                PASSED = False
            except Exception as e: # TODO: we should probably have a couple of built-in exception types that are more specific
                # TODO: Actually validate that this is the expected exception
                self.log(Log.INFO, "PASS: exception thrown trying to read unfulfilled promise")
                print("PASS: exception thrown trying to read unfulfilled promise (above)")
                
            if PASSED:
                self.log(Log.INFO, "ALL TESTS PASSED")
                print("ALL TESTS PASSED")
            else:
                self.log(Log.ERROR, "TESTS FAILED")
                print("TEST FAILED")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("receiver.py")
    t = Receiver(spec, bb)
    t.launch()

