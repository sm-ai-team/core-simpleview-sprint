import os
import sys
sys.path.append(os.path.abspath('..'))

import asyncio
#from smcore import Agent, default_args, Log, AgentState
from src.smcore import parse_promise, PromiseSet, PromiseState
from src.smcore import Agent, AgentSpec

PASSES = True

spec = AgentSpec(attributes = {
    "second": "Promise( hello as hellotype from   silly_goose )",
    "notpromise":"10"})

promise_set = PromiseSet(spec)
if promise_set.all_fulfilled():
    PASSES = False
    print("FAIL: promises are not fulfilled")

if len(promise_set.promises) > 1:
    PASSES = False
    print("FAIL: too many promises")

# "fulfill" the promise
promise_set.promises["second"].state = PromiseState.FULFILLED

if not promise_set.all_fulfilled:
    PASSES = False
    print("FAIL: promises should be fulfilled")

# WILDCARD HANDLING TESTS
# Check that we correctly parse wildcards in all fields
p = parse_promise("Promise(hello as greeting from *)")
if p.source != "*":
    PASSES = False
    print("FAIL: failed to detect source wildcard '*'")

p = parse_promise("Promise(hello as * from test-agent)")
if p.type != "*":
    PASSES = False
    print("FAIL: failed to detect type wildcard '*'")

p = parse_promise("Promise(* as greeting from test-agent)")
if p.name != "*":
    PASSES = False
    print("FAIL: failed to detect name wildcard '*'")

# Check that "contains" correctly triggers in the presences of wildcards
spec = AgentSpec(attributes = {
    "any_agent": "Promise(hello as greeting from *)",
    "any_type":  "Promise(some-data as * from test-agent)",
    "any_data":  "Promise(* as lung-ct-data from test-agent)",
    "permissive_arent_we": "Promise(* as * from reliable-provider)"})

ps = PromiseSet(spec)

# result source agent wildcards
p1 = parse_promise("Promise(hello as greeting from test-agent)")
p2 = parse_promise("Promise(hello as greeting from totally-diff-agent)")

matched_attr, matched = ps.contains(p1)
if matched_attr != "any_agent" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (source)")

matched_attr, matched = ps.contains(p2)
if matched_attr != "any_agent" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (source)")

# result type wildcar
p1 = parse_promise("Promise(some-data as data-result-type from test-agent)")
p2 = parse_promise("Promise(some-data as different-result-type from test-agent)")

matched_attr, matched = ps.contains(p1)
if matched_attr != "any_type" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")

matched_attr, matched = ps.contains(p2)
if matched_attr != "any_type" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")

# result name wildcard
p1 = parse_promise("Promise(data-result-1 as lung-ct-data from test-agent)")
p2 = parse_promise("Promise(data-result-2 as lung-ct-data from test-agent)")

matched_attr, matched = ps.contains(p1)
if matched_attr != "any_data" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")

matched_attr, matched = ps.contains(p2)
if matched_attr != "any_data" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")    

# Double wild card
p1 = parse_promise("Promise(never-post-phi as tax-forms from reliable-provider)")
p2 = parse_promise("Promise(never-share-phi as medical-records from reliable-provider)")

matched_attr, matched = ps.contains(p1)
if matched_attr != "permissive_arent_we" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")

matched_attr, matched = ps.contains(p2)
if matched_attr != "permissive_arent_we" or not matched:
    PASSES = False
    print("FAIL: did not match p1 despite wild card (type)")

# Print final conclusion
if PASSES:
    print("PASS: all promise_set tests pass")
else:
    print("FAIL: one or more promise tests failed")