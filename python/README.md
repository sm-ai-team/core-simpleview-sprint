# Build and Deploy Notes

Last updated: 12/9/2023

# Building docker images

All "official" building should be done via the Makefile in this directory.

```bash
# Build local images for local use
make python # python+smcore base image
make pytorch # pytorch+smcore base image

# Build all images, architectures and push to dockerhub (auth required)
make deploy
```

If using the Makefile, VERSION is will be overwritten by Makefile.

VERSION is used by setuptools.py to configure the installed version number of smcore in the container.

To check the version of pytorch+smcore installed use:

```
make pytorch
docker run -it --rm --entrypoint "./pytorch_test.py" johnmarianhoffman/smcore-pytorch
```

## Building the package for PyPi

Building for PyPi should also be done via the Makefile **in** `pkg`.

I'm still figuring out exactly what the correct way to do this is, but for now:

```
cd pkg
make build
make upload
```

Version numbers are also handled automatically, using the exact same approach as when deploying.

Development versions (any commit between tags) won't be able to be uploaded PyPi.  Only "full" version tags are expected to push correctly for now.  If needed in the future, we can research pushing prerelease versions.