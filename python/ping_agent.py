# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio

from pkg.src.smcore import Agent, default_args, Log, AgentState

import traceback

class PingAgent(Agent):
    
    def __init__(self, spec, bb):
        # The "attribute not a promise" error shows up here
        super().__init__(spec, bb)

    async def run(self):
        try:
            self.log(Log.INFO,"Hello from PingAgent!")
            
            #dwell_time = int(self.attribute("dwell"))
            dwell_time = int(self.attributes.value("dwell"))

            self.post_status_update(AgentState.WORKING, "waiting for {} seconds...".format(dwell_time))
            
            await asyncio.sleep(dwell_time)

            self.post_result("SpecialPing","PingPongResult", "some silly data")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":   
    spec, bb = default_args("ping_agent.py")
    t = PingAgent(spec, bb)
    t.launch()

