#!/usr/bin/env python

# This is not a real agent, but exists to double-check that we
# can successfully import smcore in our base docker images
import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

import sys
from importlib.metadata import version

print("python: " + sys.version)
print("smcore: " + version('smcore'))