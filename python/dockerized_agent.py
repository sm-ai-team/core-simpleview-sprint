# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

class DockerizedAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            self.log(Log.DEBUG,"hello from DockerizedAgent")
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("dockerized_agent.py")
    t = DockerizedAgent(spec, bb)
    t.launch()

