# This agent was generated using SM's "generate" tool.
#
# To start using your agent immediately, move it into the "python"
# directory in the root directory of core, and configure it to
# utilize the "local" runner.
#
# More generally, python agents do NOT need to be compiled into SM for
# general use, however keep in mind that the runner the agent is
# matched with must be able to (1) find the script to start it, and (2)
# resolve any of your script's dependencies (i.e. environment management)
#
# If you think your agent would be broadly useful, please consider
# submitting it back to the SimpleMind team via merge request!
#
# TODO: include link to complete python agent example
#
# TODO: include MR link

#from enums import Log, AgentState
#from agent_http import AgentHTTP
#
#from default_args  import default_args
#
#import traceback

import asyncio

from pkg.src.smcore import Agent, default_args, Log, AgentState

import traceback

class PongAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:

            self.log(Log.INFO,"Hello from PongAgent!")

            # Ensure all data is delivered and catch any obvious issues
            # with await_data (still technically part of the API)
            await self.await_data()

            # Doubling up a bit here on tests (should really be moved to
            # a dedicated test).
            # Verify that await_value behaves as expected on non-promise attributes
            test = await self.attributes.await_value("test")
            expected = "some random data"
            if test != expected:
                raise Exception("test: {}; expected {}".format(test, expected))

            # Verify that value also returns data
            # NOTE/TODO: with the current controller setup, we don't have a great way to
            #    test the keyerror except return state of .value. In the future, we should
            #    best testing this as well as the consistency of the returned data.
            data = self.attributes.value("pong")
            self.log(Log.INFO, "2got data: {}".format(data))
            
            # Await the value (which will have already been written to the blackboard
            data = await self.attributes.await_value("pong")
            self.log(Log.INFO, "got data: {}".format(data))

            # Just for fun
            self.post_result("SpecialPong", "PingPongResult", None)

        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("pong_agent.py")
    t = PongAgent(spec, bb)
    t.launch()

