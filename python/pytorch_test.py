#!/usr/bin/env python

# This is not a real agent, but exists to double-check that we
# can successfully import smcore in our base docker images
import asyncio
from smcore import Agent, default_args, Log, AgentState
import traceback

# Verify we can import torch
import torch
from torch import nn

from importlib.metadata import version
version = version('smcore')

# TODO: Have a version function that reports the current version of smcore installed
print("pytorch: " + torch.__version__)
print("smcore:  " + version)
