# dicom_uploader puts base64 encoded dicom data onto the blackboard
#
# required attributes:
#   path (string)                // DICOM directory
#   extensions (list of strings) // dicom extensions to load
#
# the contents of each result can be saved into a file
# or parsed directly in memory (maybe, I dunno if pydicom can
# actually do this, but maybe.)
#
# If it can work, it'll probably look something like:
#
# data = await self.attributes.await_value("dcm-data")
# data = base64.b64decode(data.decode("ascii"))
# buff = io.BytesIO(data)
# ds = pydicom.dcmread(buff)
# 
# It also can likely be made significantly more efficent
#
# This agent was generated using SM's "generate" tool.
#
#   ./core generate python-agent --name dicom-uploader

import asyncio
#from smcore import Agent, default_args, Log, AgentState
from pkg.src.smcore import Agent, default_args, Log, AgentState
import traceback

import os
import pydicom
import base64

class DicomUploader(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            self.log(Log.INFO,"Hello from DicomUploader")
            
            dcm_path = self.attributes.value("path")
            dcm_extensions = self.attributes.value("extensions").split(",")
            
            for f in os.listdir(dcm_path):
                filename = os.fsdecode(f)

                (_, ext) = os.path.splitext(filename)
                
                if ext in dcm_extensions:
                    fullpath = os.path.join(dcm_path, filename)
                    
                    with open(fullpath, 'rb') as f_dcm:
                        self.log(Log.INFO, "uploading: " + filename)
                        raw_data = f_dcm.read()

                        msg = base64.b64encode(raw_data)
                        msg = msg.decode("ascii")
                        
                        self.post_result(filename, "dicom-file", msg)
            
        except Exception as e:
            self.post_status_update(AgentState.ERROR, traceback.format_exc())
            print(traceback.format_exc())
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("dicom_uploader.py")
    t = DicomUploader(spec, bb)
    t.launch()

