# docker build . -f Dockerfile_2 -t core
# docker image ls | grep core | grep latest

# Stage 1: Build container to compile our software
FROM --platform="$TARGETPLATFORM" golang:alpine as BUILDER

RUN mkdir /app
WORKDIR /app

# Fetch + cache dependencies
# (only reruns if go.mod or go.sum changes)
COPY go.mod ./go.mod
COPY go.sum ./go.sum
RUN go mod download

# Copy project files
# Actual go build is reasonably quick, so we aren't too careful here
COPY ./ ./
RUN go build

ARG TARGETPLATFORM

# Stage 2: Runtime container with minimal dependencies
# Minimal runtime environment w/ only executable available
FROM --platform="$TARGETPLATFORM" alpine:latest as RUNNER

# COPY can also transfer data/artifacts between stages
COPY --from=BUILDER /app/core /core
ENTRYPOINT ["./core", "server", "--addr", ":8080"]