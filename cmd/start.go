/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start different core services (subcommand required)",
}

func init() {
	rootCmd.AddCommand(startCmd)
}
