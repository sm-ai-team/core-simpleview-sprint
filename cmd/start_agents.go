/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strings"

	"net"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hoffman-lab/core/pkg/agents"
	"gitlab.com/hoffman-lab/core/pkg/config"
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

// agentsCmd represents the agents command
var agentsCmd = &cobra.Command{
	Use:   "agents",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: StartAgents,
}

// Get preferred outbound ip of this machine
func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func init() {
	startCmd.AddCommand(agentsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// agentsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// agentsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func DispatchRunner(bb Blackboard, spec *AgentSpec) error {
	log.Info("dispatching runner: ", spec.Name)
	switch spec.Name {
	case "go":
		return agents.NewGoRunner(bb, spec)
	case "local":
		return agents.NewLocalRunner(bb, spec)
	}
	return fmt.Errorf("only runners 'go' and 'local' are currently supported")
}

func DispatchAgent(spec *AgentSpec) error {

	// Override with limits as needed
	if int(spec.PingIntervalMs) < config.Globals.MinPingInterval_Ms {
		spec.PingIntervalMs = int32(config.Globals.MinPingInterval_Ms)
	}

	return nil //c.SendMessage(&Message_CreateAgent{CreateAgent: a})

}

func StartAgents2(BB Blackboard, conf *agents.AgentsConfig) (chan struct{}, error) {

	// Make sure we have a valid blackboard interface
	var err error
	if BB == nil {
		// This gives us the http link to send messages to the blackboard
		BB, err = NewHTTPBlackboard(config.Globals.BlackboardHTTPAddress)
		if err != nil {
			log.Fatal("failed to establish blackboard connection: ", err)
		}

	}

	// Dispatch Runners (required to start any other agents)
	// Although they are agents, everything else, including the controller,
	// requires a runner.
	for _, spec := range conf.Runners {
		err := DispatchRunner(BB, spec)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Create our Go agent for the EXE (note, this is NOT the controller
	// this agent just bootstraps everything else and reflects that it
	// is a call to the executable, and not an independent agent)
	//
	// We make a point to utilize our own interfaces to achieve the
	// desired behavior.
	ip := GetOutboundIP().String()
	ip = strings.Replace(ip, ".", "-", -1)

	exe_spec := &AgentSpec{
		Name: "core-exe-" + ip,
	}

	type exeAgent struct {
		*GoAgent
		completed chan struct{}
	}

	exe_agent := &exeAgent{
		GoAgent:   NewGoAgent(BB, exe_spec),
		completed: make(chan struct{}),
	}

	exe_agent.Start()

	// Create the controllers
	//
	// NOTE: we do not require a controller
	// there could be use cases where we want a preexisting controller
	// to schedule our agents.
	if conf.Controllers != nil {
		for name, spec := range conf.Controllers {
			spec.Name = name
			exe_agent.PostCreateAgent(spec)
		}
	}

	if conf.Controllers == nil && config.Globals.UseDefaultController {
		conf.Controllers = make(map[string]*AgentSpec)
		controller_spec := agents.GetDefaultControllerSpec()
		exe_agent.PostCreateAgent(controller_spec)
		conf.Controllers[controller_spec.Name] = controller_spec
	}

	// Finally, we put our agent-bundle onto the blackboard
	// for a controller to pick up and begin executing
	data, err := json.Marshal(conf.Agents) // This may not behave correctly since protojson is not working on a map (eye roll)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal agent bundle: %s", err)
	}

	exe_agent.PostResult(Result_Result, &Promise{
		Name:   "agent-bundle-" + exe_spec.Name,
		Type:   "agent-bundle",
		Source: exe_spec.Name,
		State:  Promise_Final,
		Contents: &Promise_Data{
			Data: data,
		},
	})

	// exe_agent monitors for the controller(s) shutdown
	count := 0
	exe_agent.OnNewMessage = func(m *Message) {
		switch m.Contents.(type) {
		case *Message_Goodbye:

			_, exists := conf.Controllers[m.Source]
			if exists {
				count++
			}

			if count == len(conf.Controllers) {
				exe_agent.completed <- struct{}{}
			}

		}

	}

	return exe_agent.completed, nil
}

func StartAgents(cmd *cobra.Command, args []string) {

	a := &agents.AgentsConfig{}
	if err := a.Load(args[0]); err != nil {
		log.Fatal("could not parse agents: ", err)
	}

	completed, err := StartAgents2(nil, a)
	if err != nil {
		log.Fatal("failed to bootstrap agents: ", err)
	}

	// Listen for system signals (Ctrl-C)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		count := 0
		for range c {
			count++
			if count == 1 { // 1 press of ctrl-c
				completed <- struct{}{}
			}

			if count == 2 { // 2 press of ctrl-c
				log.Fatal("immediate shutdown (blackboard not saved)")
			}
		}
	}()

	<-completed

	// Execute a clean shutdown (what does this mean for agents?)
}
