/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/hoffman-lab/core/pkg/config"

	_ "embed"
)

//go:embed VERSION
var version string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "core",
	Short: "The \"sm\" tool (maybe \"core\")",
	Long: `The sm tool (short for simple mind) is a
command line tool use to interact with simple mind.
You can run a knowledge base, start a blackboard
server, etc.  See help for complete info.`,
	Version: version,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {

	if strings.Contains(rootCmd.Version, "prerelease") {
		rootCmd.Version = rootCmd.Version + "-" + time.Now().Format("20060102")
	}

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.core.yaml)")
	rootCmd.PersistentFlags().BoolVar(&config.Globals.QuietBlackboard, "quiet-bb", config.Globals.QuietBlackboard, "disable stdout from blackboard")
	rootCmd.PersistentFlags().BoolVar(&config.Globals.ShowPings, "show-pings", config.Globals.ShowPings, "enable display of agent pings in blackboard")
	rootCmd.PersistentFlags().BoolVar(&config.Globals.NoSplash, "no-splash", config.Globals.NoSplash, "disable the simple mind splash message")
	rootCmd.PersistentFlags().IntVar(&config.Globals.MinPingInterval_Ms, "min-ping", config.Globals.MinPingInterval_Ms, "minimum allowed ping interval in ms")
	rootCmd.PersistentFlags().StringVar(&config.Globals.OutputFile, "output-file", config.Globals.OutputFile, "output file, where blackboard will be saved as JSON")
	rootCmd.PersistentFlags().IntVar(&config.Globals.OutputWidth, "output-width", config.Globals.OutputWidth, "control output width of blackboard (all other output unaffected). N==0 is auto (may not work in all environments); N<0 do not truncate BB output; N>0 will restrict to N columns")
	rootCmd.PersistentFlags().StringVar(&config.Globals.BlackboardHTTPAddress, "addr", config.Globals.BlackboardHTTPAddress, "hostname:port on which to run blackboard (HTTP)")
	rootCmd.PersistentFlags().StringVar(&config.Globals.MongoBBSpec, "mongo", config.Globals.MongoBBSpec, "enable mongo blackboard using path to spec as YAML")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolVar(&config.Globals.ShowPings, "show-pings", config.Globals.ShowPings, "enable/disable ping display in blackboard (default: false)")

}
