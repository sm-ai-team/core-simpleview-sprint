// The agent was generated using SM's "generate" tool.  Go Agents
// *must* be compiled into the SM program to function.  To utilize
// this agent, copy/move this file into pkgs/agents/ and recompile.
//
// If you think your agent would be broadly useful, please consider
// submitting it back to the SimpleMind team via merge request!
//
// TODO: include link to complete go agent example
//
// TODO: include MR link.

package agents

import (
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

type {{.TypeName}} struct {
	AgentType string
	*GoAgent
}

func Start{{.TypeName}}(bb Blackboard, spec *AgentSpec) (Agent, error) {
	a := &{{.TypeName}}{
		AgentType: "{{.RFCName}}",
		GoAgent: NewGoAgent(bb, spec),
	}
	return a, nil
}

func (m *{{.TypeName}}) Run() error {
	
	// Your code here

	return nil
}
