# Agent Templates

## Create new agents

To create new agents from these templates:

```
./core generate python-agent [name]
```

and 

```
./core generate go-agent [name]
```

and are NOT meant to be copied/edited directly.

## Notes

These templates are actually embedded into the executable for `core`, so if changes need to be made to the template, the main executable must be recompiled before the changes will be visible.