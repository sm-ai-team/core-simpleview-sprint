/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hoffman-lab/core/pkg/agents"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Execute a configuration as standalone application",
	Long:  `run creates a blackboard server and agents`,
	Run:   RunConfigFile,
}

func init() {
	rootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func RunConfigFile(cmd *cobra.Command, args []string) {

	a := &agents.AgentsConfig{}
	if err := a.Load(args[0]); err != nil {
		log.Fatal("could not parse agents: ", err)
	}

	BB, comms, err := StartServer2()
	if err != nil {
		log.Fatal("during run, failed to start blackboard server:", err)
	}

	completed, err := StartAgents2(BB, a)
	if err != nil {
		log.Fatal("during run, failed to bootstrap agents: ", err)
	}

	// TODO: How to monitor for controller shutdown?
	//       this is where it will need to happen

	// Listen for system signals (Ctrl-C)
	// to handle graceful shutdowns
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		count := 0
		for range c {
			count++
			if count == 1 { // 1 press of ctrl-c
				completed <- struct{}{}
			}

			if count == 2 { // 2 press of ctrl-c
				log.Fatal("immediate shutdown (blackboard not saved)")
			}
		}
	}()

	<-completed

	// Shut down APIs
	for idx := range comms {
		err := comms[idx].Shutdown()
		if err != nil {
			log.Error("error while shutting down: ", err)
		}
	}

	// Executing blackboard shutdown
	BB.Shutdown()

}
