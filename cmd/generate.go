/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"strings"
	"text/template"

	"embed"

	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hoffman-lab/core/pkg/core"
)

var f embed.FS // to keep flymake from stripping the import (ugh)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: GenerateEntrypoint,
}

var agent_name string

func init() {

	python_agent_cmd := &cobra.Command{
		Use:   "python-agent",
		Short: "Generate an empty, working python agent",
		Run:   GeneratePythonAgent,
	}

	go_agent_cmd := &cobra.Command{
		Use:   "go-agent",
		Short: "Generate an empty, working go agent",
		Run:   GenerateGoAgent,
	}

	rootCmd.AddCommand(generateCmd)
	generateCmd.AddCommand(python_agent_cmd)
	generateCmd.AddCommand(go_agent_cmd)

	command_group := []*cobra.Command{python_agent_cmd, go_agent_cmd}

	for _, curr_cmd := range command_group {
		curr_cmd.Flags().StringVar(&agent_name, "name", "", "agent name")
		curr_cmd.MarkFlagRequired("name")
	}

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func GenerateEntrypoint(cmd *cobra.Command, args []string) {
	log.Fatal("not enough arguments")
	cmd.Usage()
}

// TODO: Empty agent template generation can be combined into one function.

//go:embed templates/empty_agent.go.tpl
var go_tpl string

func GenerateGoAgent(cmd *cobra.Command, args []string) {

	agent_type := fmt.Sprintf("%s", cmd.Flag("name").Value)

	fmt.Printf("Generating empty go agent type: %s\n", agent_type)

	_, err := core.ValidateAgentName(agent_type)
	if err != nil {
		log.Fatal(err)
	}

	//go_tpl, found := strings.CutPrefix(go_tpl, "//go:build exclude // Do not modify this line")
	//if !found {
	//	log.Fatal("Expected \"//go:build exclude\" directive in templates/empty_agent.go, but did not find it. This is a critical error. Did something recently change?")
	//}

	// Prep the different versions of our name
	file_name := strings.Replace(agent_name, "-", "_", -1) + ".go"
	type_name := strings.Replace(strings.Title(strings.Replace(agent_name, "-", " ", -1)), " ", "", -1)

	data := struct {
		RFCName  string
		TypeName string
	}{
		RFCName:  agent_type,
		TypeName: type_name,
	}

	f, err := os.Create(file_name)
	if err != nil {
		log.Fatal(err)
	}

	tpl, err := template.New("go-agent").Parse(go_tpl)
	if err != nil {
		log.Fatal("failed to parse template: ", err)
	}
	err = tpl.Execute(f, data)
	if err != nil {
		log.Fatal("failed to render template: ", err)
	}

	fmt.Printf("Agent file written to: %s\n", file_name)
	fmt.Println("To use your agent with SM, please read the comment at the top of the generated file.")
}

//go:embed templates/empty_agent.py.tpl
var python_tpl string

func GeneratePythonAgent(cmd *cobra.Command, args []string) {
	agent_type := fmt.Sprintf("%s", cmd.Flag("name").Value)

	fmt.Printf("Generating empty python agent type: %s\n", agent_type)

	_, err := core.ValidateAgentName(agent_type)
	if err != nil {
		log.Fatal(err)
	}

	// Prep the different versions of our name
	file_name := strings.Replace(agent_name, "-", "_", -1) + ".py"
	type_name := strings.Replace(strings.Title(strings.Replace(agent_name, "-", " ", -1)), " ", "", -1)

	data := struct {
		RFCName  string
		TypeName string
		FileName string
	}{
		RFCName:  agent_type,
		TypeName: type_name,
		FileName: file_name,
	}

	f, err := os.Create(file_name)
	if err != nil {
		log.Fatal(err)
	}

	tpl, err := template.New("py-agent").Parse(python_tpl)
	if err != nil {
		log.Fatal("failed to parse template: ", err)
	}
	err = tpl.Execute(f, data)
	if err != nil {
		log.Fatal("failed to render template: ", err)
	}

	fmt.Printf("Agent file written to: %s\n", file_name)
	fmt.Println("To use your agent with SM, please read the comment at the top of the generated file.")
}
