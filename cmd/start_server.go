// This file is DEPRECATED. We intend to roll all functionality into
// the `run.go` command, and accomplish different control behavior using
// different controller agents, rather than a separate command/mode like this.
// This will remain available for the time being, but disappear soon.
//
// Please see https://gitlab.com/hoffman-lab/core/-/issues/117

package cmd

import (
	"net/http"
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hoffman-lab/core/pkg/apis"
	"gitlab.com/hoffman-lab/core/pkg/config"
	. "gitlab.com/hoffman-lab/core/pkg/core"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "run an SM blackboard server",
	Long:  ``,
	Run:   StartServer,
}

func init() {
	rootCmd.AddCommand(serverCmd)  // TODO: deprecate this w/ a message to users
	startCmd.AddCommand(serverCmd) // Add the the start command

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}

var use_web_interface bool = true

func StartServer2() (Blackboard, []apis.CoreAPI, error) {

	if !config.Globals.NoSplash {
		SMSplash()
	}

	log.Info("Starting `core` server...\n")
	//log.Infof("Blackboard API address: %s\n", address)

	// Instantiate the object store Object store needs to run as a
	// dedicated "service", even if our executable is ultimately
	// bootstrapping it into existence.  This is because we cannot (or
	// choose not to) expose any object storage functionality in the
	// blackboard API (recall the requirement that the object storage
	// functionality should not be something ever explicitly
	// interacted with by users).  The approach to treat all object
	// stores as unique services, even when managed/mediated by the
	// same executable also keeps our interface clean.  There may be a
	// btter/cleaner/more efficient way to do this.
	var OBJ ObjectStore
	sos, err := StartSimpleObjectService()
	if err != nil {
		log.Fatal("Failed to start object store (required): ", err)
	}
	OBJ = sos

	// Instantiate the BB for the run
	// Eventually might be cleaner to have BlackboardSpec interface...
	var BB Blackboard
	if config.Globals.MongoBBSpec != "" {
		bb_spec := NewMongoBBSpec(config.Globals.MongoBBSpec)
		bb_spec.ValidateSpec()
		BB = NewMongoBB(bb_spec)
	} else {
		BB = NewMemoryBB(OBJ)
	}

	// Start the outside facing APIs
	http_bb := apis.NewBlackboardHTTPService(BB)

	comms := []apis.CoreAPI{http_bb}

	go func() {

		err := http_bb.Start(config.Globals.BlackboardHTTPAddress)

		if err != nil && err != http.ErrServerClosed {
			// TODO: this is not necessarily a fatal event
			log.Fatal("http api went down: ", err)
		}

	}()

	return BB, comms, nil
}

func StartServer(cmd *cobra.Command, args []string) {

	BB, comms, err := StartServer2()
	if err != nil {
		log.Fatal("failed to start core server: ", err)
	}

	// Listen for system signals (Ctrl-C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	count := 0
	for range c {
		count++
		if count == 1 { // 1 press of ctrl-c
			// TODO: stop accepting new messages, save bb to disk
			// log.Info("TODO: implement clean shutdown (ctrl-c again to stop)")

			for idx := range comms {
				err := comms[idx].Shutdown()
				if err != nil {
					log.Error("error while shutting down: ", err)
				}
			}

			BB.Shutdown()

			os.Exit(0)
		}

		if count == 2 { // 2 press of ctrl-c
			log.Fatal("immediate shutdown (blackboard not saved)")
		}
	}

}
