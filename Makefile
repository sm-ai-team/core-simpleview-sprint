VERSION := $(shell git describe --tags)
USER := johnmarianhoffman
REPO := core-server

# Build a single server image
server:
	docker build . -t ${USER}/${REPO} \
		-t ${USER}/${REPO}:${VERSION}

#TODO: support for linux/riscv64 (looks like alpine isn't currently building for it)
# Build server for all supported platforms
server-multiarch:
	docker buildx build --platform linux/amd64,linux/arm64 \
		-t ${USER}/${REPO} \
		-t ${USER}/${REPO}:${VERSION} \
		--push .

# Build individual python images
python-images: 
	$(MAKE) -C python/ docker-images

# Build python for all supported architectures
python-multiarch:
	$(MAKE) -C python/ deploy

# Mega target to build everything for everything
deploy: server-multiarch python-multiarch
