# core

![coverage](https://gitlab.com/hoffman-lab/core/badges/main/coverage.svg?job=format)
![pipeline status](https://gitlab.com/hoffman-lab/core/badges/main/pipeline.svg?ignore_skipped=true)
![latest release](https://gitlab.com/hoffman-lab/core/-/badges/release.svg)

![SimpleMind logo](docs/icons/sm_logo_163.png)

This repository is in active development to provide a modern, cloud/network-friendly, extensible version of the [SimpleMind](https://gitlab.com/sm-ai-team/simplemind) backend, primarily developed in Go.

**NOTE: Our immediate goal is to get the minimum functionality online so that current SM users can begin switching over to, and developing agents for this version of SM. If a feature is missing that blocks your ability to use this implementation, PLEASE REPORT IT, comment on an existing ticket, or give it a thumbs up to help us prioritize what we focus on.**

## Looking for the Python API?

The documentation in this repository is related to the `core` server code, and the Go Agent API.

Python API documentation can be found here:
- [README](python/pkg/README.md)
- [PyPi](https://pypi.org/project/smcore/) 

- [FAQ](https://gitlab.com/hoffman-lab/core/-/issues/?sort=created_date&state=all&label_name%5B%5D=FAQ&first_page_size=20)

## Getting Started

### Docker images 

(More documentation coming soon, but for now):

#### Core Server

Run a core blackboard server on port 8080 (port 8082 is required for the object service):
```
docker pull johnmarianhoffman/core-server
docker run --rm -p 8080:8080 -p 8082:8082 johnmarianhoffman/core-server
curl -i localhost:8080/health # should return status 200 if working correctly
``` 

#### Agent base containers:

Python base image:
```
docker pull johnmarianhoffman/smcore-python

# prints version numbers if working correctly
docker run -it --rm --entrypoint "./docker_test.py" johnmarianhoffman/smcore-python

# shell into the container for experimentation
docker run -it --rm johnmarianhoffman/smcore-python
```

PyTorch base image:
```
docker pull johnmarianhoffman/smcore-pytorch

# prints version numbers if working correctly
docker run -it --rm --entrypoint "./pytorch_test.py" johnmarianhoffman/smcore-pytorch

# shell into the container for experimentation
docker run -it --rm johnmarianhoffman/smcore-pytorch
```

You should be able to copy your agent script into the base images and then run inside the container.

There will be a bit of network finagling to get the docker container to see the host ports correctly, that I intend to automate at some point in the future.  Please refer to this [Stack Overflow question](https://stackoverflow.com/questions/31324981/how-to-access-host-port-from-docker-container) if you'd like to go ahead and try to get it working. 

TODO: Put together a working example of how to do this and/or automate the configuration for easy (easier) use.

### Quick Reference 

Build the project from source (requires the [go tool](https://go.dev/dl)):
```
git clone https://gitlab.com/hoffman-lab/core.git
cd core/
go mod tidy
go build
./core run test-files/ping-pong.yaml
```

Run a knowledge graph until it all agents complete:
```
./core run path/to/config.yaml
```

Create a new python agent:
```
./core generate python-agent --name my-new-agent
```

Run a local server:
```
./core start server
# or
./core start server --addr myhost.mydomain.com:<port-num>
```

Start agents but no server (**NEW!**):
```
./core start agents path/to/config.yaml --addr myhost.mydomain.com:<port-num>
```

### Sample configuration file (YAML):

The following is an annotated version of [test-files/ping-pong.yaml](test-files/ping-pong.yaml).  Please refer to the repository version for the most up to date version of this example.

``` yaml
# controllers manage agents listed in the "agents" block.
# different controllers have different strategies for managing agents.
# naive-controller launches all agents at once
# basic-controller starts agents once their dependencies (promises)
# are fulfilled.
controllers:
  #naive-controller:
  #  runner: go
  #  entrypoint: StartNaiveController
  #  attributes:
  #    agents: Promise( * as agent-bundle from * )

  basic-controller:
    runner: go
    entrypoint: StartBasicController
    attributes:
      agents: Promise( * as agent-bundle from * )
      save-to-svg: "TEST.svg"
	  
# agents carry out processing (whatever that means for your problem!)
# They must be paired with a runner.  More on runners below.
# Attributes are how you pass configuration data, as well as express
# interdependencies between agents ("promises", see below).
agents:
  ping_agent:
    type: go
    runner: go
    pingIntervalMs: 1000
    entrypoint: "StartPingAgent"
    attributes:
      dwell: 1

# PongAgent expresses a dependency on "ping-agent" via a "promise"
# Promises take the form "Promise( <name> as <type> from <source-agent>)".
# Name, type, and source can also be "*" to allow anything to match.
  pong_agent:
    type: go
    runner: go
    entrypoint: "StartPongAgent"
    attributes:
      ping: Promise(SpecialPing as Result from ping_agent)

# Runners are themselves agents that know how to dispatch a processing 
# agent a target environment.  The "go" runner (for now) is always required.
runners:
  go:
    name: 'go'

```

## Run the above example

```
./core run test-files/ping-pong.yaml
```

## Explaining the Example

![Software architecture and knowledge graph for ping-pong example](docs/ping_pong_arch_403.png)

Here, we run the configuration file (a.k.a. a "knowledge base", shown below) using the `sm` tool.

From [test-files/ping-pong.yaml](test-files/ping-pong.yaml):

Two agents are created: `ping_agent` and `pong_agent`.

The pong agent expresses a dependency on data produced by the ping agent using a `Promise`. Promises take the form of `Promise(DATA_NAME as DATA_TYPE from SOURCE_AGENT_NAME)`. These fields are 100% configurable by the user and should be used to match data between the independent agents. The `SOURCE_AGENT_NAME` must be another agent specified in the configuration file.

In the ping pong example, we have `Promise(SpecialPing as Result from ping_agent)` and the pong agent utilizes this to listen for a "SpecialPing" from the "PingAgent" before writing its own `SpecialPong` message to the Blackboard, and exiting.

Try changing the name, type, or source of the Promise in the example file and rerun. You'll see that it doesn't work!

The command-line output that you see, something like:

```
================================================================================
Index      Source               Type             Latency    Msg Contents
================================================================================
00000000   controller           Hello            0s         &{}
00000001   go                   Hello            0s         &{}
00000002   controller           CreateAgent      0s         &{name:"ping_agent"  runner:"go"  pollInterval_ms:500  pingInterval...
00000003   controller           CreateAgent      0s         &{name:"pong_agent"  runner:"go"  pollInterval_ms:500  pingInterval...
00000004   grpc-api             Log              1µs        &{severity:Info  message:"grpc api starting on :8081"}
00000005   grpc-api             Log              407µs      &{severity:Info  message:"http api starting on :8080"}
00000006   ping_agent           Hello            4µs        &{}
00000007   pong_agent           Hello            1µs        &{}
00000008   pong_agent           StatusUpdate     1µs        &{state:AwaitingPromise  message:"awaiting 1 promises"}
00000011   ping_agent           Result           2µs        &{promise:{name:"SpecialPing"  type:"Result"  source:"ping_agent" ...
00000012   ping_agent           Goodbye          2µs        &{}
00000015   pong_agent           StatusUpdate     1µs        &{state:Working}
00000016   pong_agent           Result           1µs        &{promise:{name:"SpecialPong"  type:"Result"  source:"pong_agent" ...
00000017   pong_agent           Goodbye          1µs        &{}
00000021   controller           Goodbye          12µs       &{}
================================================================================
```

is the live-scrolling contents of the Blackboard that all agents use to communicate.

If you're already familiar with agents and blackboards, feel free to skip ahead to learn [how to create your own agents](#adding-your-own-agents). Otherwise, the architecture summary below may be useful to fill in some of the basics.

## Architecture Basics

TODO: Diagram of agent/BB communication

At its heart, the SimpleMind architecture is simply a **Blackboard** data structure to allow independent, asynchrous **Agents** to communicate. Obviously we've added a lot of additional functionality, but that's the bulk of it! Everything else is built around that model.

### The Blackboard

The blackboard is just a **log** of **messages**. The blackboard exposes a minimal gRPC, HTTP, and Go APIs (as an interface). This interface allows agents to `GetMessages` and `WriteMessage`. Agents use these APIs to communicate with the blackboard.

### Agents

An agent is anything that communicates with the blackboard. Most of these will be small programs that you implement that solve some piece of a bigger task. There are also several "core" agent types to be aware whose messages will show up in the blackboard.

#### Core Agents

A blackboard, by itself, doesn't do anything. We need something to parse our knowledge base (configuration file) and bootstrap the rest of the system.

Currently, there are two "core" agents required to call `sm run <config-file>` SimpleMind:

- The **controller** ([controller.go](pkg/agents/controller.go)) schedules, dispatches, and monitors agents
- At least one **runner** to listen for CreateAgent messages and send the appropriate commands to start the agent in the runner's environment. The base runner used to implement new runners can be found at [runner.go](pkg/agents/runner.go)

We currently offer two runners (with more on the way soon):

- A `GoRunner` type ([runner_go.go](pkg/agents/runner_go.go)), that starts agents written in Go that are compiled into the `sm` tool
- A `LocalRunner` type ([runner_local.go](pkg/agents/runner_local.go)), the issues command line calls to the local computing environment (such as to start python scripts)

The two particular runners are the only "special" types that offer configuration shorthands `go:` and `local:` and only one of each is allowed.

The ping pong example above uses the `GoRunner` type to start the agents found in [ping_pong.go](pkg/agents/ping_pong.go).

We hope to bring a `DockerRunner` and `CondorRunner` online shortly.

### Messages

Agents communicate with the blackboard with a predefined messaging protocol. The protocol is specified by in the protobuffer file [proto/types.proto](proto/types.proto). However, we provide language-specific APIs so that you do not need to worry about the details of the protocol.

As your implementing agents, the main messages your agents will send will be

- Logs (`log(severity, message)`)
- Status Updates (`post_status_update(agent_state, message)`)
- Results (`post_result(data_name, data_type, data)`)

The other ones you'll come across soon

- Partial results (`post_partial_result(data_name, data_type, data)`)
- No results (`post_no_result(data_name, data_type, data)`)

There are additional messages you'll see appear in the blackboard, but they are handled automatically via our agent APIs. These include:

- CreateAgent messages (passed between the controller and the runners)
- Hello/Goodbye messages from agents
- Certain types of status updates
- Agent background pings (only if display is enabled)

Except for advanced agent implementation, you should not need to manually send these messages (and doing so might interfere with the excected behavior).

## Adding Your Own Agents

At some point, you're going to want to implement your own agents. The two currently supported agent types are `GoAgent` and a python `Agent`.

We have provided templates for empty python agent types and empty go agent types:

```bash
sm generate python-agent --name my-example-python-agent
sm generate go-agent --name my-example-go-agent
```

Note that the `--name` flag is required, and the name format must follow RFC-1035 (alphanumeric characters using "-" as a separator).

This will create the `my_example_go_agent.go/py` file in the current directory. Inside of the file, you will find additional instructions of where to put your agent file and how to start it in a SimpleMind run. Replace the `# Your code here` section as if it were the "main" function of any other program.

See our full example (TODO) of adding a python agent for more information.

See our full agent API documentation

We're working to build up a library of off-the-shelf agents to use, but in the meantime, any agents you think would be broadly useful, please consider submitting back to the project.

## Project Details

Please use the [issues](https://gitlab.com/hoffman-lab/core/-/issues) page to report bugs, provide feedback, and request features.

Current, high-priority work includes:

- Essential documentation [issue](#22)
- A Docker runner [issue](#23)
- An HTCondor runner [issue](#24)
- Knowledge graph [issue](#27)
  - dependency resolution
  - visualization

If any of these are especially important to you, please go give the issue a thumbs-up.

We're working as quickly as possible to get the basics online so that current users of SM can begin developing ASAP, so please also report features that you would need to begin using this version of SM.
