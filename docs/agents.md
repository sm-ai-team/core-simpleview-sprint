# Agents

Other than the Blackboard, an Agent is the most fundamental component of the Simple Mind architecture.

- An agent is *anything* that communicates with the blackboard (more on this below)
- Agents are *independent* and only communicate with one another via the Blackboard

## Agent Lifecycle

Most agents will follow a well-defined lifecycle:

1. Agent starts and says hello 
1. Agent will await blackboard messages containing any "promised" data from other agents
1. Once promises are resolved, the agent will perform its unit of work
1. The agent will write any results it has created back to the blackboard, including output data.
1. Finally, the agent will say goodbye and clean up after itself

The good news is that all lifecycle management is handled via our agent APIs provided.

More advanced agent development is possible (long-lived agents and agents that don't follow the standard lifecycle, customized message handling, etc.) however all agents should use our API to handle basic lifecycle behavior.

## Agent API

For end users of SM, we prefer you use our Python API; we also support a Go agent API that functions very similarly to the Python API.  We have dedicated examples of each API given at (TODO: links to full examples); the notes below are given for the Python API.

Below is a minimal Python agent, generated using the `sm` tool.

```
$ ./sm generate python-agent --name minimal-agent
```

This creates a file in the current directory called `minimal_agent.py`:

```python
from enums import Log
from agent import Agent, default_args

class MinimalAgent(Agent):
    
    def __init__(self, spec, bb):
        super().__init__(spec, bb)
        
    async def run(self):
        try:
            
            # Your code HERE
            # self.log(Log.DEBUG,"hello from MinimalAgent")
            
        except Exception as e:
            self.post_status_update(Status.ERROR, str(e))
        finally:
            self.stop()

if __name__=="__main__":    
    spec, bb = default_args("minimal_agent.py")
    t = MinimalAgent(spec, bb)
    t.launch()
```

**NOTE: The python agent is still not 100% cleaned up just yet. I'm trying to keep this document in sync with what would work with the current code and not "aspirational. That being said, it's still actively changing so I make no guarantees."**

**Currently, python agents will only run if they are in the "python" directory of the core repository. We're working on a pip-installable module for the python API ASAP.**

You don't actually need to understand anything beyond the "Your code HERE" section if you don't want to.  Simply replace "Your code here" as if it were a main function, put the script into the "python" directory of this repository, and add the agent to your configuration with the "local" runner.

We've added a lot of convenience for communicating with the blackboard that will be useful, so keep reading.

### Basics from the example

As you can see in the example, we provide the `Agent` base class which provides all of the connection logic, real-time messaging, data retrieval, etc. as well as exposes convenience methods for easy communication between your agent and the blackboard. 

The `launch` method called on the last line is what kicks the whole process off and is required to establish communication with the blackboard.  Functionality such as `await_data` relies on an agent having been launched.

Overriding the base class' `run` method is how you make the agent do your calculation.  Once launch is called, your run method will be called in proper sequence.

The `try/catch/finally` statement here is used to wrap user exceptions and populate them back onto the blackboard so that we aggregate all of our logging in one place.  `self.stop` is required to complete agent execution (shut down background processes) and "say goodbye" to the blackboard.

### Important Agent methods

The most common agent methods that you'll use with most agents are:

- `agent.await_data()` 

await_data holds agent execution until the required precursor data arrives from the blackboard.  Once enough data is available, await data returns an array of promises that can be used for further processing.

- `agent.log(self, severity, message)`

log is used to post messages from your agent to the blackboard.  Use log messages for "stdout"-type logging from your agent.

- `agent.post_result(name, type, data):`

post\_results is an agent's primary mechanism for writing data onto the blackboard.  Each result has a "name" and a "type" that are set by the agent and used by other agents to identify data they need for their own processing.  We also provide `post_partial_result` which can be used if an updated version of the result will be provided at a later time, as well as `post_no_result` if an agent calculation completes successfully, but did not produce a result.

- `agent.post_status_update(state, message)`

In general, you should prefer log and really only use post\_status\_update if your agent encounters an error it can't recover from.  Status updates are intended to be more standardized than logs and represent a state change in the agent, accompanied with a message.  The statuses are used primarily by the controller to monitor agents and know when to shut down, start new agents, etc.

Most agent state changes and their associated status changes are *already* handled by the API, but catching and reporting errors is important and we recommend doing that with `post_status_update`, as shown in the above example.

## A More Realistic Example

TODO

## More Information 

TODO