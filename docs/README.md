# SimpleMind Documentation

For now, all documentation and supporting files (diagrams, images, etc.) related to Simple Mind, specifically the Go implementation of SM, will live in this directory (do not include any documentation from the first version).  This README should serve as an index for the docs and an FAQ.  Any unlinked item below is unfinished documentation on our roadmap.

## First Steps

- Install the `sm` tool
- First example (ping pong)
- Creating your own agents
  - Full Python example
  - Full Go example
- Next steps

## Concepts and architecture

The documents explain some core concepts and ideas of SimpleMind geared towards *users* of SM.

- The Blackboard
- [Agents](agents.md)
  - [Python API (recommended)](agents.md#agent-api)
  - Go API
- Runners
- Managing Environments

## Developer Documentation

Developer documentation discusses how each of the following is implemented in the core and where appropriate, some discussion about the choices made and why.

If you would like to extend the core (such as adding a new runner, adding a new blackboard implementation, adding core agents, etc.) this is the correct place to find information.  PLEASE READ ANY RELEVANT DOCUMENTATION BELOW BEFORE SUBMITTING ANY MERGE REQUESTS TO THIS REPO.

- Overview
- The Blackboard
- Agents
- Runners
- Knowledge Base (configuration file)
- Communication and networking
- Thoughts from John

## FAQ

### Is this ready?

(We will keep updating this in the coming weeks - JMH 2023/07/09)

Yes and no.  Most of the fundamental constructs are in place and ready for initial use.

- An in-memory blackboard
- Python agent API
- Go agent API
- Local runner (to run scheduled jobs on the local computer, i.e. python agents)
- Promise resolution (for passing data between agents via the blackboard) 

This is probably enough to start building *some* things, but not most.

Key features that are still missing before we can really even call it a "beta" version:

- Essential documentation
- Input data handling
- A "results" fileserver
- Basic dashboard and monitoring

### How do I request a feature?

Please check the issues page first to see if there's already an open (or closed) issue about your request.  If not, or if the issue was closed without resolution please open an issue.

### How do I get help?

For now, bug John or Josh.  In the long run, we'd like to try and collect Q&As online (in a forum or something), but we don't have anything set up just yet.