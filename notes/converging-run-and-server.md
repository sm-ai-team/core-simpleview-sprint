# Converging Run and Server modes

Performing this action is somewhat "philosophical" but I actually think that it's a critical requirement for future development.

In a true blackboard architecture, all agents should be equal, and the controllers are just another type of agent. They're deceptively a little bit special in that they manage other agents, but in terms of how the blackboard server treats them, they shouldn't be handled any different than regular old processing/computing agents.

I went back and forth on this decision early on and concluded that controller *was* a special agent and merited being handled specially.  I also wanted to make sure that I wasn't being to pathological in my interpretation/implementation of the blackboard/agent architecture. Now it's become clear that to truly maintain extensibility and customizability of the architecture in the long run, we actually should be a bit pathological/literal.

Essentially, it should be up to the user what controller is injected onto the server to manage their agents. The blackboard can have limited say in whether or not controllers are allowed to, say, shut the blackboard down, however the default behavior of the blackboard should be "I'm a server, I do server things, and servers don't shut down."  We will provide our "run" controller as an out-of-the-box controller that should address most folks' use cases in these early days.

## Some definitions:

- agent -> anything that communicates with the blackboard
- controller -> any agent that manages agent lifecycles (i.e. writes CreateAgent messages)

## The issue:

> if the barber shaves everyone in the town who doesn't shave themselves, who shaves the barber?

Controllers *must* not be special in the bigger picture of SM. This is to enable the ability at some point for controllers to manage other controllers as KGs grow and become more complex.  We must be able to easily compose them.  To make a controller a special snowflake agent means that the lifecyle will need special handling, etc. which will block or overcomplicate the future composition of increasingly complex knowledge graphs.

So given that controllers cannot be "special", but only controllers can manage other agents, who starts the controller?

## Proposed solution

This should be a fully-backwards compatible possible solution to this issue. 

We propose the following knowledge graph specification:

```yaml
# The controller block specifies agents that are to be started manually by our executable.
# 
# As the name signifies, this is generally expected to be used for a single controller to manage our agent lifecycles.  This agent will manage the "agents" block found below (i.e., all creation of agents should be handled at the agent level, not the exe level.
#
# Side note: I don't actually like the name "controller" for this block, but it's pretty understandable here.  A more appropriate name might be something like "bootstrap" or similar.  Basically any agents that the executable will manually start (i.e. the exe/cmd will write its own "create agent" messages to the blackboard.)
controller:
	naive: 
		runner: go
		entrypoint: "StartNaiveController"

# Agents 
# These agent specs will be passed
agents:
	agent_1:
		...
	agent_2:
		...

# The runners block specifies runners (obvs).
#
# As with controllers, runners are required to just be agents and must receive all of their instructions/commands from the blackboard.
#
# Runners must also started manually by the executable.  I think controllers are more likely to be customized (i.e. implemented in python) than runners (runners are a write once per environment kind of thing, as long as the configuration options are good enough).  Therefore, the controller must be able to specify its runner.  If the controller requires a runner to exist, but the runner has to be started by a controller, then we have a dependency cycle.  Therefore, this requires us to start runners manually.
#
# Note: We can simple have different "heading" items, and handle them however we want (e.g., "controller", "bootstrap", "agents", "runners", etc.).  We can enable different functionality depeneding on what people request.
#
# Runners provide the "shim" logic to map an agent specification to a target runtime environment.
runners:
	go:
	...
```

At this stage, we initially should implement `controller`, `agents`, and `runners`.  This preserves backwards compatilibity and is (in my opinion) an intuitive transition into the new paradigm.

## Executable Commands

This also represents a bit of a shift in how we understand the `core` executable.  The cobra commands that we provide (one command per file found in `cmd/`) now can be understood as "what should be started/performed".  The commands should become something like:

- server 

  only start the server without any agents
  possible names: "server"
  
- server+agents
  
  start a server and then start runners, and agents
  possible names: "run", "start", "launch"
  
- agents 

  perform agent startup via configuration file but do not start a blackboard server
  possible names: "add", "insert", "inject", "attach"
  
- generate
  
  code generation functionality
  possible names: "add", "insert", "inject", "attach"
  
The executable now becomes a "first class" agent and should be treated as such.  We can reflect this in the naming of the agent and the identifying information that gets shared (providing some traceability/identifiability will also be useful if we ever needed to identify bad actors)

The executable will have to write at least one full copy of the input spec onto the blackboard.  The controller can technically only receive its information via the blackboard (i.e. it should not have a special interface/signature).  This can eventually more easily allow for controllers to be written in python, and run distrubuted (i.e. away from the blackboard executable.

The blackboard then contains the configuration used to generate the data, that is then contained on the blackboard.  Lotsa self-similarity. (You could even have the agents dump their source code onto the blackboard if you wanted! We'll stick with this approach for now, since there's actually demonstrable value it doing it.)

## The controller interface

There shall not be a controller interface.  Controllers should be handled exactly like any other agent.

## The runner interface

There shall not be a runner interface.  Runners should be handled like all other agents.

We can (do) provide a base runner that captures create agent messages and automatically surfaces them into a callback function.

In the long run, we *must* find a way to manage runners like all other agents, but for right now, runners will be required to run as part of the main executable and be written in Go.  We don't have an immediate need for this right now, so we will postpone the figuring out of this until some time in the future.

Only runners do not have to specify a runner to be matched to.  In this regard, for now, runners are the only true "special agents".  I don't think that they actually *have* to be though.  It should be totally possible to implement a runner in python and dispatch it with a different runner, but we do need some sort of bootstrapping runner.  Maybe just the Go runner and the Local runner are special, and maybe eventually we can hide them a little bit and move to a more general description/handling of runners. 

