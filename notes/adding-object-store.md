# Adding an object store to the blackboard

## The Problem

The blackboard's performance is currently limited since it ends up receiving and shared *all* results posted, even when not specifically requested or required by an agent.  This leads to a tremendous amount of unnecessarily shared data.

"Spiritually" all data should be stored directly on the blackboard, and every agent should know about it.  Practically, we need a workaround. In the agent/blackboard paradigm, we're letting agents skim the blackboard for the data that they need, rather than reading everything.

## Requirements

### Functionality
- Blackboard must be able to function without user-specified object store (i.e. object store should be optionally invisible to end-user)
- (for now) Nothing should interfere with default agent/blackboard interaction of all agents receive all messages
- If an object store is present, the blackboard should transparently store and link to any data bigger than some user-configurable size into the object store
  - Agent result retrieval in this case is 2 stage: (1) retrieve the result and then (2) retrieve the result data from the object store
  - Additional thought: maybe every result goes into the object store (i.e. no configurable threshold).  Although result passing requests are likely to be the most compute intensive overall, the total number will likely be reasonably low.
  - There likely is a very real performance tradeoff to enforcing an "always object store" mode.  Going ahead putting in the effort to build a user-configurable threshold would allow people to tune based on their needs.  IF WE DO THIS: we should provide a script to automatically measure and recommend settings for folks (secondary though).
- Object store should be completely invisible to users (i.e. no modifications to the end-user API)
  
### Object store interface
- Must support multiple implementations of object stores
- Object store should be configurable (i.e., an interface so we can swap in different object stores easily and quickly)
- Relatively easy to add new object stores (i.e. interface should be simple and easy to implement for new services)

### Final stuff
- I'd like to be able to concretely measure performance of three situations (1) no object store, (2) our basic file store, and (3) third party object stores.  This is something really important to get right, and if we can't measure it, we can't make claims about it.

## Our own thing vs third-party software

So we should really eventually make sure that we support something like minio, which in a real deployment is likely to be way more optimized for actual high-performance, however we're currently seeing that we probably need something lightweight and local to improve performance in the short term.

Do we need a full object store like [core-objectstore](https://gitlab.com/hoffman-lab/core-objectstore) or could it just be part of the blackboard API exposing either a download link and/or a redirect? It could also be both, with core-objectstore in use for all results?

## Possible scenario 1 (maybe john's favorite): Always have an object store

There is no longer a mode for a blackboard to operate without an object store

- ObjectStore is an interface
- Blackboard always assumes that it has an object store (maybe even embeds an object store?)
- We provide an automatic, simple object store if no 3rd party object store is specified by the user
- We provide at least one example of supporting a third party interface using MinIO

Possible con:
- Two services/points of failure
- No obvious recovery mechanism for blackboard if object store goes down
- Has some symptoms of "distributed monolith" syndrome

Cons could be mitigated by:
- Own the distributed monolith (in this case).  If one part goes down, everything goes down (how to enforce and log this?)
- Good monitoring: maybe not everything crashes, but we have a status page (part of the web interface) that would clearly show that things have stalled b/c of lost connection

Possible pros:
- I think this is the cleanest approach in terms of maintainability
