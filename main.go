/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import (
	"embed"

	"gitlab.com/hoffman-lab/core/cmd"
	"gitlab.com/hoffman-lab/core/pkg/agents"
)

//go:embed web/template/*
var WebFS embed.FS

func main() {

	agents.WebFS = WebFS
	cmd.Execute()
}
