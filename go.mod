module gitlab.com/hoffman-lab/core

go 1.21.2

//replace gitlab.com/hoffman-lab/core-objectstore => ../core-objectstore

require (
	github.com/briandowns/spinner v1.23.0
	github.com/deckarep/golang-set/v2 v2.3.0
	github.com/golang/protobuf v1.5.3
	github.com/gorilla/mux v1.8.1
	github.com/matryer/is v1.4.1
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/cobra v1.7.0
	gitlab.com/hoffman-lab/core-objectstore v0.0.3
	go.mongodb.org/mongo-driver v1.12.1
	golang.org/x/crypto v0.10.0
	gonum.org/v1/gonum v0.14.0
	google.golang.org/grpc v1.56.1
	google.golang.org/protobuf v1.31.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/fatih/color v1.7.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.4.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/term v0.9.0 // indirect
	golang.org/x/text v0.10.0 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
)
